(cl:defpackage :std
  (:use :std.core)
  (:local-nicknames (:array :std.sequence.array)
                    (:file :std.fs.file)
                    (:function :std.function)
                    (:hashtable :std.hashtable)
                    (:list :std.sequence.list)
                    (:number :std.number)
                    (:package :std.namespace)
                    (:print :std.io.print)
                    (:regex :std.regex)
                    (:sequence :std.sequence)
                    (:string :std.string)
                    (:symbol :std.symbol)))
(cl:in-package :std)

(defun package-data-collect ()
  (-->
    (sequence:sort (package:list) string:<= :key package:name)
    (list:map :? (lambda (package)
                   (let ((name (package:name package)))
                     (when (string:start= name "STD.")
                       (let ((symbols ()))
                         (cl:do-external-symbols (symbol package)
                           (list:push-start! symbols symbol))
                         (list package
                               (string:lowercase name)
                               (sequence:sort symbols string:<= :key symbol:name)))))))
    (sequence:remove :? nil)))

;; (defvar +bracket-curly-start+ (string:format "~%{~%"))
;; (defvar +bracket-curly-end+ (string:format "~%}~%"))
;; (defvar +code+ "```")

(defvar +doc-info+ (string:format "~%INFO: "))
(defvar +doc-warning+ (string:format "~%WARNING: "))
(defvar +doc-todo+ (string:format "~%TODO: "))

(defvar +markdown-info+ (string:format "~%?> "))
(defvar +markdown-warning+ (string:format "~%!> "))
(defvar +markdown-todo+ "...")

(defun docstring-process (string)
  (cl:flet ((replace! (from to)
           (setf string (regex:replace-all from string to))))
    ;; (replace! (string:format "~%\\(") (string:format "~%~%("))
    ;; (replace! " +; => "               (string:format "~%; "))
    (replace! +doc-warning+           +markdown-warning+)
    (replace! +doc-info+              +markdown-info+)
    (replace! +doc-todo+              +markdown-todo+)

    ;; (when (string:contains? string (string:format "~%("))
    ;;   (setf string (string:concat (std.regex:replace (string:format "~%~%\\(")
    ;;                                                  string
    ;;                                                  (string:format "```~%("))
    ;;                               "```")))
    )
  string)
  ;; (dolist (line (std.string.unicode:lines string))
  ;;   (setf line (std.string:trim '(#\Newline) line))
  ;;   )

(defun arguments-to-string (list)
  (string:join
   (sequence:map
    list
    (lambda (item)
      (cl:typecase item
        (cl:null "nil")
        (keyword (string:format "~(~S~)" item))
        (list
         (string:format "(~a)"
                        (let ((1st (list:1st item))
                              (2nd (list:2nd item)))
                          (if (and (symbol:? 1st)
                                   (string:= (symbol:name 1st) "QUOTE")
                                   (list:?? 2nd))
                              (arguments-to-string 2nd)
                              (arguments-to-string item)))))
        (symbol (string:format "~(~a~)" item))
        (cl:character (string:format "~:(#\\~a~)" (std.char:name item)))
        (otherwise (string:format "~a" item))))
    'list)
   " "))

(defun symbol-escape (symbol)
  (let* ((name (symbol:name symbol))
         (len (sequence:len name)))
    ;; (print:format t "~%symbol-escape ::: symbol '~(~a~)' has len = ~a" symbol len)
    (if (and (number:positive? len)
             (string:start= name "*"))
        (string:trim name (list #\*))
        name)))

;; (sequence:subseq! )

;; (defun print-note-symbol-macro (file symbol ))
;;

(defun print-args (file package-nickname symbol &optional args-new args-old)
  (let ((arguments (if (and args-new args-old)
                       (-->
                         (list:len args-new)
                         (list:skip args-old :?)
                         (list:append args-new :?))
                       (function:args symbol))))
    (if arguments
        (print:format file "~%`(~a ~a)`~%"
                      (string:format "~(~a:~a~)" package-nickname symbol)
                      (arguments-to-string arguments))
        (print:format file "~%`(~(~a:~a~))`~%" package-nickname symbol))))

(defun package-documentation-write (data)
  (cl:destructuring-bind (package name symbols)
      data
    (print:format t "~%~%~%PACKAGE NAME: ~(~a~)~%~%~%" (package:name package))
    (unless (string:start= (package:name package) "STD.INTERNAL")

      (let ((filename (string:concat (string:lowercase name) ".md"))
            (banner (string:trim (or (cl:documentation package t) "")
                                 (list #\Space #\Newline #\Linefeed #\Return)))
            (package-nickname (sequence:subseq! name (1+ (or (sequence:position name #\. :from-end t)
                                                             -1)))))
        (let* (;; (package-size (sequence:len symbols))
               ;; (variables (array:create package-size :fill-pointer 0))
               ;; (functions (array:create package-size :fill-pointer 0))
               ;; (macros (array:create package-size :fill-pointer 0))
               )
          (file:stream-do (file (string:concat "./DOCUMENTATION/" filename)
                                :direction :output
                                :if-exists :supersede
                                :if-does-not-exist :create)
            (print:format file "# ~a~%" (string:lowercase name))
            (print:format file banner)
            (list:foreach! (symbol symbols nil)
              (print:format file "~%---")
              (cl:cond
                ;; constants
                ((string:start= (symbol:name symbol) "+")
                 (print:format file "~%### *Constant* &nbsp; ~(~a~)" (symbol-escape symbol)))
                ;; dynamic variables
                ((string:start= (symbol:name symbol) "*")
                 (print:format file "~%### *Dynamic variable* &nbsp; \\*~(~a~)\\*" (symbol-escape symbol)))
                (t
                 (cl:destructuring-bind (&key args-new args-old source macro? &allow-other-keys)
                     (hashtable:get std.internal::*documentation* symbol)
                   (declare (ignorable source))
                   (cl:cond
                     ;; macro
                     (macro?
                      (print-args file package-nickname symbol args-new args-old)
                      ;; (print:format file "~%__Signature:__ ~(~a~)"
                      ;;               (-->
                      ;;                 (list:len args-new)
                      ;;                 (list:skip args-old :?)
                      ;;                 (list:append args-new :?)
                      ;;                 (list:node (string:format "~(~a:~a~)" package-nickname symbol) :?)))

                      (when (getf (hashtable:get std.internal::*documentation* symbol) :symbol-macro?)
                        (print:format file "~%Can be used as value (without `#'`)."))

                      (unless (sequence:start= args-old args-new :key symbol:name :test string:=)
                        (print:format file "~%~aDifferent argument order!" +markdown-warning+))

                      (let ((docs (docstring-process (cl:documentation symbol 'cl:function))))
                        (print:format file "~%~a" docs)))

                     ;; function
                     ((or (cl:ignore-errors (symbol:function source))
                          (and source (symbol:function source)))

                      (print:format file "~%### ~(~a~)~%__Signature:__ " (symbol-escape symbol))
                      (print-args file package-nickname symbol args-new args-old)

                      (when (getf (hashtable:get std.internal::*documentation* symbol) :symbol-macro?)
                        (print:format file "~%__Has symbol macro `~(~a:~a~)` (without the `#'`)__~%"
                                      package-nickname
                                      symbol))

                      (let ((docs (docstring-process (cl:documentation symbol 'cl:function))))
                        (when docs
                          (print:format file "~%~a" docs))))
                     ;; no idea
                     ((cl:macro-function symbol)
                      (print:format t "~%got a regular %:macro ....... ~(~a~)" symbol)

                      (print:format file "~%### *Macro* ~(~a~)" (symbol-escape symbol))
                      (print:format file "~%__Signature:__ " (symbol-escape symbol))
                      (print-args file package-nickname symbol)
                      )))
                 ))))
          ;; (let ((variables "")
          ;;       (functions (-->
          ;;                    (list:map symbols
          ;;                              (lambda (symbol)
          ;;                                (when (and (cl:ignore-errors (symbol:function symbol))
          ;;                                           (not (symbol:macro-function symbol)))

          ;;                                  (string:format "~%---~%### ~(~a~)~%~%~a"
          ;;                                                 symbol
          ;;                                                 (docstring-process
          ;;                                                  (or (cl:documentation symbol 'cl:function)
          ;;                                                      ""))))))
          ;;                    (sequence:remove :? nil)))
          ;;       (macros (-->
          ;;                 (list:map symbols
          ;;                           (lambda (symbol)
          ;;                             (when (symbol:macro-function symbol)
          ;;                               (string:format "~%---~%### _MACRO:_ ~(~a~)~%~%~a"
          ;;                                              symbol
          ;;                                              (docstring-process
          ;;                                               (or (cl:documentation symbol 'cl:function)
          ;;                                                   ""))))))
          ;;                 (sequence:remove :? nil))))
          ;;   (cl:with-open-file (doc (string:concat "./DOCUMENTATION/" filename)
          ;;                           :direction :output
          ;;                           :if-exists :supersede
          ;;                           :if-does-not-exist :create)

          ;;     (print:format doc "# ~a~%" (std.string:lowercase name))
          ;;     ;; banner
          ;;     (when banner
          ;;       (print:string banner doc))
          ;;     ;; variables & constants
          ;;     (when variables
          ;;       (print:string variables doc))
          ;;     ;; functions
          ;;     (when functions
          ;;       (cl:dolist (function functions)
          ;;         (print:format doc "~%~%---~%~a" function)))
          ;;     ;; macros
          ;;     (when macros
          ;;       (cl:dolist (macro macros)
          ;;         (print:string macro doc)))

          ;;     (print:format doc "~%")))
          )))))

(defun sidebar (packages)
  (cl:with-open-file (sidebar "./DOCUMENTATION/_sidebar.md"
                              :direction :output
                              :if-exists :supersede
                              :if-does-not-exist :create)
    (print:string (file:to-string "./DOCUMENTATION/_sidebar.prefix.md")
                  sidebar)
    (cl:dolist (package-data packages)
      (cl:destructuring-bind (package name symbols)
          package-data
        (declare (ignore package symbols))
        (print:format sidebar "~%  - [~a](~a.md)" name name)))))

(defun generate ()
  (let ((packages (package-data-collect)))
    (sequence:map packages #'package-documentation-write nil)
    (sidebar packages))
  (print:format t "~%Done!~%"))

(generate)

;; (cl:maphash (cl:lambda (key value)
;;               (print:format stream:*stdout*
;;                             "~%[~a].~a: ~a"
;;                             (cl:package-name (cl:symbol-package key))
;;                             key
;;                             value))
;;             std.internal::*documentation*)


(arguments-to-string (std.function:args string:to-octets))

;; (sequence:map (std.function:args string:to-octets)
;;               (lambda (item)
;;                 (print:format t "~%type: ~S" item))
;;               'list)

;; (std.function:arg string:to-octets)
;; (sb-introspect:function-lambda-list string:to-octets)
;; (sb-introspect:function-arglist 'cl:char=)
