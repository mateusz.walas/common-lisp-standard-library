# Alternative Standard Library for Common Lisp - batteries included. (Work In Progress)

### Target platform:
64-bit [SBCL](http://www.sbcl.org/) on Linux ([manual](http://www.sbcl.org/manual/))

Portability is not a concern although this project _might_ work on other Common Lisp runtimes just by the virtue of using 3rd party libraries whose authors care about portability.
This project uses a bunch of SBCL-specific functions/macros here and there (`std.internal:*` for example)

### My personal lingo

- when i say `library` I mean `system`
- when i say `namespace` I mean `package`
  - package -> namespace ([there is 1 potential conflict here, to be discussed](https://www.reddit.com/r/Common_Lisp/comments/kylep5/starting_a_batteriesincluded_extended_standard/gjib4nw/?utm_source=reddit&utm_medium=web2x&context=3))

### Goals and design choices
- split `cl:*` into granular libraries with names that make sense
  - `std.concurrency.thread`
  - `std.math`
  - `std.os.process`
  - `std.sequence`
  - `std.sequence.list`
  - `std.string`
  - `std.regex`
  - etc
- 1 file = 1 library = 1 namespace
- unified argument order in functions and macros, unified argument naming, unified variable naming
  - helps with discoverability and makes IDE autocompletion a bit nicer
  - (WIP) `!` means (potential) mutation / side effects
    - I say (potential) because you can use `(std.sequence:slice! "abc" 1)` to just return `"bc"`
  - (WIP) `?` means a boolean return value
    - primarily functions, variables might not adhere to this rule yet
  - unified boolean-returning function names:
    - `namep` -> `name?`
    - `name-p` -> `name?`
  - `+constant+`
    - the standard `cl:*` doesn't follow this (see `std.sequence.array` for examples)
  - `*dynamic-variable*`
  - nouns come first, verbs come last
    - it's easier to organize symbols into (sub)namespaces this way
    - it helps group similar functions together and differentiate them
  - not all naming rules have been decided yet, for example:
    - with-stuff
      - the thing here is that emacs does syntax highlighting with symbols prefixed with `with-...`
      - emacs doesn't necessarily indent those expressions properly as it relies on the actual/original symbol names from `cl:*`
    - dostuff
      - an example would be `cl:dolist` or `cl:dotimes`
      - while I'm fine with `cl:dotimes` as a way to do enumerated looping, I'm not sure whether `cl:dolist` should remain in `std.core:*` or be moved to `std.sequence.list:*`
- unify function/macro argument order
  - consistent argument order (i.e. `(gethash key hashtable)` vs `(aref array index)`)
    - `(std.hashtable:get table key)` and `(std.sequence.array:get array index)` instead of `cl:gethash key hashtable)` and `(cl:aref array index)`
- `std.core:*` is the new 'default' package to `(:use ...)`
  - it replaces `cl:*` and declutters your own namespaces thus giving you more freedom in naming functions
  - it contains a minimal subset of the most crucial symbols from `cl:*` along with a kind of 'pipeline' macro `-->`
    - certain functions are renamed, for example `/=` is now `!=`
- aesthetics
  - (WIP) full words are preferred
  - many (not all) functions have automatically generated symbol macros for them
    - no need to prefix function pointers/objects with `#'`, so
    - instead of `(list:map (list 1 -2 3) #'number:positive?)`
    - you can say `(list:map (list 1 -2 3) number:positive?)`
- dependencies
  - primary goal is to organize the built-in `cl:*`
  - 'batteries-included' approach, filling gaps with 3rd party libraries
    - the approach is to use the (most commonly used | most complete | best documented | fastest) libraries for any given task
  - you are expected to make the conscious decision to include a specific functionality by importing any of the sublibraries of this project as needed
- utility
  - certain functions do not deserve to be in this project because of either atrocious naming, lack of usefulness or simply beacuse there are better alternatives in place
    - this project gets rid of `caaddr` (yes this is a [real function name](http://clhs.lisp.se/Body/f_car_c.htm)) and friends
- documentation
  - (WIP) all `(uiop:define-package ...)` forms contain `:documentation` with a set of recommended local nicknames
  - (WIP) web-based documentation
  - (WIP) all variables, functions, macros have documentation strings with code examples and explanations in plain English
  - (WIP) all namespaces provide a set of recommended local nicknames so you don't have to think too hard
    - (WIP) you can check them by looking at the source code or at the documentation
- CLOS is **not** the preferred way of doing stuff in this project
  - this project is just a bunch of variables, functions, macros and symbol macros
  - performance penalty in general should be treated as a bug

### How to use this project:

```shell
cd ~/common-lisp
git clone https://gitlab.com/mateusz.walas/common-lisp-standard-library.git
```

This project is not available in any of the library repositories like quicklisp.
One of the requirements of quicklisp is for a library to build on at least 2 platforms, and since this project (at least for now) is concerned with only 1 platform, this situation isn't going to change in the foreseeable future.
For now, the recommended way of getting this project is to `git clone` it (or download as .zip and unpack) into `~/common-lisp/std/`.
The `~/common-lisp/` folder is the default place where ASDF looks for libraries.
Then in your own code you can use any of the sublibraries of this project as dependencies in your `.asd` files.
In order to create a namespace (package) you can do it like this:

```common-lisp
(uiop:define-package :app
  (:use :std.core)
  (:local-nicknames (:list :std.sequence.list)
                    (:sequence :std.sequence)
                    (:math :std.math)
                    (:string :std.string)))
(cl:in-package :app)

(-->
  (list "144" "16" "27" "9" "64" )
  (list:map :? string:to-integer)
  (sequence:sort :? <=)
  (list:map :? math:sqrt-integer)
  (list:skip :? 1))
```

- use `(uiop:define-package)` instead of `(cl:defpackage)`
  - (cl:defpackage ...) used to throw an error after changing it and re-evaluating, maybe it's been fixed by now
- use `(:use :std.core)` instead of `(:use :cl)`
  - `:std.core` is a minimal subset of :cl, it has the most crucial symbols like `vector`, `list`, simple arithmetic, `defun` etc
- use `(:local-nicknames ...)` and provide your own aliases for `std.*` namespaces

### Some other examples:

TODO.

## What is wrong with the 'current state' of Common Lisp

Common Lisp's _default standard library_ resides in the `cl:*` namespace and contains **978 symbols**.
Documentation is verbose and repetitive.
Naming is inconsistent and sometimes straight up insane.
Function argument order is inconsistent between similar functions.
Outdated terminology ('system' instead of 'library')

# Scope of functionality

- (library name in parentheses) either covers or extends a given standard functionality
- (?) means no good or adequate library has been found (or selected) to fill the gap
  - 'good' = doesn't force a custom middleware datastructure that you have to convert to and from
    - example: if I want to create UUID string, I want the freaking string, not some CLOS object
    - NOTE: we might still use such library after wrapping it in a sane function/macro call

NOTE: 3rd-party libraries you see here HAVE NOT been fully vetted in terms of source code or license compatibility. Right now (as of 18th January 2021) you are seeing merely a 'potential collection' of libs. Expect this project to become more and more granular with time, so in the end you could either discard a given part in its entirety or even swap it with a 'better' alternative of your liking.

- C foreign function interface (sb-alien, sb-grovel)
- code coverage (sb-cover)
- compression
  - gzip (?)
  - zip ([zip](https://common-lisp.net/project/zip/), ?)
  - zlib ([franzinc/zlib](https://github.com/franzinc/zlib), ?)
- concurrency
  - atomics (sb-ext)
  - channels ([lparallel](https://github.com/lmj/lparallel))
  - compare-and-swap (sb-ext)
  - condition variables (sb-thread)
    - SBCL calls them 'waitqueues'
  - gates (sb-concurrency)
  - intervals (sb-ext)
  - locks a.k.a. mutexes (sb-thread, [bordeaux-threads](https://common-lisp.net/project/bordeaux-threads/))
    - fast read locks (sb-concurrency)
    - locks/mutexes (sb-concurrency)
    - recursive locks/mutexes (sb-concurrency, [bordeaux-threads](https://common-lisp.net/project/bordeaux-threads/))
  - mailboxes (sb-concurrency)
  - memory barriers (sb-thread)
  - queues (sb-concurrency, [lparallel](https://github.com/lmj/lparallel))
  - semaphores (sb-thread, [bordeaux-threads](https://common-lisp.net/project/bordeaux-threads/))
  - threads (sb-thread, [bordeaux-threads](https://common-lisp.net/project/bordeaux-threads/), [lparallel](https://github.com/lmj/lparallel))
  - timeouts (sb-thread)
- crypto
  - hashing
    - md5 (sb-md5)
    - sha\* ([ironclad](https://github.com/sharplispers/ironclad))
    - ...
  - encryption and decryption ([ironclad](https://github.com/sharplispers/ironclad), ?)
  - base64 ([qbase64](https://github.com/chaitanyagupta/qbase64))
  - uuid (2 contenders: [min minh's uuid](https://www.kim-minh.com/pub/uuid/) vs [dardoria/uuid](https://github.com/dardoria/uuid))
  - random number generator (sb-ext)
- databases
  - migrations / seeds ([cl-migratum](https://github.com/dnaeon/cl-migratum))
  - postgres ([postmodern](https://github.com/marijnh/Postmodern))
  - sql ([s-sql](https://marijnhaverbeke.nl/postmodern/s-sql.html))
- data types
  - bits
  - bytes
  - booleans
  - chars
  - numbers
    - integers
    - floats
    - real
    - complex
  - symbols
    - keywords
  - promises ([blackbird](https://github.com/orthecreedence/blackbird))
- data structures
  - sequences
    - arrays
      - vectors
      - bit vectors
    - linked lists
      - alists a.k.a. associative lists
      - plists a.k.a. property lists
      - trees
    - strings ([str](https://github.com/vindarel/cl-str))
  - classes
  - hashtables
  - hashsets ([hashset](https://quickref.common-lisp.net/hash-set.html))
  - byte streams
    - an important note here: these are merely char/number streams, you can't use it like RxJS/RxJava etc
    - broadcast stream
    - bivalent streams
    - concatenated streams
    - echo streams
    - gray streams (sb-gray)
    - input & output streams
    - simple streams
    - synonym streams
    - two way streams
  - structs
- data/file formats
  - json ([jsown](https://github.com/madnificent/jsown))
  - xhtml ([plump (parsing)](https://github.com/Shinmera/plump), [clss (querying)](https://github.com/Shinmera/CLSS), [lquery (querying?)](https://github.com/Shinmera/lquery))
  - csv ([cl-csv](https://github.com/AccelerationNet/cl-csv))
  - toml (2 contenders: [pnathan/pp-toml](https://github.com/pnathan/pp-toml) vs [cxxxr/cl-toml](https://github.com/cxxxr/cl-toml))
  - yaml ([cl-yaml](https://github.com/eudoxia0/cl-yaml))
  - unicode (sb-unicode)
- date & time ([local-time](https://common-lisp.net/project/local-time/), ?)
  - timestamps
- debugger
  - on/off (sb-ext)
  - function call tracing (cl, sb-debug)
- filesystem operations ([uiop](https://common-lisp.net/project/asdf/uiop.html), ?)
  - files
  - directories
  - filepaths
  - namestrings - not sure what's the difference
- functions definitions
  - currying (?) - [see guile scheme for a potential solution](https://www.gnu.org/software/guile/manual/html_node/Curried-Definitions.html)
  - typed arguments (define-fun from std.core, WIP, no support yet for `&key`, `&optional`)
  - generic functions + methods on them
- library manager ([asdf](https://common-lisp.net/project/asdf/), quicklisp)
- lisp virtual machine (SBCL) image ([uiop/image](https://common-lisp.net/project/asdf/uiop.html))
- logging ([logcl](https://github.com/7max/log4cl))
- memory
  - weak pointers (sb-ext)
  - garbage collector hooks and triggering (sb-ext)
  - garbage collector introspection (sb-ext)
- namespaces a.k.a. 'packages' (sb-ext)
- networking
  - sockets (sb-bsd-sockets)
  - http
    - mime (?)
    - client ([dexador](https://github.com/fukamachi/dexador))
    - server ([hunchentoot](http://edicl.github.io/hunchentoot/))
      - opinionated, forces CLOS, dynamic variables, generic methods etc
    - cookies ([hunchentoot](http://edicl.github.io/hunchentoot/), ?)
    - authentication (?)
      - oauth2 (2 contenders (lacking?): [MadEarl/cl-oauth2](https://github.com/MadEarl/cl-oauth2) vs [html/oauth2](https://github.com/html/oauth2))
    - url operations ([quri](https://github.com/fukamachi/quri), ?)
  - websockets ([hunchensocket](https://github.com/joaotavora/hunchensocket))
    - client
    - server
  - payments (?)
  - dns (?)
- pattern matching ([trivia](https://github.com/guicho271828/trivia), ?)
- printing stuff to screen or a stream
  - pretty printing (cl)
- profiling
  - deterministic (sb-profile)
  - statistical (sb-sprof)
- querying environment
  - basic operating system information ([uiop/os](https://common-lisp.net/project/asdf/uiop.html))
  - posix (sb-posix, [uiop](https://common-lisp.net/project/asdf/uiop.html))
- regular expressions ([cl-ppcre](http://edicl.github.io/cl-ppcre/))
- unit testing (3 contenders: [fiveam](https://github.com/lispci/fiveam) vs [prove](https://github.com/fukamachi/prove) vs [rove](https://github.com/fukamachi/rove))
- utility macros
  - note: in common lisp it's easy to start nesting expressions. There are some solutions to this problem like [nightfly19/cl-arrows](https://github.com/nightfly19/cl-arrows) or [eschulte/cl-arrowz](https://github.com/eschulte/cl-arrowz). I personally find these syntactic choices bad (-<>>... &<>>... really?), but perhaps there is a way to customize these libs in an easy way. Otherwise we'll have to hand-roll out own utility macros like these.

## 3rd party dependencies

These libraries are used throughout this project, but they contribute only 1-2 symbols to some namespaces, so I'll put them here instead

- [alexandria](https://common-lisp.net/project/alexandria/)
- [serapeum](https://github.com/ruricolist/serapeum)

## Useful links

- [hyperspec a.k.a. 'the standard documentation'](http://www.lispworks.com/documentation/HyperSpec/Front/)
- [cliki](https://www.cliki.net/)
- [cl libraries at readthedocs.io (short, but useful)](https://common-lisp-libraries.readthedocs.io/)
- [cl cookbook](https://lispcookbook.github.io/cl-cookbook/)
