(asdf:defsystem :std
  :depends-on (:std.internal.doc
               :std.core
               :std.access
               ;; :std.block
               ;; :std.cffi
               ;; :std.cffi.sbcl
               :std.char
               ;; :std.char.unicode
               ;; :std.class
               ;; :std.class.object
               ;; :std.concurrency.atomic
               ;; :std.concurrency.barrier
               ;; :std.concurrency.cas
               ;; :std.concurrency.frlock
               ;; :std.concurrency.gate
               ;; :std.concurrency.mailbox
               ;; :std.concurrency.mutex
               ;; :std.concurrency.queue
               ;; :std.concurrency.semaphore
               :std.concurrency.thread
               ;; :std.concurrency.timeout
               ;; :std.concurrency.timer
               ;; :std.concurrency.waitqueue
               ;; :std.condition
               ;; :std.crypto
               ;; :std.crypto.random
               ;; :std.data.csv
               :std.data.json
               ;; :std.data.xml
               ;; :std.data.yaml
               ;; :std.database.postgres
               ;; :std.database.sql
               ;; :std.debug
               ;; :std.debug.restart
               ;; :std.docstrings
               ;; :std.fs.dir
               :std.fs.file
               ;; :std.fs.file.path
               ;; :std.fs.namestring
               :std.function
               ;; :std.function.generic
               ;; :std.hashset
               :std.hashtable
               ;; :std.io.pprint
               :std.io.print
               ;; :std.io.stream
               ;; :std.let
               ;; :std.lib
               ;; :std.lispvm
               ;; :std.match
               ;; :std.math
               :std.namespace
               ;; :std.net.http.client
               ;; ;; :std.net.http.server
               ;; ;; :std.net.http.server.taskmasters
               ;; :std.net.http.uri
               ;; :std.net.socket
               ;; :std.number.bit
               :std.number.byte
               :std.number.complex
               :std.number.fixnum
               :std.number.float
               :std.number.int
               :std.number
               :std.os
               ;; :std.os.process
               ;; :std.profiler
               :std.regex
               :std.sequence
               :std.sequence.array
               :std.sequence.list
               ;; :std.sequence.list.set
               ;; :std.sequence.alist
               ;; :std.sequence.plist
               ;; ;; TODO: maybe name it std.sequence.list.tree ???
               ;; :std.sequence.tree
               :std.sequence.vector
               ;; :std.sequence.vector.bit
               :std.string
               ;; :std.string.unicode
               :std.symbol
               ;; :std.symbol.keyword
               ;; :std.time.runtime
               ;; :std.time.timestamp
               ;; :std.type
               ;; :std.values
               ;; :std.version
               )
  :components
  ((:file "std")))
