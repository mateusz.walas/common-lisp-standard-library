(cl:defpackage :std.fs.file
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.fs.file)

;; Some of these functions operate on filepaths
;; and some on already opened file streams...

;; the documentation for UIOP functions are terrible
;; safe-read-file-line implicitly uses keyword arguments from read-file-line
;;       which in turn implicitly uses keyword arguments from slurp-stream-line

;; TODO: I need to unify all these
;; - pathspec
;; - pathname - these are the #P"...", i think these expect absolute paths
;; - file
;; - x
;;
;; and then there are filespecs and filepaths too x) jesus...
;; and probably filenames too

(%:fn/macro = (file1 file2 :key (buffer-size 4096)) serapeum:file=)
(%:fn/macro author (pathspec) cl:file-author)
(%:fn compile cl:compile-file)
(%:fn/macro copy (input output) uiop:copy-file)
(%:fn/macro create (pathname mode) sb-posix:creat)
(%:fn/macro eval! (filespec
                   :key
                   ((verbose verbose?) cl:*load-verbose*)
                   ((print print?) cl:*load-print*)
                   (if-does-not-exist cl:t)
                   (external-format :default))
            cl:load)
;; NOTE: 'X'? really?
(%:fn/macro exists? (x) uiop/filesystem:file-exists-p)
(%:fn/macro len-human (file :key flavor space suffix stream) serapeum:file-size-human-readable)
(%:fn/macro rename! (file new-name) cl:rename-file)
(%:fn/macro remove! (file) cl:delete-file)
;; NOTE: 'X'? really?
(%:fn/macro remove-if-exists! (x) uiop:delete-file-if-exists)

(%:fn/macro concatenate-all (inputs output) uiop:concatenate-files)
(%:fn/macro date-write (pathspec) cl:file-write-date)
(%:fn/macro date-write-safe (pathname) uiop:safe-file-write-date)
;; NOTE: it has &allow-other-keys and this function uses a bunch of other functions under the hood
;; I need to look closer at this function to understand exactly what key arguments it expects
(%:fn/macro read-line-safe (pathname :key (package :cl)) uiop:safe-read-file-line)
;; NOTE: it has &allow-other-keys
(%:fn/macro read-line (file :key (at 0)) uiop:read-file-line)

;; NOTE: terrific naming, 'keys'
;; i guess this function also uses other functions with other &KEY arguments
(%:fn/macro to-lines (file :rest keys) uiop:read-file-lines)
(%:fn/macro to-string (file :rest keys) uiop:read-file-string)

;; great, it's expects a freakin STREAM not a filename or even filespec
(%:fn/macro stream-len (stream) cl:file-length)
(%:fn/macro stream-read-line-at (input :key (at 0)) uiop:slurp-stream-line)
;; (%:fn/macro stream-read-line-nth () uiop:slurp-stream-line)
(%:fn/macro stream-read-lines (input :key count) uiop:slurp-stream-lines)
(%:fn/macro stream-open (filename  :key (direction :input) (element-type 'cl:base-char) if-exists if-does-not-exist (external-format :default) (class 'sb-sys:fd-stream)) cl:open)
(%:fn/macro open (filename  :key (direction :input) (element-type 'cl:base-char) if-exists if-does-not-exist (external-format :default) (class 'sb-sys:fd-stream)) cl:open)
(%:macro with-open-stream cl:with-open-file)
(%:macro with-open cl:with-open-file)
(%:fn/macro stream-position (stream :optional (position 0)) cl:file-position)
(%:fn/macro stream-fd ((file-descriptor stream-or-fd)) sb-posix:file-descriptor)

(%:fn/macro write-to-stream (pathname output :key (if-does-not-exist :error) (external-format :default)) serapeum:write-file-into-stream)

;; This is a bad name because we're not creating an entire file from a stream
;; we're simply taking a stream what putting it in a potentially existing file
(%:fn/macro from-stream (stream pathname :key (if-exists :error) if-does-not-exist) serapeum:write-stream-into-file "
The original name might be better even though it's very long.
")
(%:fn/macro /dev/null () uiop:null-device-pathname)


;; ERROR (a.k.a. 'condition')
; (%:fn/macro error cl:file-error) ; this is a condition type
(%:fn/macro error-pathname (condition) cl:file-error-pathname)
