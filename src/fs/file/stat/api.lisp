(cl:defpackage :std.fs.file.stat
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.fs.file.stat)

;; TODO:
;; maybe prefix/suffix namespaces that use posix APIs with 'posix' ???

(%:fn/macro stat (pathname :optional stat) sb-posix:stat)
(%:fn/macro device-id ((object stat)) sb-posix:stat-dev)
(%:fn/macro device-id-special ((object stat)) sb-posix:stat-rdev)
(%:fn/macro inode ((object stat)) sb-posix:stat-ino)
(%:fn/macro inode-last-change ((object stat)) sb-posix:stat-ctime)
(%:fn/macro hard-link-count ((object stat)) sb-posix:stat-nlink)
(%:fn/macro owner-group-id ((object stat)) sb-posix:stat-gid)
(%:fn/macro owner-user-id ((object stat)) sb-posix:stat-uid)
(%:fn/macro time-of-last-access ((object stat)) sb-posix:stat-atime)
(%:fn/macro time-of-modification ((object stat)) sb-posix:stat-mtime)
(%:fn/macro total-size-bytes ((object stat)) sb-posix:stat-size)
(%:fn/macro type-and-mode ((object stat)) sb-posix:stat-mode)

;; TODO: include more posix stuff like chdir, mmap etc, all of it (within reason (i guess))
