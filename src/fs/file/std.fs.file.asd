(asdf:defsystem :std.fs.file
  :depends-on (:std.internal
               :serapeum)
  :components
  ((:file "api")))
