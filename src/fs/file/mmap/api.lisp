(cl:defpackage :std.fs.file.mmap
  (:use)
  (:local-nicknames (:% :std.internal))
  (:documentation "
Suggested local-nickname: __fs.file.mmap__ or __fs.mmap__ or __file.mmap__ or __mmap__
"))
(cl:in-package :std.fs.file.mmap)

(%:fn/macro mmap ((addr address) len prot flags fd offset) sb-posix:mmap)
(%:fn/macro munmap (start length) sb-posix:munmap)
(%:fn/macro msync ((addr address) length flags) sb-posix:msync)

;; (%:fn/macro close sb-posix:close)
;; (%:fn/macro pipe sb-posix:pipe)

(%:var prot-read sb-posix:prot-read)
(%:var prot-write sb-posix:prot-write)
(%:var prot-exec sb-posix:prot-exec)
(%:var prot-none sb-posix:prot-none)

(%:var opt-anon sb-posix:map-anon)
(%:var opt-fixed sb-posix:map-fixed)
(%:var opt-private sb-posix:map-private)
(%:var opt-shared sb-posix:map-shared)

(%:var error-eaccess sb-posix:eacces)
(%:var error-eagain sb-posix:eagain)
(%:var error-ebadf sb-posix:ebadf)
(%:var error-einval sb-posix:einval)
(%:var error-emfile sb-posix:emfile)
(%:var error-enodev sb-posix:enodev)
(%:var error-enomem sb-posix:enomem)
;; (%:var opt-enotsup sb-posix:enotsup) ; missing
(%:var error-enxio sb-posix:enxio)
(%:var error-eoverflow sb-posix:eoverflow)
