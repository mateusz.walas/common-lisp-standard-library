// copied from stdlib/fs/pathname/api.lisp documentation string

Namestring = \"/path/to/something\"
Pathname = #P\"/path/to/something\"
Pathspec = pathname | namestring | file stream (from which the pathname can be extracted)
    - can also be referred to as 'pathname designator'
