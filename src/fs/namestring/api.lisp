(cl:defpackage :std.fs.namestring
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.fs.namestring)

;; TODO: learn what this is exactly and how is it different from filepaths
(%:fn/macro namestring (pathname) cl:namestring)
;; NOTE: terrific naming
;; TODO: this function has both &OPTIONAL and &KEY
;; i'm not touching it with a 10-feet pole until I implement 'coersion' of &OPTIONAL arguments
(%:fn parse cl:parse-namestring)
(%:fn/macro file (pathname) cl:file-namestring)
(%:fn/macro directory (pathname) cl:directory-namestring)
(%:fn/macro host (pathname) cl:host-namestring)
;; choose a better name
(%:fn/macro enough (pathname :optional (defaults cl:*default-pathname-defaults*)) cl:enough-namestring)
