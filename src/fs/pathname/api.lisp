(cl:defpackage :std.fs.pathname
  (:use)
  (:local-nicknames (:% :std.internal))
  (:documentation "
Suggested local-nickname: __fs.path__ or __path__

A little legend:
```
Namestring = \"/path/to/something\"
Pathname = #P\"/path/to/something\"
Pathspec = pathname | namestring | file stream (from which the pathname can be extracted)

Pathspec can also be referred to as 'pathname designator'
```
"))
(cl:in-package :std.fs.pathname)

(%:var *default* cl:*default-pathname-defaults*)

;; namestring = "/path/to/a/file/or/directory"
;; pathname = #P + namestring (conceptually)

;; these expect 'pathspec' instead of regular strings representing filepaths
(%:fn/macro create (:key host device directory name type version defaults (case :local)) cl:make-pathname)
(%:fn/macro ? (object) cl:pathnamep)
(%:fn/macro device ((pathname string-or-pathname) :key (case :local)) cl:pathname-device)
(%:fn/macro dir-as-pathname ((pathname string-or-pathname) :key (case :local)) path:dirname)
;; the 2 here are aliases
(%:fn/macro dir-as-list ((pathname string-or-pathname) :key (case :local)) cl:pathname-directory)
(%:fn/macro dir ((pathname string-or-pathname) :key (case :local)) cl:pathname-directory "
INFO: You can specify a pathname or a string. Either is fine.
INFO: Returns a list of keywords and/or strings (see examples).
WARNING: Slashes are very important here!

(dir #p\"./file.txt\") ; => (:relative \".\")
(dir \"./file.txt\") ; => (:relative \".\")

;; here it thinks that \"project\" is a file without an extension
(dir \"project\") ; => nil
(dir \"./project\") ; => (:relative \".\")
(dir \"project/\") ; => (:relative \"project\")
(dir \"./project/\") ; => (:relative \".\" \"project\")
(dir \"/../file\") ; => (:relative :up \"bin\")
(dir \"/bin/bash\") ; => (:absolute \"bin\")
")
(%:fn/macro file-name ((pathname string-or-pathname) :key (case :local)) cl:pathname-name)
(%:fn/macro file-extension ((pathname string-or-pathname) :key (case :local)) cl:pathname-type)
;; (%:fn/macro host (pathname :key (case :local)) cl:pathname-host)
(%:fn/macro matches? (in-pathname in-wildname) cl:pathname-match-p)
(%:fn/macro version ((pathname string-or-pathname)) cl:pathname-version)
(%:fn/macro concatenate (:rest pathnames) serapeum:path-join)
(%:fn/macro join (:rest pathnames) serapeum:path-join)
(%:fn/macro to-string (pathname) cl:namestring)
(%:fn/macro unlink (pathname) sb-posix:unlink)

;; These functrions use pathSPECS (so either pathname or namestring or a stream)
(%:fn/macro from-pathspec (pathspec) cl:pathname)
(%:fn/macro probe (pathspec) cl:probe-file)
(%:fn/macro absolute (pathspec) cl:truename)
(%:fn/macro logical (pathspec) cl:logical-pathname)

(%:fn/macro root? (pathname) path:root-p)
