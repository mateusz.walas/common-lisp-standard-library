(cl:defpackage :std.fs.dir
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.fs.dir)

(%:fn/macro directory (pathspec :key ((resolve-symlinks resolve-symlinks?) cl:t)) cl:directory)
; not sure what it does exactly, i dont understand the documentation for it
;; NOTE: is pathname-spec supposed to be a PATHSPEC ???
(%:fn/macro directory* (pathname-spec) uiop:directory*)
(%:fn/macro cd ((x directory)) uiop:chdir)
(%:fn/macro home (:optional host) cl:user-homedir-pathname)
(%:fn/macro current () uiop:getcwd)
(%:macro with-current uiop:with-current-directory)
;; TODO: make sure 'VERBOSE' is supposed to be a boolean
(%:fn/macro ensure-path (pathspec :key ((verbose verbose?)) (mode 511)) cl:ensure-directories-exist)
;; NOTE: probably just a alias to 'mkdir -p ...' but I'd have to check
(%:fn/macro ensure-path/uiop (pathnames) uiop:ensure-all-directories-exist) ; not sure what's the difference
(%:fn/macro exists? ((x directory)) uiop:directory-exists-p)
(%:fn/macro files (directory :optional (pattern uiop/pathname:*wild-file-for-directory*)) uiop:directory-files)
;; TODO: check how is it different from sb-ext:delete-directory
;; NOTE: why not just 'DIRECTORY" ? what's with the pathnames? is that the #P"..." thing?
;; TODO: figure out if 'VALIDATE' is supposed to be a boolean
(%:fn/macro remove-tree (directory-pathname :key validate (if-does-not-exist :error)) uiop:delete-directory-tree)
(%:fn/macro remove-if-empty (directory-pathname) uiop:delete-empty-directory)
(%:fn/macro subdirectories (directory (collectp collect?) (recursep recurse?) collector) uiop:collect-sub*directories)
;; (%:macro directory-do uiop:call-with-current-directory)
