(cl:defpackage :std.internal
  (:use)
  (:local-nicknames (:args :std.internal.args))
  (:import-from
   :cl
   :deftype
   :format
   :t
   :nil
   :progn
   :cond
   :error
   :documentation
   :setf
   :loop
   :&body
   :&optional
   :&rest
   :export
   :when
   :if
   :let
   :or
   :and
   :not))
(cl:in-package :std.internal)


(cl:defparameter *documentation?* nil)
(cl:defparameter *documentation* (cl:make-hash-table))

(cl:defun doc-with-alias (doc old)
  (cl:format nil "~%__Uses `~(~a:~a~)` under the hood.__~%~%~a"
             (let ((name (cl:package-name (cl:symbol-package old))))
               (if (cl:string= name "COMMON-LISP")
                   "CL"
                   name))
             old
             (or doc "")))

(cl:defmacro def! (name accessors type)
  `(progn
     (cl:defmacro ,name (exported imported &optional (doc "TODO."))
       `(progn
          (cl:cond
            ,@(loop :for accessor :in ',accessors
                    :collect `((,accessor ',imported)
                               (setf (,accessor ',exported)
                                     (,accessor ',imported))))
            (t (cl:error (cl:format nil
                                    "Missing ~(~a~): ~a."
                                    ',',type
                                    ',imported))))
          (setf (cl:documentation ',exported ',',type)
                (doc-with-alias ,doc ',imported))
          (export ',exported cl:*package*)))
     (export ',name)))

(def! macro (cl:macro-function cl:compiler-macro-function) cl:function)
(def! macro-symbol (cl:macro-function cl:compiler-macro-function) cl:function)
(def! fn (cl:symbol-function) cl:function)
(def! const (cl:symbol-value) cl:variable)

(cl:export
 (cl:defmacro fnbody-export (name args &body body)
   `(cl:block nil
      (cl:defun ,name ,args ,@body)
      (cl:export ',name)
      (cl:define-symbol-macro ,name #',name))))

(cl:defmacro fnbody (name args &body body)
  `(cl:defun ,name ,args ,@body))

(cl:defmacro var (exported imported &optional (doc "TODO."))
  `(progn
     (export (cl:define-symbol-macro ,exported ,imported))
     (setf (cl:documentation ',exported 'cl:variable)
           (doc-with-alias ,doc ',imported))))
(export 'var)

;; (cl:describe 'std.core:list)
(cl:defmacro type (exported imported &optional (doc "TODO.") symbol-macro?)
  ;; "Doesnt handle imported types like (cl:unsigned-byte 62).
  ;;  Had to manually declare it in std.core"
  (let ((args (cl:ignore-errors (sb-introspect:deftype-lambda-list imported))))
    `(progn
       (deftype ,exported (,@args)
         ,(doc-with-alias doc imported)
         ,(if args
              ;; `(,imported ,@(cl:rest args))
              (cl:list* 'cl:list `(cl:quote ,imported)
                        (cl:mapcar (cl:lambda (arg)
                                     (if (cl:consp arg)
                                         (cl:first arg)
                                         arg))
                                   (cl:rest args)))
              ;; ``(,',imported ,,@(cl:rest args))
              `',imported))
       ,(when symbol-macro?
          `(cl:define-symbol-macro ,exported ',imported))
       (export ',exported cl:*package*))))
(export 'type)

(export
 (cl:defun log (message)
   (cl:terpri)
   (cl:format t "Loading module: ~a" message)))

;; (cl:export
;;  (cl:defmacro preload (&rest libraries)
;;    `(cl:eval-when (:compile-toplevel :load-toplevel)
;;       (cl:let ((ns (cl:intern (cl:package-name cl:*package*)
;;                               "KEYWORD")))
;;         (cl:format cl:t "~%ns string: ~a~%" ns)
;;         (cl:in-package :cl)
;;         (ql:quickload ',libraries)
;;         (cl:eval `(cl:in-package ,ns))))))

;; making sure emacs doesn't get retarded because it sees some (cl:in-package ...) in the code...
(cl:in-package :std.internal)

(cl:export
 (cl:defmacro defmacro (name args &body body)
   `(cl:export
     (cl:defmacro ,name ,args ,@body))))

(cl:export
 (cl:defmacro class (target source &optional (doc "TODO."))
   `(progn
      (setf (cl:find-class ',target)
            (cl:find-class ',source))
      (setf (documentation ',target 'cl:type)
            ,(doc-with-alias doc source))
      (export ',target cl:*package*))))

(cl:export
 (cl:defmacro macro-with-args (new args-new old &optional doc options)
   "We assume that args-new is a *full* list of arguments preceding any &* symbol like &rest or &key.
    If you skip any you will get an error."
   (cl:let ((symbol-macro? (and (not (cl:macro-function old))
                                (not (and (cl:find :symbol-macro? options)
                                          (not (cl:getf options :symbol-macro?))))
                                (or (cl:<= 0 (cl:length args-new) 1)
                                    (cl:getf options :symbol-macro?))))
            (args-old (sb-introspect:function-lambda-list old)))
     (cl:list
      'cl:progn
      (when symbol-macro?
        ;; (cl:format t "~%GENERATING SYMBOL MACRO FOR ~a.~%" new)
        `(cl:export
          (cl:define-symbol-macro ,new #',old)))
      (when std.internal::*documentation?*
        `(cl:progn
           (cl:setf (cl:gethash ',new std.internal::*documentation*)
                    (cl:list :args-new ',args-new
                             :args-old ',args-old
                             :source ',old
                             :macro? (cl:macro-function ',old)
                             :symbol-macro? ,symbol-macro?))))
      (cl:with-slots ((old-normal args:normal)
                      (old-optional args:optional)
                      (old-rest args:rest)
                      (old-key args:key)
                      (old-key-other args:key-other))
          (std.internal.args:analyze args-old :old? t)
        (cl:with-slots ((new-normal args:normal)
                        (new-optional args:optional)
                        (new-rest args:rest)
                        (new-key args:key)
                        (new-key-other args:key-other))
            (std.internal.args:analyze args-new)
          `(cl:defmacro ,new (,@(loop :for (name alias) :in new-normal
                                      :collect alias)
                              ,@(when new-optional
                                  (cl:cons 'cl:&optional (cl:mapcar #'args:optional-new new-optional)))
                              ,@(when new-rest
                                  (cl:list 'cl:&rest new-rest))
                              ,@(when new-key
                                  (cl:cons 'cl:&key (cl:mapcar #'args:key-new new-key)))
                              ,@(when new-key-other
                                  'cl:&allow-other-keys))

             ;; documentation string
             ,@(cl:list (doc-with-alias (or doc "") old))

             ;; body - function call
             (cl:append (cl:list ',old
                                 ,@(loop :for old-symbol :in old-normal
                                         :collect (cl:progn
                                                    ;; (cl:format cl:t "~%trying to pair '~(~a~)' with any of '~(~a~)'" old-symbol new-normal)
                                                    (cl:second
                                                     (cl:find (cl:symbol-name old-symbol)
                                                              new-normal
                                                              :test #'cl:string=
                                                              :key #'cl:first)))))
                        ;; we assume original order here
                        ;; might approach it differently once the whole thing is 'setup'
                        ,@(loop :for (name val given? alias) :in new-optional
                                :collect `(when ,given?
                                            (cl:list ,alias)))
                        ,@(cl:list new-rest)
                        ,@(loop :for (name val given? alias) :in new-key
                                :collect `(when ,given?
                                            (cl:list ,(cl:intern (cl:string name) :keyword) ,alias)))
                        ,@new-key-other))))
      `(cl:export ',new)))))

(cl:export
 (cl:defmacro fn/macro (new args-new old &optional doc options)
   `(macro-with-args ,new ,args-new ,old ,doc ,options)))
