(cl:defpackage :std.internal.args
  (:use :cl))
(cl:in-package :std.internal.args)

;; TODO:
;; would be nice to export all the fields alond with the constructors (main, copying) etc...
;; in 1 sweep instead of having to do it manually below (wall of exports)
(defstruct (analysis (:constructor analysis)
                     (:conc-name analysis.))
  (normal nil :type list)
  (optional nil :type list)
  (rest nil :type symbol)
  (key nil :type list)
  (key-other nil :type list))

(export 'analysis)
(export 'analysis.normal)
(export 'analysis.rest)
(export 'analysis.key)
(export 'analysis.key-other)
(export 'analysis.optional)
(export 'normal)
(export 'rest)
(export 'key)
(export 'optional)
(export 'key-other)

(defun arg-with-supplied-check (arg)
  (etypecase arg
    (symbol
     (list arg
           nil
           (intern (format nil "~a???" arg))
           arg))
    (cons
     (or (destructuring-bind (name &optional value-default name???)
             arg
           (destructuring-bind (name alias)
               (if (consp name)
                   name
                   (list name name))
             (list name
                   value-default
                   (or name??? (intern (format nil "~a???" name)))
                   alias)))))))

(export
 (defun analyze (args &key old?)
   (let ((analysis (analysis))
         (segment :normal))

     (with-slots (normal optional rest key key-other)
         analysis
       (dolist (arg args)
         (case arg
           ((:optional cl:&optional) (setf segment :optional))
           ((:rest cl:&rest) (setf segment :rest))
           ((:key cl:&key) (setf segment :key))
           ((:key-other cl:&allow-other-keys) (setf segment :key-other))
           (otherwise
            (case segment
              (:normal
               (push (cond
                       ((consp arg) arg)
                       (old? arg)
                       (t (list arg arg)))
                     normal))
              (:optional
               (push (arg-with-supplied-check arg)
                     optional))
              (:rest
               (setf rest
                     (etypecase arg
                       (cons (second arg))
                       (symbol arg))))
              (:key
               (push (arg-with-supplied-check arg)
                     key))
              (:key-other
               (push arg
                     key-other))))))
       (setf normal (nreverse normal))
       (setf optional (nreverse optional))
       (setf key (nreverse key))
       (setf key-other key-other))
     analysis)))

(export
 (defun optional-new (arg)
   (destructuring-bind (name value-default name??? alias)
       arg
     (declare (ignore name))
     (list alias value-default name???))))

(export
 (defun key-new (arg)
   (optional-new arg)))
