(cl:defpackage :std.namespace
  (:use)
  (:local-nicknames (:% :std.internal))
  (:documentation "Suggested nicknames: __namespace__ or __package__"))
(cl:in-package :std.namespace)

(%:var *current* cl:*package*)

;; TODO: am i even using this anywhere?
(cl:eval-when (:compile-toplevel :load-toplevel)
  (cl:defmacro def (namespace cl:&rest options)
    `(cl:defpackage ,namespace ,@options))
  (cl:export 'def cl:*package*))

;; (%:macro namespace cl:defpackage)
(%:fn/macro create (name :key use nicknames (internal-symbols 10) (external-symbols 10)) cl:make-package)
(%:fn/macro ? (object) cl:packagep)
(%:fn/macro nicknames (package-designator) cl:package-nicknames)
;; what's the difference between actual-package and package-designator???
;; (%:fn/macro shadowing-symbols ((package-designator nicknamed-package)) cl:package-shadowing-symbols)
(%:fn/macro nickname-local-add ((local-nickname nickname) (actual-package nicknamed-package) :optional ((package-designator namespace-designator))) sb-ext:add-package-local-nickname)
(%:fn/macro nickname-local-remove ((old-nickname nickname) :optional ((package-designator namespace-designator))) sb-ext:remove-package-local-nickname)
(%:fn/macro dependencies ((package-designator namespace-designator)) cl:package-use-list)
(%:fn/macro users ((package-designator namespace-designator)) cl:package-used-by-list)
(%:macro enter cl:in-package)
(%:fn/macro find ((package-designator namespace-designator)) cl:find-package)
(%:fn/macro list () cl:list-all-packages)
(%:fn/macro name ((package-designator namespace-designator)) cl:package-name)
(%:fn/macro remove! ((package-designator namespace-designator)) cl:delete-package)
(%:fn/macro rename! ((package-designator namespace-designator) name) cl:rename-package)
(%:fn/macro use (packages-to-use) cl:use-package) ; think of a better name
(%:fn/macro unuse (packages-to-unuse) cl:unuse-package) ; maybe name it 'discard'? 'detach'?

(%:macro with-iterator cl:with-package-iterator)
(cl:export
 (cl:defun symbols (package)
   (cl:loop :for symbol :being :the external-symbols :of package
      :collect symbol)))
(%:macro do-symbols-accessible cl:do-symbols)
(%:macro do-symbols-external cl:do-external-symbols)
(%:macro do-symbols-all cl:do-all-symbols)

(%:fnbody-export current ()
  cl:*package*)

;; (cl:export
;;  (cl:define-symbol-macro *current-name* (name *current*)))
