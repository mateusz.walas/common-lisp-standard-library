(cl:defpackage :std.access
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.access)

(%:fn/macro at (object at) uiop/utility:access-at)
(%:fn/macro at-count (at) uiop/utility:access-at-count)
