(cl:defpackage :std.lib
  (:use)
  (:local-nicknames (:% :std.internal))
  (:documentation "
Suggested local-nicknames: __lisp.library__, __library__, __lib__ (WIP)
"))
(cl:in-package :std.lib)

;; TODO:
;; is there a way to *just* reload dependencies of an already loaded project
;; without reloading/recompiling/re-executing the user code???

;; lib is probably not the best name - think of a better one?
(%:fn help quicklisp:help)
(ql:register-local-projects)
;; there doesn't seem to be an option to *JUST* download
(%:fn add! ql-dist:ensure-installed)
(%:fn load! quicklisp:quickload)
(%:fn remove! quicklisp:uninstall)
(%:fn dependencies quicklisp:who-depends-on)
(%:fn directory quicklisp:where-is-system)
(%:fn find-all quicklisp:system-apropos)
; not sure based on what data this function operates, the return value seems unhelpful
(%:fn list-installed-names quicklisp:list-local-systems)
(%:fn list-installed-systems quicklisp:system-list)
(%:fn list-installed-filepaths quicklisp:list-local-projects)
(%:fn reindex! ql:register-local-projects)

;; TODO: idk where to put it
(%:fn setup-init! quicklisp:add-to-init-file)

;; TODO: maybe put it in std.lib.repo or even std.lib.client.repo ?
;; meh, idk, sounds like an overkill.
(%:fn repo-add! ql-dist:install-dist)
(%:fn repo-remove! quicklisp:uninstall-dist)
(%:fn repo-update! quicklisp:update-dist)
(%:fn repo-update-all! quicklisp:update-all-dists)
(%:fn repo-url quicklisp:dist-url)
(%:fn repo-version quicklisp:dist-version)
(%:fn repo-versions quicklisp:available-dist-versions)
;; TODO: go through the rest of quicklisp:*
;; TODO: go through ql-dist ?


;; TODO:
;; go through asdf:* and use the most useful stuff
;; no point pulling all of it
;; if I need any special functionality i'll use asdf:* directly
;; (quicklisp:where-is-system :alexandria)
;; (asdf:locate-system :alexandria)

;; TODO:
;; there is also XCVB on the horizon
;; https://xcvb.common-lisp.dev/doc/README.html
