(cl:defpackage :std.lib.client
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.lib.client)

(%:fn update! quicklisp:update-client) ; no idea what 'client' is supposed to be
(%:fn url quicklisp:client-url)
(%:fn version quicklisp:client-url)
(%:fn version-list quicklisp:available-client-versions)
