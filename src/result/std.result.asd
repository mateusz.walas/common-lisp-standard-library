(asdf:defsystem :std.result
  :depends-on (:std.internal
               :std.core
               :std.function
               :std.match
               :std.option
               :std.type)
  :components
  ((:file "api")))
