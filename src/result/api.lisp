(cl:defpackage :std.result
  (:use :std.core)
  (:import-from :cl :signed-byte)
  (:local-nicknames (:% :std.internal)
                    (:function :std.function)
                    (:match :std.match)
                    (:option :std.option)
                    (:type :std.type))
  (:documentation "
Suggested local-nicknames: __option__
"))
(cl:in-package :std.result)

(export*
  (defunion result ()
    (ok value)
    (err message)))

;; (setf trivia.level2:*arity-check-by-test-call* nil)

;; (export*
;;   (define-fun ok? ((result result))
;;     (:returns bool)
;;     (cl:typep result 'ok)))

;; (export*
;;   (define-fun err? ((result result))
;;     (:returns bool)
;;     (cl:typep result 'err)))

(define-fun bind ((result result) (binder (function (t) result)))
  (:returns result)
  (match:pattern result
    ((ok value) (values (function:call binder value)))
    (_ result)))

(declaim (ftype (function (result t &key (:comparison (function (t t) bool)))) contains?))
(export*
  (defun contains? (result val &key (comparison equal))
    (declare (type result result))
    (declare (type t val))
    (declare (type (function (t t) bool) comparison))
    ;; (when (ok? result)
    ;;   (values (function:call comparison value val)))
    (match:pattern result
      ((ok value) (values (function:call comparison value val)))
      (_ nil))))

(export*
  (define-fun count ((result result))
    (:returns uint)
    (if (ok? result) 1 0)))

(export*
  (define-fun or-value ((result result) default-value)
    (:returns t)
    (match:pattern result
      ((ok value) value)
      (_ default-value))))

(export*
  (define-fun or-function ((result result) (default-getter (function () t)))
    (:returns t)
    (match:pattern result
      ((ok value) value)
      (_ (function:call default-getter)))))

(export*
  (define-fun exists? ((result result))
    (:returns bool)
    (values (ok? result))))

(export*
  (define-fun fold ((result result) state (folder (function (t t) t)))
    (:returns t)
    ;; (if (ok? value)
    ;;     (function:call folder state value)
    ;;     state)
    (match:pattern result
      ((ok value) (function:call folder state value))
      (_ state))))

(declaim (ftype (function (result (function (t) bool) &key (:value-if-err bool))) every))
(export*
  (defun every (result test &key (value-if-err t))
    (match:pattern result
      ((ok value) (values (function:call test value)))
      (_ value-if-err))))

(export*
  (define-fun do ((result result) (action (function (t) t)))
    (:returns nil)
    (match:pattern result
      ((ok value) (function:call action value)))
    result))

(export*
  (define-fun map ((result result) (mapper (function (t) t)))
    (:returns result)
    (match:pattern result
      ((ok value) (ok (function:call mapper value)))
      (_ result))))

(export*
  (define-fun map-error ((result result) (mapper (function (t) t)))
    (:returns result)
    (match:pattern result
      ((err message) (err (function:call mapper message)))
      (_ result))))

(export*
  (define-fun to-list ((result result))
    (:returns list)
    (match:pattern result
      ((ok value) (list value))
      (_ (list)))))

(export*
  (define-fun to-option ((result result))
    (:returns option:option)
    (match:pattern result
      ((ok value) (option:some value))
      (_ (option:none)))))
