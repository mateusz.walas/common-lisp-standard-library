(cl:defpackage :std.io.pprint
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.io.pprint)

;; TODO: apparently these functions are not used that often (or at all)
;; maybe we can just take 1 or 2, move them to another package and get rid of the rest ???

(%:fn/macro pprint (object :optional stream) cl:pprint)
(%:fn/macro dispatch (object :optional (table cl:*print-pprint-dispatch*)) cl:pprint-dispatch)
(%:fn/macro dispatch-set! (type function :optional (priority 0) (table cl:*print-pprint-dispatch*)) cl:set-pprint-dispatch)
(%:fn/macro dispatch-copy (:optional (table cl:*print-pprint-dispatch*)) cl:copy-pprint-dispatch)

;; NOTE: notice how these booleans arguments actually have the ? suffix. Crazy.
;; must be one of the older functions from before they switched to that stupid 'p' or '-p' suffix
;; or maybe not, who knows
(%:fn/macro fill (stream list :optional (colon? cl:t) atsign?) cl:pprint-fill)
(%:fn/macro linear (stream list :optional (colon? cl:t) atsign?) cl:pprint-linear)
(%:fn/macro tabular (stream list :optional (colon? cl:t) atsign? tabsize) cl:pprint-tabular)
;; TODO: what tf is 'colinc' supposed to mean ??? probably something about incrementing
(%:fn/macro tab (kind (colnum column-number) (colinc) :optional stream) cl:pprint-tab)
(%:fn/macro indent (relative-to n :optional stream) cl:pprint-indent)
(%:macro logical-block cl:pprint-logical-block)
(%:fn/macro newline (kind :optional stream) cl:pprint-newline)
(%:macro pop cl:pprint-pop)
