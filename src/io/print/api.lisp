(cl:defpackage :std.io.print
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.io.print)

;; TODO: some of these are probably booleans
;; so add the ? suffix to those
(%:var *array* cl:*print-array*)
(%:var *base* cl:*print-base*)
(%:var *case* cl:*print-case* "
Accepted values:
- :upcase
- :downcase
- :capitalize
")
(%:var *circle* cl:*print-circle*)
(%:var *escape* cl:*print-escape*)
(%:var *gensym* cl:*print-gensym*)
(%:var *length* cl:*print-length*)
(%:var *level* cl:*print-level*)
(%:var *lines* cl:*print-lines*)
(%:var *miser-width* cl:*print-miser-width*)
(%:var *pprint-dispatch* cl:*print-pprint-dispatch*)
(%:var *pretty* cl:*print-pretty*)
(%:var *radix* cl:*print-radix*)
(%:var *readably* cl:*print-readably*)
(%:var *read-suppress* cl:*read-suppress*)
(%:var *right-margin* cl:*print-right-margin*)
(%:var *suppress-print-errors* sb-ext:*suppress-print-errors*)

;; TODO:
;; i left some of the functions here as %:fn
;; because I haven't implemented yet forcing &OPTIONAL arguments into the NORMAL argument list
;; but that's what will have to be done here because the argument lists in some of these functions
;; are like this: (object &optional stream :key (start 0) end)
;; if people mostly print stuff to stdout then why was it so hard to just make the stream a &key argument if it's optional anyway?
;; key arguments are obviously optional, they just require the appropriate keyword before them
;; (print:object some-object)
;; (print:object some-object :stream some-stream)

;; maybe wrap these functions so that we always pass cl:t or cl:*standard-output* as the output stream

;; TODO: move it to std.io.stream maybe?
;; I'm really tempted to get rid of all these &KEY arguments
;;
(%:fn/macro write (object
                   :key
                   stream
                   ;; (array (cl:macroexpand-1 *array*))
                   ;; (base (cl:macroexpand-1 *base*))
                   ;; (case (cl:macroexpand-1 *case*))
                   ;; (circle (cl:macroexpand-1 *circle*))
                   ;; (escape (cl:macroexpand-1 *escape*))
                   ;; (gensym (cl:macroexpand-1 *gensym*))
                   ;; (length (cl:macroexpand-1 *length*))
                   ;; (level (cl:macroexpand-1 *level*))
                   ;; (lines (cl:macroexpand-1 *lines*))
                   ;; (miser-width (cl:macroexpand-1 *miser-width*))
                   ;; (pprint-dispatch (cl:macroexpand-1 *pprint-dispatch*))
                   ;; (pretty (cl:macroexpand-1 *pretty*))
                   ;; (radix (cl:macroexpand-1 *radix*))
                   ;; (readably (cl:macroexpand-1 *readably*))
                   ;; (right-margin (cl:macroexpand-1 *right-margin*))
                   ;; (suppress-print-errors (cl:macroexpand-1 *suppress-print-errors*))
                   )
            cl:write)
;; arguments are the same as in (write ...)
(%:fn/macro write-to-string (object
                             ;; :key
                             ;; (array (cl:macroexpand-1 *array*))
                             ;; (base (cl:macroexpand-1 *base*))
                             ;; (case (cl:macroexpand-1 *case*))
                             ;; (circle (cl:macroexpand-1 *circle*))
                             ;; (escape (cl:macroexpand-1 *escape*))
                             ;; (gensym (cl:macroexpand-1 *gensym*))
                             ;; (length (cl:macroexpand-1 *length*))
                             ;; (level (cl:macroexpand-1 *level*))
                             ;; (lines (cl:macroexpand-1 *lines*))
                             ;; (miser-width (cl:macroexpand-1 *miser-width*))
                             ;; (pprint-dispatch (cl:macroexpand-1 *pprint-dispatch*))
                             ;; (pretty (cl:macroexpand-1 *pretty*))
                             ;; (radix (cl:macroexpand-1 *radix*))
                             ;; (readably (cl:macroexpand-1 *readably*))
                             ;; (right-margin (cl:macroexpand-1 *right-margin*))
                             ;; (suppress-print-errors (cl:macroexpand-1 *suppress-print-errors*))
                             )
            cl:write-to-string)
;; NOTE: it has both &OPTIONAL and &KEY
(%:fn line cl:write-line)
;; TODO: probably need to customize fn further
;; to allow fully customized argument lists
;; and take them as the original (old)
;; this function has some bullshit going on &optional followed by &key
(%:fn string cl:write-string)
;; TODO: need to revise these names, decide how to name these...
;; TODO: how is it different from just (cl:write object) ???
(%:fn/macro object (stream object) cl:print-object)
(%:macro object-unreadable cl:print-unreadable-object)
(%:fn/macro for-read (object :optional stream) cl:prin1)
(%:fn/macro for-read-to-string (object) cl:prin1-to-string)
(%:fn/macro for-read-padded (object :optional stream) cl:print)

(%:fn/macro for-human (object :optional stream) cl:princ)
(%:fn/macro for-human-to-string (object) cl:princ-to-string)

;; TODO: maybe name the 'stream' an 'output-stream' even?
(%:fn/macro format (destination (control-string format-string) :rest (format-arguments format-args)) cl:format "

The DESTINATION argument can be a:
- nil    (which simply returns the formatted string)
- t      (it's an alias for stdout)
- stream (a stream to write the formatted string to, obviously)
- string (it must have a fill pointer)

```lisp
; uppercase (default) vs lowercase
(format nil \"~a\" 'symbol)     ; => \"SYMBOL\"
(format nil \"~(~a~)\" 'symbol) ; => \"symbol\"

(format nil \"~a\"     \"heLLo\") ; => \"heLLo\"
(format nil \"~a\"     \"HELLO\") ; => \"HELLO\"
(format nil \"~(~a~)\" \"HELLO\") ; => \"hello\"

(format nil \"~$\" 1.23456) => \"1.23\" (default is 2 decimal places)

(format nil \"~3$\"  0.12345) ; => \"0.123\"
(format nil \"~5$\"  0.12345) ; => \"0.12345\"
(format nil \"~10$\" 0.12345) ; => \"0.1234500000\"

(let ((pattern \"~v$\"))
  (format nil pattern 3  1.23456)  ; => \"1.234\"
  (format nil pattern 5  1.23456)  ; => \"1.23456\"
  (format nil pattern 10 1.23456)) ; => \"1.2345600000\"

(let ((pattern \"~#$\"))
  (format nil pattern 0.12345)                 ; => \"0.1\"
  (format nil pattern 0.12345 arg2)            ; => \"0.12\"
  (format nil pattern 0.12345 arg2 arg3)       ; => \"0.123\"
  (format nil pattern 0.12345 arg2 arg3 arg4)) ; => \"0.1234\"

(format nil \"~,3f\" 123.456) ; => \"123.456\"

; Comma separates modifiers
(format nil \"~10,2f\" 123.456) ; => \"    123.46\"  (10 chars in total, padded with spaces)

(format nil \"~d\" 1000)     ; => \"1000\"
(format nil \"~:d\" 1000)    ; => \"1,000\"
(format nil \"~:d\" 1000000) ; => \"1,000,000\"
(format nil \"~@d\" 1000)    ; => \"+1000\"
(format nil \"~10,'ad\" 666) ; => \"aaaaaaa666\" (the 'a part changes the padding char)

; Combining the modifiers together (order doesn't matter):
(format nil \"~:@d\" 1000000) ; => \"+1,000,000\"
(format nil \"~@:d\" 1000000) ; => \"+1,000,000\"

(format nil \"~a\" 'cl:+) ; => \"+\"
(format nil \"~s\" 'cl:+) ; => \"COMMON-LISP:+\"

(format nil \"~a\" \"hello\") ; => \"hello\"
(format nil \"~s\" \"hello\") ; => \"\"hello\"\" (7 chars, the inner quotes are escaped)

(format nil \"~a\" nil)  ; => \"nil\"
(format nil \"~s\" nil)  ; => \"nil\"
(format nil \"~:a\" nil) ; => \"()\"
(format nil \"~:s\" nil) ; => \"()\"

#|
~% means newline (always)
~& means fresh line (does not duplicate newlines)
So for example ~%~& will print only 1 newline.

~5% means 5 newlines (always)
~5& means 5 newlines (at most - see previous note)

~~ prints a tilde
~5~ prints 5 tildes
|#

(format nil \"~:c\" #\\a) ; => \"a\"
(format nil \"~@c\" #\\a) ; => \"#\\a\"

#|
Formatting dates in a fixed-width format:
Conceptually: <2 digits>-<2 digits>-<4 digits>
Each part is an integer, so we'll use ~d
Each part will be padded to a given length (2 or 4)
Each part will be padded with 0 (since we're dealing with numbers).
Padding to 2 digits with zeros: ~2,'0d
Padding to 4 digits with zeros: ~2,'0d
So to display a date like day: 3, month: 5, year: 2021
|#
(print-format nil \"~2,'0d-~2,'0d-~d\" 3 5 2021) ; => \"03-05-2021\"

(format nil \"~x\" 255) ; => \"FF\" (hexadecimal)
(format nil \"~o\" 255) ; => \"377\" (octal)
(format nil \"~b\" 255) ; => \"11111111\" (binary)

(let ((pattern \"~r\"))
  (format nil pattern 1)    ; => \"one\"
  (format nil pattern 2)    ; => \"two\"
  (format nil pattern 3)    ; => \"three\"
  (format nil pattern 123)) ; => \"one hundred twenty-three\"

(let ((pattern \"~:r\"))
  (format nil pattern 1)    ; => \"first\"
  (format nil pattern 2)    ; => \"second\"
  (format nil pattern 3)    ; => \"third\"
  (format nil pattern 123)) ; => \"one hundred twenty-third\"

(let ((pattern \"~@r\"))
  (format nil pattern 1)   ; => \"I\"
  (format nil pattern 2)   ; => \"II\"
  (format nil pattern 3)   ; => \"III\"
  (format nil pattern 5)   ; => \"V\"
  (format nil pattern 12)) ; => \"XII\"

; The next 2 groups with ~p and ~:p merely add 's' if the number is not 1:
; So, while it works for words that require only 's' for plural form:
(let ((pattern \"cat~p\"))
  (format nil pattern 1)  ; => \"apple\"
  (format nil pattern 2)  ; => \"apples\"
  (format nil pattern 0)) ; => \"apples\"

; With preceding word processed too:
(let ((pattern \"~r cat~:p\"))
  (format nil pattern 1)  ; => \"one apple\"
  (format nil pattern 2)  ; => \"two apples\"
  (format nil pattern 0)) ; => \"zero apples\"

; ... in order to have let's say 'fly' changed to 'flies' you need to use ~:@p instead
(let ((pattern \"~r fruit fl~:@p\"))
  (format nil pattern 1)  ; => \"one fruit fly\"
  (format nil pattern 2)) ; => \"two fruit flies\"

; Use ~( and ~) to group things:
(let ((str \"tHiS iS a CaT\"))
  (format nil \"~(~a~)\" str)   ; => \"this is a cat\" (everything lowercase)
  (format nil \"~@(~a~)\" str)  ; => \"This is a cat\" (capitalized only 1-st word)
  (format nil \"~:(~a~)\" str)  ; => \"This Is A Cat\" (capitalized every word)
  (format nil \"~:@(~a~)\" str) ; => \"THIS IS A CAT\" (everything uppercase)

; Use ~[ and ~] to select by index.
Treat is like array[index] lookup from languages like C or js (except the weird ~ prefix for each bracket):
(let ((pattern \"~[cat~;dog~;cookie~]\"))
  (format nil pattern 0)   ; => \"cat\"
  (format nil pattern 1)   ; => \"dog\"
  (format nil pattern 2)   ; => \"cookie\"
  (format nil pattern 50)) ; => \"\" (no 'default' match specified)

; Notice the ':' inside the last '~;'
(let ((pattern \"~[cat~;dog~:;cookie~]\"))
                           ^
  (format nil pattern 0)  ; => \"cat\"
  (format nil pattern 1)  ; => \"dog\"
  (format nil pattern 2)  ; => \"cookie\"
  (format nil pattern 50) ; => \"cookie\" ('default' match)

; Use ~:[ and ~] to select one of 2 'if branches':
(let ((pattern \"~:[yes~:no~]\"))
  (format nil pattern t)    ; => \"yes\"
  (format nil pattern nil)) ; => \"no\"

; Use ~@[ and ~] to treat it like a single 'if' condition:
(let ((pattern \"~@[a = ~a~], ~@[b = ~a~]\"))
  (format nil pattern 5 10)     ; => \"a = 5, b = 10\"
  (format nil pattern nil 10)   ; => \", b = 10\"
  (format nil pattern 5 nil)    ; => \"a = 5, \"
  (format nil pattern nil nil)) ; => \", \"

; It doesn't matter how many arguments you pass, it only takes the first 2 (because there are only 2 conditions):
; If you put a bunch of nils at the beginning - it's totally 'fine' as far as this function is concerned:
(let ((pattern \"~@[a = ~a~], ~@[b = ~a~]\"))
  (format nil pattern 1 2 3 nil nil)  ; => \"a = 1, b = 2\"
  (format nil pattern nil nil 1 2 3)) ; => \", \"

; 100 IQ loop:
(format nil \"~{~a, ~}\" (list 1 2 3) ; => \"1, 2, 3, \"

; 105 IQ loop (notice the ~^ signifying 'when there are no more items, end HERE'):
(format nil \"~{~a~^, ~}\" (list 1 2 3) ; => \"1, 2, 3\"

; Inlining arguments instead of collecting them in to a list:
(format nil \"~@{~a~^, ~}\" 1 2 3) ; => \"1, 2, 3\"

(let ((pattern \"(~#[~;~a~:;~a, ~a~]~#[~:;, ...other elements~])\"))
  (format nil pattern)        ; => \"()\"
  (format nil pattern 1)      ; => \"(1)\"
  (format nil pattern 1 2)    ; => \"(1, 2)\"
  (format nil pattern 1 2 3)) ; => \"(1, 2, ...other elements)\"

; TODO: document ~* ~:* ~? ~/ and possible more (no idea how many more there are...)

; Reuse previous argument:
(format nil \"~r ~:*(~d)\" 5) ; => \"five (5)\"

; In a loop ~* skips 1 (by default) element
(format nil \"~{~a~*~^ ~}\" (list \"a\" 1 \"b\" 2 \"c\" 3) ; => \"a b c\"

; You can specify how many elements to skip
(format nil \"~{~a~2*~^ ~}\" (list \"a\" 1 2 \"b\" 3 4 \"c\" 5 6) ; => \"a b c\"

; TODO: finish it...
; ... this 'format' function is the embodiment of insanity.
```
")
