(cl:defpackage :std.io.stream.string.input
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.io.stream.string.input)

(%:fn/macro create (string :optional (start 0) end) cl:make-string-input-stream)
