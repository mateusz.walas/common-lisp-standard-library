(cl:defpackage :std.io.stream.concatenated
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.io.stream.concatenated)

(%:fn/macro create (:rest streams) cl:make-concatenated-stream)
(%:fn/macro streams ((instance concatenated-stream)) cl:concatenated-stream-streams)
