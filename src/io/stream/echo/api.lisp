(cl:defpackage :std.io.stream.echo
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.io.stream.echo)

(%:fn/macro create (input-stream output-stream) cl:make-echo-stream)
(%:fn/macro input ((instance echo-stream)) cl:echo-stream-input-stream)
(%:fn/macro output ((instance echo-stream)) cl:echo-stream-output-stream)
