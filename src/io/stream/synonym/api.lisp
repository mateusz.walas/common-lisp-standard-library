(cl:defpackage :std.io.stream.synonym
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.io.stream.synonym)

(%:fn/macro create (symbol) cl:make-synonym-stream)
(%:fn/macro synonym-symbol ((instance synonym-stream)) cl:synonym-stream-symbol)
