(asdf:defsystem :std.io.stream
  :depends-on (:std.internal
               :serapeum)
  :components
  ((:file "api")))
