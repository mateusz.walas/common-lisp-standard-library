(cl:defpackage :std.io.stream.duplex
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.io.stream.duplex)

(%:fn/macro create (input-stream output-stream) cl:make-two-way-stream)
(%:fn/macro input ((instance duplex-stream)) cl:two-way-stream-input-stream)
(%:fn/macro output ((instance duplex-stream)) cl:two-way-stream-output-stream)
