(cl:defpackage :std.io.stream.broadcast
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.io.stream.broadcast)

(%:fn/macro create (:rest streams) cl:make-broadcast-stream)
(%:fn/macro streams ((instance broadcast-stream)) cl:broadcast-stream-streams)
