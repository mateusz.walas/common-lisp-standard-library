(cl:defpackage :std.io.stream.string.output
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.io.stream.string.output)

;; TODO
;; not sure about the rename element-type to type but I'm leaving it like this for now
;; and not just in this package, a few more too
(%:fn/macro create (:key ((element-type type) 'cl:character)) cl:make-string-output-stream)
