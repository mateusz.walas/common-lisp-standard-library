(cl:defpackage :std.concurrency.cas
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.concurrency.cas)

(%:macro-with-args cas (place old new) sb-ext:compare-and-swap)
(%:macro-with-args compare-and-swap (place old new) sb-ext:compare-and-swap)
