(cl:defpackage :std.concurrency.barrier
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.concurrency.barrier)

(%:macro memory-barrier sb-thread:barrier)
