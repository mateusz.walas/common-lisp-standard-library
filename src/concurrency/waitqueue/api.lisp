(cl:defpackage :std.concurrency.waitqueue
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.concurrency.waitqueue)

;; a.k.a. Condition Variables

(%:macro-with-args create () sb-thread:make-waitqueue)
(%:macro-with-args name ((instance waitqueue)) sb-thread:waitqueue-name)
;; no thread-condition-var? function in the :bt library
(%:macro-with-args broadcast ((queue waitqueue)) sb-thread:condition-broadcast)
(%:macro-with-args notify ((queue waitqueue)) sb-thread:condition-notify)
(%:macro-with-args wait ((queue waitqueue) mutex) sb-thread:condition-wait)
