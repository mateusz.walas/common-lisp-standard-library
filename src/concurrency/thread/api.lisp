(cl:defpackage :std.concurrency.thread
  (:use)
  (:local-nicknames (:% :std.internal))
  (:documentation "
Suggested local-nickname: __thread__
"))
(cl:in-package :std.concurrency.thread)

(%:type thread sb-thread:thread)

;; maybe call it *thread-thread* so that
;; when this file is put in its own namespace thread:*
;; the variable will be called *thread* instead of *current*
(%:var *current* sb-thread:*current-thread*)
;; TODO:
;; decide what to do with these
;; they come from bordeaux-threads:
;; - *default-special-bindings*
;; - *standard-io-bindings*
;; - *supports-threads-p*

(%:fn/macro create (function) sb-thread:make-thread)
(%:fn/macro ? (object) bt:threadp)
(%:fn/macro name (thread) sb-thread:thread-name)
(%:fn/macro alive? (thread) sb-thread:thread-alive-p)
(%:fn/macro list () sb-thread:list-all-threads)
(%:fn/macro main () sb-thread:main-thread)
(%:fn/macro main? () sb-thread:main-thread-p)

(cl:eval-when (:compile-toplevel :load-toplevel :execute)
 (cl:defun current% ()
   sb-thread:*current-thread*)

 (%:fn/macro current () std.concurrency.thread::current% "
It seems like there is no function that does it so we defined our own.
"))

(%:macro return sb-thread:return-from-thread)
(%:fn/macro interrupt (thread function) sb-thread:interrupt-thread)
(%:fn/macro abort () sb-thread:abort-thread)
(%:fn/macro destroy (thread) sb-thread:destroy-thread)
(%:fn/macro terminate (thread) sb-thread:terminate-thread)
(%:fn/macro wait (thread) sb-thread:join-thread)
(%:fn/macro join (thread) sb-thread:join-thread)
(%:fn/macro yield () sb-thread:thread-yield)
(%:fn/macro symbol-value (thread symbol) sb-thread:symbol-value-in-thread)
;; TODO: missing conditions here
(%:fn/macro error-thread (condition) sb-thread:thread-error-thread)
