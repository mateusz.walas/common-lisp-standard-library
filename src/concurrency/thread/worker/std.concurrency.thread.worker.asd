(asdf:defsystem :std.concurrency.thread.worker
  :depends-on (:std.core
               :std.class.object
               :std.function
               :log4cl)
  :components
  ((:file "data")
   (:file "api")))
