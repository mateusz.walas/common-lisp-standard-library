(cl:defpackage :std.concurrency.thread.worker
  (:use :std.core)
  (:local-nicknames (:function :std.function)))
(cl:in-package :std.concurrency.thread.worker)

(export*
  (defstruct (worker (:constructor new (&key
                                          name
                                          create
                                          health-check
                                          start
                                          stop
                                          up
                                          down))
                     (:copier nil)
                     (:conc-name)
                     (:predicate ?))
    (name nil :type string)
    object
    ;; (keep-alive? nil :type string)
    (create nil :type function)
    (zombie-check nil :type (or null (function (t) bool)))
    (health-check nil :type (or null (function (t) bool)))
    (start nil :type (or null (function (t))))
    (stop nil :type (or null (function (t))))
    (up nil :type (or null (function (t))))
    (down nil :type (or null (function (t))))))
