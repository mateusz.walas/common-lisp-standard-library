(cl:defpackage :std.concurrency.thread.worker.api
  (:use :std.core)
  (:import-from :std.concurrency.thread.worker :worker)
  (:local-nicknames (:function :std.function)
                    (:object :std.class.object)
                    (:worker :std.concurrency.thread.worker)))
(cl:in-package :std.concurrency.thread.worker.api)

(define-fun up ((worker worker))
  (object:with-slots ((object worker:object)
                      (create worker:create)
                      (name worker:name)
                      (health-check worker:health-check)
                      (start worker:start))
      worker

    (handler-case
        (block up
          (unless object
            (setf object (setf object (function:call create)))
            (log:debug "~a :: created ... OK" name))

          (when start
            (log:debug "~a :: starting ..." name)
            (let ((message (function:call start object)))
              (when message
                (log:debug "~a :: stared with message '~a'" name message)))
            (log:debug "~a :: started ... OK" name))

          (when health-check
            (assert (function:call health-check object))
            (log:debug "~a :: startup check ... OK" name)))
      (error (e)
        (log:error "~a :: startup ERROR: ~a" name e)))))

(define-fun up? ((worker worker))
  (function:call (worker:health-check worker) (worker:object worker)))

(define-fun down ((worker worker))
  (object:with-slots ((object worker:object)
                      (name worker:name)
                      (health-check worker:health-check)
                      (stop worker:stop)
                      (zombie-check worker:zombie-check))
      worker

    (when zombie-check
      (log:debug "~a :: zombie-check exists ... IN PROGRESS" name)
      (when (not (function:call zombie-check worker))
        (log:error "~a :: zombie-check failed ... ERROR." name)
        (return-from down))
      (log:debug "~a :: zombie-check passed ... OK" name))

    (when (not object)
      (log:debug "~a  :: already down ... OK" name)
      (return-from down))

    (when (function:call health-check object)
      (log:debug "~a :: shutting down ... IN PROGRESS" name)
      (handler-case
          (block shutdown
            (function:call stop object)
            (setf object nil)
            (log:debug "~a :: stopped ... OK" name))
        (error (e)
          (log:error "~a :: shutdown ERROR: ~a" name e))))))
