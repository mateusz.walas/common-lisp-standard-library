(asdf:defsystem :std.concurrency.thread
  :depends-on (:std.internal
               :bordeaux-threads)
  :components
  ((:file "api")))
