(cl:defpackage :std.concurrency.timeout
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.concurrency.timeout)

;; TODO: decide if sync/async timeouts deserve their own files

;; TODO: is there a way to create a 'timeout object'?
;; TODO: add the timeout condition ?
(%:macro wait-for sb-ext:wait-for) ; not sure how else to call it
(%:macro with-timeout-async sb-ext:with-timeout "
expires - in seconds
")
(%:macro deadline-do sb-sys:with-deadline)
;; deadline-timeout is a condition, not a function
;; (%:macro deadline-timeout sb-sys:deadline-timeout)
