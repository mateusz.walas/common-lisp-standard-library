(cl:defpackage :std.concurrency.timer
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.concurrency.timer)

;; TODO: is this even a multithreading thing in particular?
(%:macro-with-args create (function :key name thread) sb-ext:make-timer)
(%:macro-with-args name (timer) sb-ext:timer-name)
(%:macro-with-args schedule! (timer (time seconds)) sb-ext:schedule-timer)
(%:macro-with-args scheduled? (timer) sb-ext:timer-scheduled-p)
(%:macro-with-args cancel! (timer) sb-ext:unschedule-timer)
(%:macro-with-args list () sb-ext:list-all-timers)
