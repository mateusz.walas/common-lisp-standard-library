(cl:defpackage :std.concurrency.queue
  (:use)
  (:import-from :sb-concurrency :queue)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.concurrency.queue)

(cl:export 'queue)
(%:fn/macro create (:key name initial-contents) sb-concurrency:make-queue)
(%:fn/macro ? (object) sb-concurrency:queuep)
(%:fn/macro empty? (queue) sb-concurrency:queue-empty-p)
(%:fn/macro size (queue) sb-concurrency:queue-count) ; count vs size vs len vs ?
(%:fn/macro entries (queue) sb-concurrency:list-queue-contents)
(%:fn/macro name ((instance queue)) sb-concurrency:queue-name)
(%:fn/macro write! (queue value) sb-concurrency:enqueue)
(%:fn/macro read! (queue) sb-concurrency:dequeue)
