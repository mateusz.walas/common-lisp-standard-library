(asdf:defsystem :std.concurrency.queue
  :depends-on (:std.internal
               :sb-concurrency)
  :components
  ((:file "api")))
