(cl:defpackage :std.concurrency.mutex
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.concurrency.mutex)

;; some of these might (?) get simple re-exported using defnamespace (defpackage)
;; once this file is split into separate namespaces
;; because we're literally repeating ourselves here for nor reason other than
;; consistency with the rest of this project
(%:fn/macro create (:key name) sb-thread:make-mutex)
(%:fn/macro ? (object) bt:lock-p)
(%:macro do sb-thread:with-mutex)
(%:fn/macro name ((instance mutex)) sb-thread:mutex-name)
(%:fn/macro owner (mutex) sb-thread:mutex-owner)
(%:fn/macro value (mutex) sb-thread:mutex-value)
(%:fn/macro grab (mutex :key ((waitp wait?) cl:t) timeout) sb-thread:grab-mutex)
(%:macro grab-do bt:with-lock-held)
(%:fn/macro release (mutex :key (if-not-owner :punt)) sb-thread:release-mutex)
