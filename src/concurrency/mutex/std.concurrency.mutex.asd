(asdf:defsystem :std.concurrency.mutex
  :depends-on (:std.internal
               :bordeaux-threads)
  :components
  ((:file "api")))
