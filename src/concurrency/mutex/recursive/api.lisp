(cl:defpackage :std.concurrency.mutex.recursive
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.concurrency.mutex.recursive)

(%:fn/macro create (:optional name) bt:make-recursive-lock)
(%:fn/macro ? (object) bt:recursive-lock-p)
(%:fn/macro grab (lock) bt:acquire-recursive-lock)
(%:fn/macro release (lock) bt:release-recursive-lock)
;; TODO: aren't these 2 the same thing?
(%:macro with-lock-held bt:with-recursive-lock-held)
(%:macro with-lock sb-thread:with-recursive-lock)
