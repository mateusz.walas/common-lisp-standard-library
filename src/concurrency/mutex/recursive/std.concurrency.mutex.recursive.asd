(asdf:defsystem :std.concurrency.mutex.recursive
  :depends-on (:std.internal
               :bordeaux-threads)
  :components
  ((:file "api")))
