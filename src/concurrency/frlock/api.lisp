(cl:defpackage :std.concurrency.frlock
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.concurrency.frlock)

(%:macro-with-args create () sb-concurrency:make-frlock)
(%:macro-with-args name (instance) sb-concurrency:frlock-name)
(%:macro read sb-concurrency:frlock-read)
(%:macro-with-args read-begin (frlock) sb-concurrency:frlock-read-begin)
(%:macro-with-args read-end (frlock) sb-concurrency:frlock-read-end)
(%:macro write sb-concurrency:frlock-write)
(%:macro-with-args write-grab (frlock) sb-concurrency:grab-frlock-write-lock)
(%:macro-with-args write-release (frlock) sb-concurrency:release-frlock-write-lock)
;; TODO: make sure all functions are here
