(cl:defpackage :std.concurrency.gate
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.concurrency.gate)

(%:macro-with-args create () sb-concurrency:make-gate)
(%:macro-with-args ? (object) sb-concurrency:gatep)
(%:macro-with-args close (gate) sb-concurrency:close-gate)
(%:macro-with-args name (instance) sb-concurrency:gate-name)
(%:macro-with-args open (gate) sb-concurrency:open-gate)
(%:macro-with-args open? (gate) sb-concurrency:gate-open-p)
(%:macro-with-args wait (gate) sb-concurrency:wait-on-gate)
