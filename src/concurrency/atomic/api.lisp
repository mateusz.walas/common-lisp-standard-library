(cl:defpackage :std.concurrency.atomic
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.concurrency.atomic)

(%:fn/macro incf (place :optional (diff 1)) sb-ext:atomic-incf)
(%:fn/macro decf (place :optional (diff 1)) sb-ext:atomic-decf)
(%:fn/macro push (place (obj object)) sb-ext:atomic-push)
(%:fn/macro pop (place) sb-ext:atomic-pop)
(%:fn/macro update (place update-fn :rest arguments) sb-ext:atomic-update)
