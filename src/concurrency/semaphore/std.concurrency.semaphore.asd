(asdf:defsystem :std.concurrency.semaphore
  :depends-on (:std.internal
               :bordeaux-threads)
  :components
  ((:file "api")))
