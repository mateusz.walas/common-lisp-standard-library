(cl:defpackage :std.concurrency.semaphore
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.concurrency.semaphore)

;; sb-thread is loaded by default

;; TODO: this function is probably useless on SBCL, make sure that's the case
(%:macro-with-args semaphore () sb-thread:make-semaphore)
;; TODO: write custom function here to avoid dependency on bordeaux-threads maybe?
(%:macro-with-args ? (object) bt:semaphore-p)
(%:macro-with-args signal (semaphore) sb-thread:signal-semaphore)
(%:macro-with-args wait (semaphore) sb-thread:wait-on-semaphore)
(%:macro-with-args try (semaphore) sb-thread:try-semaphore)
(%:macro-with-args value (instance) sb-thread:semaphore-count)
(%:macro-with-args name (semaphore) sb-thread:semaphore-name)
(%:macro-with-args notification () sb-thread:make-semaphore-notification)
(%:macro-with-args notification-status (semaphore-notification) sb-thread:semaphore-notification-status)
(%:macro-with-args notification-clear (semaphore-notification) sb-thread:clear-semaphore-notification)
