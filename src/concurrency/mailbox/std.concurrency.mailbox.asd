(asdf:defsystem :std.concurrency.mailbox
  :depends-on (:std.internal
               :sb-concurrency)
  :components
  ((:file "api")))
