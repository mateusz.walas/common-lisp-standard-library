(cl:defpackage :std.runtime
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.runtime)

(%:var *argv* sb-ext:*posix-argv*)
(%:var *features* cl:*features*)

;; not sure where else to put this function
(%:fn/macro assert-version>= (:rest subversions) sb-ext:assert-version->=)

(%:fn/macro hook-on-dump-add (hook :optional ((call-now-p call-now?))) uiop/image:register-image-dump-hook)
(%:fn/macro hook-on-dump-call () uiop/image:call-image-dump-hook)
(%:fn/macro hook-on-restore-add (hook :optional ((call-now-p call-now?) cl:t)) uiop/image:register-image-restore-hook)
(%:fn/macro hook-on-restore-call () uiop/image:call-image-restore-hook)

(%:var *hooks-init* sb-ext:*init-hooks*)
(%:var *hooks-exit* sb-ext:*exit-hooks*)

;; NOTE: these 3 functions are unreliable on my computer
;; so might not work on others too
;; (%:fn/macro args-command () uiop/image:argv0)
;; (%:fn/macro args uiop/image:command-line-arguments)
;; (%:fn/macro args-raw uiop/image:raw-command-line-arguments)
(%:fn/macro args () uiop/image:raw-command-line-arguments)
;; (%:fn/macro create-ecl uiop/image:create-image)
(%:fn/macro dump (filename :key compression executable output-name postlude dump-hook) uiop/image:dump-image)
(%:fn/macro save-and-die (core-file-name
                          :key
                          (toplevel #'sb-impl::toplevel-init)
                          executable
                          save-runtime-options
                          callable-exports
                          (purify cl:t)
                          root-structures
                          (environment-name "auxiliary")
                          compression)
            sb-ext:save-lisp-and-die)
(%:fn/macro die (code format :rest arguments) uiop/image:die)
(%:fn/macro quit (:optional (code 0) ((finish-output finish-output?) cl:t)) uiop/image:quit)
(%:fn/macro quit-with-code ((x code)) uiop/image:shell-boolean-exit)
(%:fn/macro restore (:key (lisp-interaction uiop/image:*lisp-interaction*)
                          (restore-hook uiop/image:*image-restore-hook*)
                          (prelude uiop/image:*image-prelude*)
                          (entry-point uiop/image:*image-entry-point*)
                          (if-already-restored '(cl:cerror "RUN RESTORE IMAGE ANYWAY")))
            uiop/image:restore-image)
(%:fn/macro condition-fatal? (condition) uiop/image:fatal-condition-p)
;; 'thunk' is a a closure without arguments
(%:fn/macro condition-fatal-prevented-call (thunk) uiop/image:call-with-fatal-condition-handler)
(%:macro condition-fatal-prevented-do uiop/image:with-fatal-condition-handler)
(%:fn/macro condition-fatal-handle (condition) uiop/image:handle-fatal-condition)
(%:fn/macro backtrace! (:key stream count condition) uiop/image:print-backtrace)
(%:fn/macro backtrace-raw! (:key (stream cl:*debug-io*) count condition) uiop/image:raw-print-backtrace)
(%:fn/macro backtrace-with-condition! (condition :key (stream uiop/stream:*stderr*) count) uiop/image:print-condition-backtrace)

(%:fn/macro id () uiop/os:implementation-identifier)
(%:fn/macro id-version () cl:lisp-implementation-version)
(%:fn/macro name () cl:lisp-implementation-type)

;; (%:fn/macro env-lisp-impl-type-short uiop/os:implementation-type)
(%:fn/macro memory-usage-print! (:optional (verbosity :default)) cl:room)

; NOTE: setf-able
;; (%:fn/macro env-get! ((x varname)) uiop:getenvp)
;; (%:fn/macro env-get-string ((x varname)) uiop:getenv)

(%:fn/macro env-put ((string name)) sb-posix:putenv)
(%:fn/macro env-get (name) sb-posix:getenv)
(%:fn/macro env-set (name value (overwrite overwrite?)) sb-posix:setenv)
(%:fn/macro env-unset (name) sb-posix:unsetenv)

(%:fn/macro feature? ((feature-expression feature)) alexandria:featurep)
