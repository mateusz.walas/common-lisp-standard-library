(cl:defpackage :std.database.sql
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.database.sql)

;; S-SQL manual: https://marijnhaverbeke.nl/postmodern/s-sql.html

;; (%:fn sql-from-lisp postmodern:sql-compile)
;; (%:fn sql-escape postmodern:sql-escape)

(%:macro sql s-sql:sql)
(%:fn compile s-sql:sql-compile)
(%:fn escape s-sql:sql-escape)
(%:fn escape-string s-sql:sql-escape-string)
(%:fn template s-sql:sql-template)
