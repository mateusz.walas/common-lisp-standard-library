(cl:defpackage :std.database.postgres
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.database.postgres)

;; Idk about this, database access is ok but making it postgres-specific - ???
;; to me postmodern is the only viable choice because it's so comprehensive
;; last time i checked the other solutions seemed half-assed.
;; That might've changed.
;; (fn pg-connect postmodern:connect)
;; (macro pg-connect-do postmodern:with-connection)
;; (fn pg-connect-toplevel postmodern:connect-toplevel)
;; (fn pg-connected? postmodern:connected-p)
;; (fn pg-disconnect postmodern:disconnect)
;; (fn pg-disconnect-toplevel postmodern:disconnect-toplevel)
;; ;; not sure what this function really does
;; (fn pg-connection-pool-clear postmodern:clear-connection-pool)
;; (macro pg-query postmodern:query)
;; (macro pg-prepared-statement postmodern:defprepared)

;; TODO: finish it
