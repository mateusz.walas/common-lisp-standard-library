(asdf:defsystem :std.database.postgres
  :depends-on (:std.internal
               :postmodern)
  :components
  ((:file "api")))
