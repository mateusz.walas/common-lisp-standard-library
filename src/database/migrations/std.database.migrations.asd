(asdf:defsystem :std.database.migrations
  :depends-on (:std.internal
               :cl-dbi
               :cl-migratum
               :cl-migratum.provider.local-path
               :cl-migratum.driver.sql)
  :components
  ((:file "api")))
