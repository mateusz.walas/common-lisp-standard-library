(cl:defpackage :std.database.migrations
  (:use)
  (:local-nicknames (:% :std.internal))
  (:documentation "
Suggested local-nicknames: __db.migrations__ or __migrations__
"))
(cl:in-package :std.database.migrations)


;; TODO: seems too specific, maybe drop it
