(cl:defpackage :std.symbol
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.symbol)

;; TODO:
;; maybe move it to std.namespace.symbol ?
;; and maybe move std.symbol.keyword to std.namespace.symbol.keyword ?

(%:fn/macro create (string) cl:make-symbol)
(%:fn/macro create-unique () cl:gensym)
(%:fn/macro intern (name :optional package) cl:intern)
(%:fn/macro ? (object) cl:symbolp)
(%:fn/macro from-strings (:rest things) alexandria:symbolicate)
;; wtf is 'control'?????
(%:fn/macro from-string (name :optional ((package namespace) (sb-int:sane-package))) cl:intern)
(%:fn/macro from-string-format (package (control format-string) :rest arguments) alexandria:format-symbol)
(%:fn/macro name (symbol) cl:symbol-name)

(%:fn/macro package (symbol) cl:symbol-package)
(%:fn/macro namespace (symbol) cl:symbol-package) ; alias of package

(%:fn/macro ensure (name :optional (package cl:*package*)) alexandria:ensure-symbol)
(%:fn/macro function (symbol) cl:symbol-function)
(%:fn/macro macro-function (symbol :optional ((env environment))) cl:macro-function)
(%:fn/macro value (symbol) cl:symbol-value)
(%:fn/macro properties (symbol) cl:symbol-plist)
(%:fn/macro property-value (symbol (indicator property) :optional default) cl:get)
(%:fn/macro bound? (symbol) cl:boundp)
(%:fn/macro remove! (symbol) cl:makunbound)

;; TODO:
;; decide what to do with these
(%:fn/macro find (name :optional ((package namespace))) cl:find-symbol)
(%:fn/macro intern (name :optional ((package namespace) (sb-int:sane-package))) cl:intern)
(%:fn/macro unintern (symbol :optional ((package namespace))) cl:unintern)
(%:fn/macro list ((string-designator string-or-symbol)) cl:find-all-symbols)
(%:fn/macro list-shadowing ((package-designator namespace-designator)) cl:package-shadowing-symbols)
(%:fn/macro import (symbols :optional ((package namespace))) cl:import)
(%:fn/macro import-shadowing (symbols :optional ((package namespace))) cl:shadowing-import)
(%:fn/macro export (symbols :optional ((package namespace))) cl:export)
(%:fn/macro shadow (symbols :optional ((package namespace))) cl:shadow)
