(cl:defpackage :std.symbol.keyword
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.symbol.keyword)

(%:fn/macro ? (object) cl:keywordp)
(%:fn/macro generate (name) alexandria:make-gensym)
(%:fn/macro generate-list (length :optional ((x prefix) "G")) alexandria:make-gensym-list)
(%:fn/macro from ((name string/symbol)) alexandria:make-keyword)
(%:fn/macro from-string ((name string)) alexandria:make-keyword)
(%:fn/macro from-symbol ((name symbol)) alexandria:make-keyword)
