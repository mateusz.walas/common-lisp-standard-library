(cl:defpackage :std.char
  (:use)
  (:local-nicknames (:% :std.internal))
  (:documentation "
Suggested local-nickname: __char__
"))
(cl:in-package :std.char)

(%:type char cl:character "
Type hierarchy:
  t > character

You can create ad-hoc/inline chars with the following syntax: #\\<char>

Examples:
```
#\\a
#\\A
#\\Space
#\\1
#\\2
#\\Newline
```
")

;; TODO: make sure this name is coherent with the other constants
;; perhaps 'limit' should be the prefix instead of the suffix?
(%:const +code-limit+ cl:char-code-limit "
WARNING: Implementation dependent.

A 96 (or higher) int.

On SBCL it's: `1114112 (21 bits, #x110000)`
")

;; TODO: maybe name it 'try' ???
(%:fn/macro coerce (object) cl:character "
Coerces object to char.

```
(char #\\a)            ; => #\\a
(char \"a\")            ; => \"a\"
(char 'a)             ; => #\\A (symbol names are uppercase)
(char '|a|)           ; => #\\a (a special symbol syntax for creating a symbol with EXACTLY the name we specified, no uppercase translation is done here)
(char 'aa)            ; => error, the object must contain just 1 character
(char '\\a)            ; => #\\a ; no idea what this '\\a syntax is, a different syntax for a #\\a?
(char 65)             ; => error (use from-code instead)
(char (from-code 65)) ; => #\\a - you can convert char code to the char if you need
(char #\\1)            ; => #\\1 (a character representing a number)
```
")

(%:fn/macro ? (object) cl:characterp "
Check if object is a char (no coercion).

```
(? 1)    ; => nil
(? #\\1)  ; => t
(? #\\a)  ; => t
(? 65)   ; => nil
```
")

(%:fn/macro alpha? (char) cl:alpha-char-p "
```
(alpha? #\a)      ; => t
(alpha? #\\1)     ; => nil
(alpha? #\\Space) ; => nil
```
")

(%:fn/macro alphanumeric? (char) cl:alphanumericp "
```
(alphanumeric? #\\a)     ; => t
(alphanumeric? #\\1)     ; => t
(alphanumeric? #\\Space) ; => nil
```
")

(%:fn/macro numeral (weight :optional (radix 10)) cl:digit-char "
INFO: I picked the 'numeral' name because this kind of thing is apparently called 'numeral systems'. Choose a (better) name: __in-base?__ or __for-base?__
WARNING: May return NIL.
WARNING: This function DOES NOT CALCULATE numbers in a given base (radix). It only gives you the single CHAR represented by a number (weight) in a given base (radix). You need to do your own calculations if you want to know what's (for example) 25 in base 8.

Returns a char representation of a number (weight) in the specified base (radix) (a.k.a numeral system).

```
(numeral 0)     ; => #\\0 (base 10 is assumed)
(numeral 0 10)  ; => #\\0
(numeral 1 10)  ; => #\\1
(numeral 9 10)  ; => #\\9
(numeral 10 10) ; => nil
(numeral 11 10) ; => nil

(numeral 0 2)   ; => #\\0
(numeral 1 2)   ; => #\\1
(numeral 2 2)   ; => nil

(numeral 0 16)  ; => #\\0
(numeral 10 16) ; => #\\A
(numeral 11 16) ; => #\\B
(numeral 15 16) ; => #\\F
(numeral 16 16) ; => nil
```
")

(%:fn/macro numeral? (char :optional (radix 10)) cl:digit-char-p "
Checks if `char` conforms to a given numeral system.
See `numeral`.
" (:symbol-macro? t))

(%:fn/macro case-possible? (char) cl:both-case-p "
Checks if char CAN BE (or if it 'makes sense' for char to be) upper/lowercase(d)..
It doesn't check if it IS upper/lowercased.

```
(case-possible? #\\a)     ; => t
(case-possible? #\\A)     ; => t
(case-possible? #\\5)     ; => nil
(case-possible? #\\Space) ; => nil
```
")

(%:fn/macro code (char) cl:char-code "
Upper limit defined by `+char-code-limit+`.

```
(code #\\a)       ; => 97
(code #\\A)       ; => 65
(code #\\1)       ; => 49
(code #\\Newline) ; => 10
```
")

(%:fn/macro from-code (code) cl:code-char "
WARNING: May return `nil`.

Upper limit defined by `+code-limit+`.

```
(from-code 97) ; => #\\a
(from-code 65) ; => #\\A
(from-code 49) ; => #\\1
(from-code 10) ; => #\\Newline
```
")

(%:fn/macro encode (char) cl:char-int "
TODO: decide if this is a completely useless function or not.

WARNING: Implementation dependent.

Basically a 'hash' or 'encoding' of char.
The result is 0+ int.
Don't be surprised if the result of this function is the same as that of char-code.

```
(= char-1 char-2)
```

... is the same as ...

```
(= (encode char-1)
   (encode char-2))
```
")

(%:fn/macro graphic? (char) cl:graphic-char-p "
Checks whether char can be represented as a (single) 'glyph'.

Graphic chars are spaces + standard chars (see #'standard?).

```
(graphic? #\\a)       ; => t
(graphic? #\\Space)   ; => t (spaces between words are like 'invisible characters')
(graphic? #\\Newline) ; => nil (newlines are not glyphs, the change the flow of text)
```
")

;; NOTE:
;; the naming 'consistency' in the cl:* package here is just nuts
;; char-downcase
;; vs
;; lower-case-p
;;
;; Keep in mind both operate SPECIFICALLY on character type
;; the lower-case-p function does not accept strings
;;
;; 3 differences:
;; - char- vs missing prefix
;; - 'down' vs 'lower'
;; - 'case' vs '-case'
;;
(%:fn/macro lowercase (char) cl:char-downcase "
If char CAN BE lowercased then it is, otherwise the original char is returned.

```
(lowercase #\\a)     ; => #\a
(lowercase #\\A)     ; => #\a
(lowercase #\\Space) ; => #\Space
```
")

(%:fn/macro lowercase? (char) cl:lower-case-p "
```
(lowercase? #\\a)     ; => t
(lowercase? #\\A)     ; => nil
(lowercase? #\\Space) ; => nil
(lowercase? #\\0)     ; => nil
```
")

(%:fn/macro name (char) cl:char-name "
Many chars in Common Lisp have english-like names describing them.

```
(name #\\a)     ; => \"LATIN_SMALL_LETTER_A\"
(name #\\A)     ; => \"LATIN_CAPITAL_LETTER_A\"
(name #\\Space) ; => \"Space\"
```
")

(%:fn/macro from-name (name) cl:name-char "
Reverse of char-name.

```
(name \"LATIN_SMALL_LETTER_A\")   ; => #\\a
(name \"LATIN_CAPITAL_LETTER_A\") ; => #\\A
(name \"Space\")                  ; => #\\Space
```
")

(%:type standard cl:standard-char)
(%:fn/macro standard? (char) cl:standard-char-p "
Checks if character is part of the standard charset in common lisp.

Basically all the single character keys on a QWERTY keyboard:
```
a-z A-Z 0-9
~ ` ! @ # $ % ^ & * ( ) - _ + =
{ } [ ] | \
: ; \" '
< , > . ? /
```
")

(%:fn/macro uppercase (char) cl:char-upcase "
```
(uppercase #\\a)     ; => #\\A
(uppercase #\\A)     ; => #\\A
(uppercase #\\Space) ; => #\\Space
```
")

(%:fn/macro uppercase? (char) cl:upper-case-p "
```
(uppercase? #\\a)     ; => nil
(uppercase? #\\A)     ; => t
(uppercase? #\\Space) ; => nil
```
")

(%:fn/macro = ((character char) :rest (more-characters chars)) cl:char= "
Case insensitive.

```
(= #\\a #\\a) ; => t
(= #\\a #\\A) ; => nil
(= #\\a #\\b) ; => nil
```
")

(%:fn/macro != ((character char) :rest (more-characters chars)) cl:char/= "
Case sensitive;

```
(!= #\\a #\\a) ; => nil
(!= #\\a #\\A) ; => t
(!= #\\a #\\b) ; => t
```
")

(%:fn/macro < ((character char) :rest (more-characters chars)) cl:char< "
Case sensitive.

```
(< #\\a #\\a) ; => nil
(< #\\a #\\A) ; => nil
(< #\\a #\\b) ; => t
```
")

(%:fn/macro <= ((character char) :rest (more-characters chars)) cl:char<= "
Case sensitive.

```
(<= #\\a #\\a) ; => t
(<= #\\a #\\A) ; => nil (code for #\\a is 97, code for #\\A is 65)
(<= #\\a #\\b) ; => t
```
")

(%:fn/macro > ((character char) :rest (more-characters chars)) cl:char> "
Case sensitive.

```
(> #\\a #\\a) ; => nil
(> #\\a #\\A) ; => t (code for #\\a is 97, code for #\\A is 65)
(> #\\a #\\b) ; => nil
```
")

(%:fn/macro >= ((character char) :rest (more-characters chars)) cl:char>= "
Case sensitive.

```
(>= #\\a #\\a) ; => t
(>= #\\a #\\A) ; => nil (code for #\\a is 97, code for #\\A is 65)
(>= #\\a #\\b) ; => nil
```
")

;; TODO: decide what to do here
;; ('i' is supposed to mean 'insensitive)
(%:fn/macro =i ((character char) :rest (more-characters chars)) cl:char-equal "
Case insensitive.

```
(=i #\\a #\\a) ; => t
(=i #\\a #\\A) ; => t
(=i #\\a #\\b) ; => nil
```
")

(%:fn/macro !=i ((character char) :rest (more-characters chars)) cl:char-not-equal "
Case insensitive.

```
(!=i #\\a #\\a)           ; => nil
(!=i #\\a #\\A)           ; => nil
(!=i #\\a #\\b)           ; => t
(!=i #\\a #\\b #\\c)      ; => t
(!=i #\\a #\\b #\\c #|\d) ; => t
```
")

(%:fn/macro <i ((character char) :rest (more-characters chars)) cl:char-lessp "
Case insensitive.

```
(<i #\\a #\\a)         ; => nil
(<i #\\a #\\A)         ; => nil ('same' character case-insensitive-wise so one is not lower than the other)
(<i #\\a #\\b)         ; => t
(<i #\\a #\\b #\\c)     ; => t
(<i #\\a #\\b #\\c #\\d) ; => t
```
")

(%:fn/macro <=i ((character char) :rest (more-characters chars)) cl:char-not-greaterp "
Case insensitive.

```
(<=i #\\a #\\a)         ; => t
(<=i #\\a #\\A)         ; => t
(<=i #\\a #\\b)         ; => t
(<=i #\\a #\\b #\\c)     ; => t
(<=i #\\a #\\b #\\c #\\d) ; => t
```
")

(%:fn/macro >i ((character char) :rest (more-characters chars)) cl:char-greaterp "
Case insensitive.

```
(>i #\\a #\\a)     ; => nil
(>i #\\a #\\A)     ; => nil (code for #\\a is 97, code for #\\A is 65)
(>i #\\c #\\b #\\a) ; => t
```
")

(%:fn/macro >=i ((character char) :rest (more-characters chars)) cl:char-not-lessp "
Case insensitive.

```
(>=i #\\a #\\a)         ; => t
(>=i #\\a #\\A)         ; => nil ('same' characters)
(>=i #\\b #\\a)         ; => t
(>=i #\\c #\\b #\\b #\\a) ; => t
(>=i #\\d #\\d #\\b #\\a) ; => t
```
")

(%:fn/macro to-string ((x char)) cl:string)
