(cl:defpackage :std.block
  (:use)
  (:local-nicknames (:% :std.internal))
  (:documentation "
Suggested local-nickname: __block__
"))
(cl:in-package :std.block)

;; (%:macro with-last cl:progn)

;; NOTE: I honestly doubt many people use stuff like prog, prog*, prog2, progv
;; I'm just leaving progn here for now

(cl:export
 (cl:defmacro last (cl:&rest forms)
   `(cl:progn ,@forms)))

(cl:export
 (cl:defmacro with-first (result cl:&rest forms)
   `(cl:prog1 ,result ,@forms)))

;; prog
;; prog*
;; prog1 - maybe call it block:1st ???
;; prog2 - maybe call it block:2nd ???
;; progn - special - maybe call it block:last ???
;; progv - special
