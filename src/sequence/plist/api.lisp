(cl:defpackage :std.sequence.plist
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.sequence.plist)

(%:fn/macro get ((place plist) (indicator key) :optional default) cl:getf)
(cl:export
  (cl:defun set (plist key value)
    (cl:setf (cl:getf plist key) value)))
(%:fn/macro properties (place (indicator-list keys)) cl:get-properties)
(%:fn/macro to-alist (plist) alexandria:plist-alist "
(plist:to-alist '(:a 1 :b 2)) ; => '((:a . 1) (:b . 2))
")
(%:fn/macro to-hash-table (plist) alexandria:plist-hash-table)
(%:fn/macro remove (plist key) uiop:remove-plist-key)
(%:fn/macro remove-many (plist keys) uiop:remove-plist-keys)
(%:fn/macro remove/eq (plist :rest keys) alexandria:remove-from-plist)
(%:fn/macro remove/eq! (plist :rest keys) alexandria:delete-from-plist)

;; (cl:export
;;  (cl:defmacro get (list property &optional default)
;;    `(cl:get property list ,@(list default))))

; remprop is just (remf (symbol-plist x) y)
; so we're not going to include it (?)
; (%:fn remove!? cl:remf)
