(asdf:defsystem :std.sequence
  :depends-on (:std.internal
               :alexandria
               :split-sequence
               :serapeum)
  :components
  ((:file "api")))
