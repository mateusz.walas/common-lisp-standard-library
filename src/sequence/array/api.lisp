(cl:defpackage :std.sequence.array
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.sequence.array)

(%:const +limit-dimension-count+ cl:array-rank-limit)
(%:const +limit-dimension-size+ cl:array-dimension-limit)
(%:const +limit-row-size+ cl:array-dimension-limit)
(%:const +limit-total-size+ cl:array-total-size-limit)

(%:type array cl:array "
t > array
")

(%:type array/simple cl:simple-array "
Type hierarchy:
  t > array > simple-array

- not displaced (must not share structure with another array)
- no fill pointer (all indices are available for manipulation all the time)
- not (expressly ?) adjustable (cannot be resized)

Basically a 'static', non-resizable array.
")

(%:fn/macro create (dimensions :key
                               ((element-type type) cl:t)
                               ((initial-element init))
                               ((initial-contents init-list))
                               ((adjustable adjustable?))
                               ((fill-pointer fill-pointer?) cl:nil)
                               displaced-to
                               displaced-index-offset)
            cl:make-array)
(%:fn/macro ? (object) cl:arrayp)
(%:fn/macro storage (array) sb-ext:array-storage-vector)
(%:fn/macro at (array :rest (subscripts indexes)) cl:aref)
(%:fn/macro copy (array :key ((element-type type)) fill-pointer ((adjustable adjustable?))) alexandria:copy-array)
(%:fn/macro complex-part-type-best (spec :optional environment) cl:upgraded-complex-part-type) ; no idea what this is for
(%:fn/macro dimention-len-at (array (axis-number dimension)) cl:array-dimension)
;; Hyperspec says "The upper exclusive bound on each individual dimension of an array. "
;; no idea what it means - maybe length of the biggest row in an array?
(%:fn/macro dimension-count-list (array) cl:array-dimensions)
(%:fn/macro dimension-count (array) cl:array-rank)
(%:fn/macro displacement (array) cl:array-displacement)
(%:fn/macro element-type (array) cl:array-element-type)
(%:fn/macro element-type-best (spec :optional environment) cl:upgraded-array-element-type)
(%:fn/macro fill-pointer (vector) cl:fill-pointer)
(%:fn/macro fill-pointer? (array) cl:array-has-fill-pointer-p)
(%:fn/macro in-bounds? (array :rest (subscripts indices)) cl:array-in-bounds-p)
(%:fn/macro resize (array dimensions :key ((element-type type)) init init-list fill-pointer displaced-to (displaced-index-offset 0)) cl:adjust-array)
(%:fn/macro resizeable? (array) cl:adjustable-array-p)
(%:fn/macro row-major-index (array :rest (subscripts indices)) cl:array-row-major-index)
(%:fn/macro size (array) cl:array-total-size)
(%:fn/macro size-total (array) cl:array-total-size "Alias for (array:size ...)")
(%:fn/macro sum (array) serapeum:sum)
