(asdf:defsystem :std.sequence.array
  :depends-on (:std.internal
               ;; literally for just 1 function...
               :alexandria)
  :components
  ((:file "api")))
