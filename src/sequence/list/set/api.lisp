(cl:defpackage :std.sequence.list.set
  (:use)
  (:local-nicknames (:% :std.internal))
  (:documentation "
Suggest local-nicknames: __list.set__ or __set__
"))
(cl:in-package :std.sequence.list.set)

(%:fn/macro ? (object :key (test #'cl:eql) (key #'cl:identity)) alexandria:setp)
(%:fn/macro = (list1 list2 :key (test #'cl:eql) key) alexandria:set-equal)
(%:fn/macro diff (list1 list2 :key key test) cl:set-difference)
(%:fn/macro diff! (list1 list2 :key key test) cl:nset-difference)
(%:fn/macro and (list1 list2 :key key test) cl:intersection)
(%:fn/macro and! (list1 list2 :key key test) cl:nintersection)
(%:fn/macro or (list1 list2 :key key test) cl:union)
(%:fn/macro or! (list1 list2 :key key test) cl:nunion)
(%:fn/macro xor (list1 list2 :key key test) cl:set-exclusive-or)
;; and here all of a sudden someone decided to set default values for TEST and TEST-NOT because of reasons
(%:fn/macro xor! (list1 list2 :key key (test #'cl:eql)) cl:nset-exclusive-or)
