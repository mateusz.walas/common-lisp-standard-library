(cl:defpackage :std.sequence.list
  (:use)
  (:local-nicknames (:% :std.internal))
  (:documentation "
Suggested local-nicknames: __list__
"))
(cl:in-package :std.sequence.list)

(%:type cons cl:cons)
(%:fn/macro create (size :key ((initial-element init))) cl:make-list "
Alias for cl:make-list.
```
(list:make 3 :initial-element 1) ; => (1 1 1)
```
")
(%:fn/macro ? (object) cl:listp "
```
(? nil)        ; => t
(? ())         ; => t
(? (list))     ; => t
(? (list 1))   ; => t
(? (cons a b)) ; => t
```
")
(%:fn/macro ?? (object) cl:consp "
Alias for CELL?
")
(%:fn/macro node ((se1 head) (se2 tail)) cl:cons)
(%:fn/macro cell ((se1 head) (se2 tail)) cl:cons "
Creates a building block of list.

```
(cell a nil) ; => (a)
(cell a b)   ; => (a . b)

(list a b)            ; => (a b)
(cell a (list b))     ; => (a b)
(cell a (cell b nil)) ; => (a b)
```
")
(%:fn/macro cell? (object) cl:consp "
```
(cell? nil)                 ; => nil
(cell? 1)                   ; => nil
(cell? \"hello\")             ; => nil
(cell? (cell nil nil))      ; => t
(cell? (cell a b))          ; => t
(cell? (cell a (cell b c))) ; => t
```
")

(%:defmacro iterate ((var list cl:&optional result) cl:&body body) "
Same as cl:dolist except resulting value defaults to the list.

```
(iterate (x (list 1 2 3))
  (print:format t \"~%x: ~a\" x)) => prints 3 lines and returns the (list 1 2 3)

(iterate (x (list 1 2 3) 4)
  (print:format t \"~%x: ~a\" x)) => prints 3 lines and returns 4
```
"
  (cl:let ((output (cl:gensym)))
    `(cl:let* ((lista ,list)
               (,output ,(cl:or result 'lista)))
       (cl:dolist (,var lista ,output)
         ,@body))))

(%:defmacro foreach! ((var list cl:&optional result) cl:&body body)
  `(iterate (,var ,list ,@(cl:and result (cl:list result)))
     ,@body))

(%:fn/macro to-values (list) cl:values-list "
```
(to-values nil)          ; => (values) - no values returned
(to-values (list a b c)) ; => (values a b c)
```
")
;; (%:fn/macro to-hashtable (list) uiop:list-to-hash-set "
;; Returns a new hashtable with list elements as keys - no duplicates obviously.
;; Uses eq (?) to check equality.
;; ")
(%:fn/macro circular (:rest elements) alexandria:circular-list)
(%:fn/macro circular? (object) alexandria:circular-list-p)
(%:fn/macro proper? (object) alexandria:proper-list-p)
(%:fn/macro proper-len (list) alexandria:proper-list-length)
(%:fn/macro ensure (list) alexandria:ensure-list) ; i have no idea what this function is supposed to do
(%:fn/macro list* (arg :rest others) cl:list* "
Same as list, except last argument becomes the tail.
```
(list* 1)               ; => 1
(list* 1 2)             ; => (1 . 2)
(list* 1 2 3)           ; => (1 2 . 3)
(list* 1 2 (list 3))    ; => (1 2 3)
(list* 1 2 (list 3 4))  ; => (1 2 3 4)
(list* 1 2 (list* 3 4)) ; => (1 2 3 . 4)
```
")

;; (%:fn at cl:nth "
;; Arguments:
;; n - 0+
;; list - regular | dotted | circular list

;; Returns:
;; nth element of a list.

;; (at -1 (list a b) ; => error, index must be 0 or higher
;; (at 0 (list a)    ; => a
;; (at 1 (list a b)  ; => b
;; (at 2 (list a b)  ; => nil
;; ")

(%:fn/macro at (list n) cl:nth)
(%:fn/macro 1st (list) cl:first)
(%:fn/macro 2nd (list) cl:second)
(%:fn/macro 3rd (list) cl:third)
(%:fn/macro 4th (list) cl:fourth)
(%:fn/macro 5th (list) cl:fifth)
(%:fn/macro 6th (list) cl:sixth)
(%:fn/macro 7th (list) cl:seventh)
(%:fn/macro 8th (list) cl:eighth)
(%:fn/macro 9th (list) cl:ninth)
(%:fn/macro 10th (list) cl:tenth)
(%:fn/macro foreach (list function :rest (more-lists lists)) cl:mapc)
(%:fn/macro foreach-tails (list function :rest (more-lists lists)) cl:mapl "
```
// Weird function
(foreach (cl:list 1 2)
  (cl:lambda (a b c)
    (cl:format cl:t \"first : ~a~%\" (cl:list a b c))))

// Here with additional lists
(foreach (cl:list 1 2)
  (cl:lambda (a b c)
    (cl:format cl:t \"kekw: ~a~%\" (cl:list a b c)))
  (cl:list 2 3)
  (cl:list 3 4))
```
")
(%:fn/macro flatmap (list function :rest (more-lists lists)) cl:mapcan)
; no idea what else to call it (yet)
(%:fn/macro flatmap-tails! (list function :rest (more-lists lists)) cl:mapcon)
(%:fn/macro map (list function :rest (more-lists lists)) cl:mapcar "
```
(map (lambda (x)
       (+ x 3))
     (list 1 2 3)) ; => (4 5 6)
```
")
(%:fn/macro map-tails! (list function :rest (more-lists lists)) cl:maplist "
Sample code:
```
(map-tails! (cl:lambda (tail)
              (print:format cl:t \"~%tail: ~a\" tail))
            (list 1 2 3 4 5))
```

Sample output:
```
tail: (1 2 3 4 5)
tail: (2 3 4 5)
tail: (3 4 5)
tail: (4 5)
tail: (5)
```
")
(%:fn/macro tail (list) cl:cdr "
```
(tail nil)          ; => nil
(tail (list 1))     ; => nil
(tail (list 1 2 3)) ; => (2 3)
```
")
(%:fn/macro tail? (object) cl:endp "
```
(tail? nil)          ; => t
(tail? (cons a nil)) ; => nil
(tail? (list a))     ; => nil
(tail? (list a b))   ; => nil
```
")
(%:fn/macro tail! ((cons list) (x value)) cl:rplacd "
Same as `(setf (tail! list) value)`
")
(%:fn/macro tail-last (list :optional (n 1)) cl:last "
```
(tail-last '(1))     ; => '(1)
(tail-last '(1 2))   ; => '(2)
(tail-last '(1 2 3)) ; => '(3)
```
")
(%:fn/macro skip (list n) cl:nthcdr "
```
(skip 0 '(1 2 3)) ; => '(1 2 3)
(skip 1 '(1 2 3)) ; => '(2 3)
(skip 2 '(1 2 3)) ; => '(3)
(skip 3 '(1 2 3)) ; => '()
```
")

;; (cl:defmacro skip (&optional n list)
;;   (cond
;;     (n `(skip ,n ,list))
;;     (list `(cl:lambda (list) (skip ,n ,list)))
;;     (cl:t #'skip)))

;; (cl:export
;;  (cl:defun skip (list n)
;;    (cl:)))

;; (cl:export
;;  (cl:defun skip<> (n)
;;    (cl:lambda (list)
;;      (skip n list))))
;; TODO: maybe call these skip-end or skip-last?
(%:fn/macro drop (list :optional (n 1)) cl:butlast)
(%:fn/macro drop! (list :optional (n 1)) cl:nbutlast)
(%:fn/macro drop-last (list :optional (n 1)) cl:butlast)
(%:fn/macro drop-last! (list :optional (n 1)) cl:nbutlast)
;; tail-before or maybe just 'before' ?
(%:fn/macro tail-before (list object) cl:ldiff "
Arguments (list object) - backwards, should be (object list)

If list does not contain object - the entire list is returned.
```
(let* ((items (list 1 2 3))
       (item-last (tail-at 2 items)))
  (tail-before list item-last)) ; => (1 2)

(tail-before '(1 2 3) '(2 3)) ; => '(1 2 3) - entire list is returned
```
")
(%:fn/macro tail-find (list item :key key test) cl:member)
(%:fn/macro tail-find-if (list (test predicate) :key key) cl:member-if)
;; rest-from? rest-at? idk about the name
;; this particular function is confusing
;; because (nthcdr 0 list) returns the entire list you gave it, not (cdr list)
;; and (list-tail-at 0 list) is the same
(%:macro appendf uiop/utility:appendf "
Does not mutate but stores concatenation of copies

(setf-append

")
(%:fn/macro append (:rest lists) cl:append "
All args except the last one are copied.

```
(append x)           ; => x (where x can be literally anything)
(append '(a) b)      ; => '(a . b)
(append '(a) '(b))   ; => '(a b)
(append '(a b) '(c)) ; => '(a b c)
(append '(a b) '()) ; => '(a b)
```
")
(%:fn/macro append! (:rest lists) cl:nconc "
Mutates all args except last.
First arg CANNOT be nil.

```
(append!)             ; => nil
(append! x)           ; => x (where x is anything except nil)
(append! '(1) '(2 3)) ; => '(1 2 3)
```

```
(let ((a '(1))
      (b '(2)))
  (append! a b '(3)) ; => '(1 2 3)
  a                  ; => '(1)
  b)                 ; => '(2 3)
```
")
;; cannot really change order
(%:fn/macro append-map (function :rest lists) alexandria:mappend)
(%:fn/macro append-reversed ((x list-to-reverse) (y list-to-append)) cl:revappend "
Reverses 1st arg.

```
(append-reversed a b) === (append! (sequence:reverse a) b)
(append-reversed '(1 2) '(3 4)) ; => '(2 1 3 4)
```
")
(%:fn/macro append-reversed! ((x list-to-reverse) (y list-to-append)) cl:nreconc "
Mutates all args but last.
Reverse all args except last.

```
(append-reversed! a b) === (append! (sequence:reverse! a) b)
```
")
;; this could literally be cl:not instead
;; or even not a function at all, since
(%:fn/macro empty? (object) cl:null "
```
(empty? '())  ; => t
(empty? '(1)) ; => nil
(empty? ... ) ; => ... you get the drill.
```
")
;; there doesn't seem to be a starts-with? equivalent
;; maybe there is but I just haven't noticed it yet
(%:fn/macro ends-with? (list object) cl:tailp)
(%:fn/macro head (list) cl:car)
; basically (setf (car list) value)
; maybe name it head-set! ? idk.
(%:fn/macro head! ((cons list) (x object)) cl:rplaca)
;; TODO: maybe add ! suffix to all these ensure-* macros?
(%:fn/macro head-ensure (cons) alexandria:ensure-cons)
(%:fn/macro len (list) cl:list-length)
;; basically same as (node ...) except you can specify :test and :key
(%:fn/macro prepend? (list item :key key (test #'cl:eql)) cl:adjoin)

;; (%:macro prepend! cl:push
;;     "
;; 2nd arg is a PLACE - it can't be an inline value or form.

;; (push! 3 '(1 2)) ; => error! '(1 2) is not a place.

;; (let ((a '(1 2)))
;;   (push! 3 a)
;;   a ; => '(3 1 2)
;; )
;; "
;;     )

(%:macro match cl:destructuring-bind)
(%:macro with-let cl:destructuring-bind)
(%:fn/macro push-start! (place (obj object)) cl:push "
2nd arg is a PLACE - it can't be an inline value or form.

```
(push! 3 '(1 2)) ; => error! '(1 2) is not a place.

(let ((a '(1 2)))
  (push! 3 a)
  a)              ; => '(3 1 2)
```
")

(%:fn/macro push! (place object) push-start! "
Alias for `std.sequence.list:push-start!`
")

;; TODO: maybe rename arguments in nconc-related functions to 'place' too?
(%:fn/macro push-start-new! (place (obj object) :key key test) cl:pushnew "
2nd arg is a PLACE - it can't be an inline value or form.

```
(let ((a '(1 2)))
  (push-new! 3 a)) ; => '(3 1 2)

(let ((a '(\"a\" \"b\")))
  (push-new \"aaa\" a)) ; => '(\"aaa\" \"aaa\" \"bbb\")

(let ((a '(\"a\" \"b\")))
  (push-new \"aaa\" a :test #'std.string:=)) ; => '(\"aaa\" \"bbb\")
```
")
(%:fn/macro push-new! (place object) push-start-new! "
Alias for `std.sequence.list:push-start-new!`
")
(%:fn/macro pop! (place) cl:pop "
Operates on a PLACE, not an inline list.
")
(%:fn/macro subset? (list1 list2 :key key (test #'cl:eql)) cl:subsetp)

(%:fn/macro iota (n :key (start 0) (step 1)) alexandria:iota)
(%:fn/macro iota-map (n function :key (start 0) (step 1)) alexandria:map-iota)
(%:fn/macro sum ((array list)) serapeum:sum)
