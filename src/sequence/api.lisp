(cl:defpackage :std.sequence
  (:use)
  (:local-nicknames (:% :std.internal))
  (:documentation "
Suggested local-nicknames: __sequence__ or __seq__

In general all std.sequence:* functions work on:
- lists (including associative-lists and property-lists)
- strings
- arrays
- vectors
- bit vectors
- etc (?)

So if you can't find for example a list-specific **std.sequence.list:reduce**
function then the next place you should look for it
is the more generic **std.sequence:reduce**.
"))
(cl:in-package :std.sequence)

(%:fn/macro create ((result-type type) (length len) :key ((initial-element init))) cl:make-sequence "
Creates a sequence of given type, size and optionally fills it with initial value.

Examples:
```
(sequence:create 'list   3 :initial-element 1)   ; (1 1 1)
(sequence:create 'string 3 :initial-element #\a)  ; \"aaa\"
(sequence:create 'vector 3 :initial-element 0)   ; #(0 0 0)
```")
(%:fn/macro rotate (sequence :optional n) alexandria:rotate)
(%:fn/macro shuffle! (sequence :key (start 0) end) alexandria:shuffle)
(%:fn/macro at (sequence index) cl:elt)
(%:fn/macro at-random (sequence :key (start 0) end) alexandria:random-elt)
(%:fn/macro batches ((seq sequence) (n batch-size) :key (start 0) end ((even even?))) serapeum:batches)
(%:fn/macro empty? (sequence) alexandria:emptyp) ; this has smaller disassemly code, could be insufficient
;; (%:fn/macro empty? uiop:emptyp)
(%:fn/macro proper-len= (sequence length) alexandria:sequence-of-length-p)
(%:fn/macro first (sequence) alexandria:first-elt)
(%:fn/macro first= (sequence object :key (test #'cl:eql) (key #'cl:identity)) alexandria:starts-with)
(%:fn/macro last (sequence) alexandria:last-elt)
(%:fn/macro last= (sequence object :key (test #'cl:eql) (key #'cl:identity)) alexandria:ends-with)
(%:fn/macro start= (sequence prefix :key ((return-suffix suffix))) alexandria:starts-with-subseq)
(%:fn/macro end= (sequence suffix :key (test #'cl:eql)) alexandria:ends-with-subseq)
(%:fn/macro copy (sequence) cl:copy-seq)
(%:fn/macro copy-coerce (sequence type) alexandria:copy-sequence)
;; TODO: maybe name it 'join' instead of the long-ass 'concatenate'
(%:fn/macro concatenate ((result-type type) :rest sequences) cl:concatenate) ; cant really change this one
(%:fn/macro concatenate! (sequence1 sequence2 predicate result-type) cl:merge)
(%:fn/macro count (sequence item :key from-end (start 0) end key (test #'cl:eql)) cl:count)
(%:fn/macro count-if (sequence predicate :key from-end (start 0) end key) cl:count-if)
(%:fn/macro every? ((first-seq sequence1) (pred predicate) :rest sequences) cl:every)
(%:fn/macro group-by ((seq sequence) :key (key #'cl:identity) (test #'cl:eql) (start 0) end hash) serapeum:assort)
;; I'm not convinced by the p prefix for parallel counterparts, especially here
;; (%:fn/macro parallel-every? lparallel:pevery)
(%:fn/macro none? ((first-seq sequence1) (pred predicate) :rest sequences) cl:notany)
(%:fn/macro fill! (sequence item :key (start 0) end) cl:fill)
(%:fn/macro find (sequence item :key from-end (start 0) end key test) cl:find)
(%:fn/macro find-if (sequence predicate :key from-end (start 0) end key) cl:find-if)
(%:fn/macro len (sequence) cl:length)
(%:fn/macro map ((first-sequence sequence1) function (result-type type) :rest (more-sequences sequences)) cl:map)
(%:fn/macro map! (result-sequence function :rest sequences) cl:map-into)
; returns position, not boolean
(%:fn/macro partition ((seq sequence) (pred predicate) :key  (start1 0) end (key #'cl:identity)) serapeum:partition)
(%:fn/macro position-mismatch (sequence1 sequence2 :key from-end (start1 0) end (start2 0) end2 key (test #'cl:eql)) cl:mismatch)
(%:fn/macro position (sequence item :key from-end (start 0) end key test) cl:position)
(%:fn/macro position-if (sequence predicate :key from-end (start 0) end key) cl:position-if)
(%:fn/macro reduce (sequence function :key from-end (start 0) end key ((initial-value init))) cl:reduce)
(%:fn/macro remove (sequence item :key from-end (start 0) end key (test #'cl:eql) count) cl:remove)
(%:fn/macro remove! (sequence item :key from-end (start 0) end key (test #'cl:eql) count) cl:delete)
(%:fn/macro remove-if (sequence predicate :key from-end (start 0) end key count) cl:remove-if)
(%:fn/macro remove-if! (sequence predicate :key from-end (start 0) end key count) cl:delete-if)
(%:fn/macro filter (sequence predicate :key from-end (start 0) end key count) cl:remove-if-not)
(%:fn/macro filter! (sequence predicate :key from-end (start 0) end key count) cl:delete-if-not)
(%:fn/macro remove-duplicates (sequence :key from-end (start 0) end key (test #'cl:eql)) cl:remove-duplicates)
(%:fn/macro remove-duplicates! (sequence :key from-end (start 0) end key (test #'cl:eql)) cl:delete-duplicates)
(%:fn/macro replace (sequence old new :key from-end (start 0) end key (test #'cl:eql) count) cl:substitute)
(%:fn/macro replace! (sequence old new :key from-end (start 0) end key (test #'cl:eql) count) cl:nsubstitute)
(%:fn/macro replace-if (sequence predicate new :key from-end (start 0) end key count) cl:substitute-if)
(%:fn/macro replace-if! (sequence predicate new :key from-end (start 0) end key count) cl:nsubstitute-if)
;; cl:replace doesnt signal errors
(%:fn/macro replace-range! ((target-sequence1 target) (source-sequence2 source) :key (start1 0) end1 (start2 0) end2) cl:replace)
(%:fn/macro reverse (sequence) cl:reverse)
(%:fn/macro reverse! (sequence) cl:nreverse)
;; name it 'subseq-position' or something
;; 'search' is not clear enough
;; can be confused with 'position'
(%:fn/macro search ((sub-sequence1 target) (main-sequence2 source) :key from-end (start1 0) end1 (start2 0) end2 key (test #'cl:eql)) cl:search)
(%:fn/macro any? ((first-seq sequence1) (pred predicate) :rest sequences) cl:some)
(%:fn/macro some? ((first-seq sequence1) (pred predicate) :rest sequences) cl:some)
; cl:sort actually expects sequence first and then predicate, amazing.
(%:fn/macro sort (sequence predicate :key key) cl:sort)
(%:fn/macro sort-stable (sequence predicate :key key) cl:stable-sort)
; there is also split-sequence:split-sequence because of course there is
(%:fn/macro split (sequence delimiter :key from-end (start 0) end (key #'cl:identity) (test #'cl:eql) count ((remove-empty-subseqs remove-empty?))) serapeum:split-sequence)
(%:fn/macro split-if (sequence predicate :key from-end (start 0) end (key #'cl:identity) count ((remove-empty-subseqs remove-empty?))) split-sequence:split-sequence-if)
(%:fn/macro slice! (sequence start :optional end) cl:subseq)
(%:fn/macro subseq! (sequence start :optional end) cl:subseq)
(%:fn/macro uniques ((seq sequence) :key (start 0) end (key #'cl:identity) (test #'cl:eql) compare-last (count cl:most-positive-fixnum)) serapeum:runs)
