(cl:defpackage :std.sequence.alist
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.sequence.alist)

(%:fn/macro copy (alist) cl:copy-alist "
(let ((alist '((:a . 1) (:b . 2))))
  (copy alist) ; => '((:a . 1) (:b . 2))
  ; 'pointer' comparison / strict equality etc
  (eq alist (copy alist) ; => nil
")

(%:fn/macro to-plist (alist) alexandria:alist-plist)

;; TODO: maybe just call it 'zip' ?
;; old name: push-zipped-pairs
(%:fn/macro zip (keys (data values) :optional alist) cl:pairlis "
Warnings:
- Pushes in reverse order

Args:
  keys - list of keys
  data (a.k.a. values) - list of keys ; TODO: rename 'data' to 'values'
  &optional alist - alist to append to

(push-zipped-pairs '(:a :b) '(1 2)) ; => '((:b . 2) (:a . 1))
(push-zipped-pairs '(:b :a) '(2 1) '((:c . 3))) ; => '((:c . 3) (:b . 2) (:a . 1))
")

; 'push' doesn't sound right here because it's not a mutation
; (and on top of that it's a function, not an accessor)
; on the other hand - it doesn't have the exclamation mark ! at the end, so...

(%:fn/macro push (alist key (datum value)) cl:acons "
Args:
  key
  value
  alist

(push :b 2 '((:a . 1))) ; => '((:b . 2) (:a . 1))
")

(%:fn/macro find-key (alist item) cl:assoc "
(find-key :a '( (:b . 2) (:a . 1))) ; => '(:a . 1)
(find-key :a '(((:b) . 2) ((:a) . 1)) :key #'list:head) ; => '((:a) . 1)
")

(%:fn/macro find-key-if (alist predicate :key key) cl:assoc-if "
Warnings:
- Returns only the FIRST MATCH

(find-key-if #'math:even? '((2 . :b) (4 . :d))) ; => '(2 . :b)
(find-key-if #'math:odd? '((1 . :a) (3 . :c))) ; => '(1 . :a)
")

(cl:export
 (cl:defun key->value (alist key cl:&key (by #'cl:identity) (test #'cl:eql))
   (cl:cdr (cl:assoc key alist :key by :test test))))

(%:fn/macro find-value (alist item :key key test) cl:rassoc "
(find-value :a '((2 . :b) (1 . :a))) ; => '(1 . :a)
(find-value :a '((2 . (:b)) (1 . (:a))) :key #'list:head) ; => '(1 . :a)
")

(%:fn/macro find-value-if (alist predicate :key key) cl:rassoc-if "
Returns only the first occurrence.

(let ((alist '((:a . 1) (:b . 2) (:c . 3))))
  (find-value-if #'std.number:odd? alist) ; => '(:a . 1))

(let ((alist '((:a . '(0 -1 -2)) (:b . '(1 2 3))))
  (find-value-if #'std.number:positive?
                 alist
                 :key #'cl:third) ; => 3
")

(%:fn/macro to-hash-table (alist) alexandria:alist-hash-table)
