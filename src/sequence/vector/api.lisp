(cl:defpackage :std.sequence.vector
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.sequence.vector)

;; t > sequence > array > simple array > vector
(%:fn/macro ? (object) cl:vectorp)
(%:fn/macro at (array :rest (subscripts indices)) cl:aref)
(%:fn/macro = ((vec1 vector1) (vec2 vector2) :key (test #'cl:eql) (start1 0) (start2 0) end1 end2) serapeum:vector=)

(%:fn/macro to-values ((vec vector)) serapeum:values-vector)
(%:fn/macro to-string (vector :key (start 0) end (external-format :default)) sb-ext:octets-to-string)
(%:fn/macro from-value ((x value)) serapeum:ensure-vector)

;; function is called VECTOR-push but the data is an ARRAY, terrific
(%:fn/macro push! ((array vector) (new-element value)) cl:vector-push)

;; but here it's VECTOR because ... reasons?
(%:fn/macro push-extend! (vector (new-element value) :optional min-extension) cl:vector-push-extend)

;; and here it's ARRAY again
(%:fn/macro pop! ((array vector)) cl:vector-pop)
(%:fn/macro sum ((array vector)) serapeum:sum)
