(cl:defpackage :std.sequence.vector.bit.simple
  (:use)
  (:local-nicknames (:% :std.internal))
  (:documentation "1-dimensional bit array of fixed size.
TODO:
In order to have a comprehensive bit operrations library, will need to incorporate the following packages:
- bit-ops (mentions all the above in its README)
- bit-smasher
- bitfield-schema
- nibbles
- trivial-bit-streams
- binary-types"))
(cl:in-package :std.sequence.vector.bit.simple)

(%:type vector.bit.simple cl:simple-bit-vector)
(%:fn/macro ? (object) cl:simple-bit-vector-p)

(%:fnbody-export create (size)
  (cl:make-array size :element-type 'cl:bit :initial-element 0))

(%:fnbody-export at (vector.bit.simple index)
  (cl:sbit vector.bit.simple index))

(%:fnbody-export at-set (vector.bit.simple index bit)
  (cl:setf (cl:sbit vector.bit.simple index) bit))
