(cl:defpackage :std.sequence.vector.bit
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.sequence.vector.bit)

;; TODO:
;; - maybe call it bitvec or something
;; - to make it shorter (although that sounds a bit stupid)
;; NOTE: maybe just get rid of it?
(%:type bit-vector cl:bit-vector "
Type: t > sequence > array > simple array > vector > bit-vector > simple-bit-vector
")
(%:fn/macro ? (object) cl:bit-vector-p)
(%:fn/macro simple? (object) cl:simple-bit-vector-p)
(%:fn/macro at (bit-array :rest (subscripts indices)) cl:bit)
(%:fn/macro at-simple (simple-bit-array :rest (subscripts indices)) cl:sbit)
(%:fn/macro and (bit-array-1 bit-array-2 :optional result-bit-array) cl:bit-and)
(%:fn/macro andc1 (bit-array-1 bit-array-2 :optional result-bit-array) cl:bit-andc1)
(%:fn/macro andc2 (bit-array-1 bit-array-2 :optional result-bit-array) cl:bit-andc2)
(%:fn/macro eqv (bit-array-1 bit-array-2 :optional result-bit-array) cl:bit-eqv)
(%:fn/macro ior (bit-array-1 bit-array-2 :optional result-bit-array) cl:bit-ior)
(%:fn/macro nand (bit-array-1 bit-array-2 :optional result-bit-array) cl:bit-nand)
(%:fn/macro nor (bit-array-1 bit-array-2 :optional result-bit-array) cl:bit-nor)
(%:fn/macro not (bit-array :optional result-bit-array) cl:bit-not)
(%:fn/macro orc1 (bit-array-1 bit-array-2 :optional result-bit-array) cl:bit-orc1)
(%:fn/macro orc2 (bit-array-1 bit-array-2 :optional result-bit-array) cl:bit-orc2)
(%:fn/macro xor (bit-array-1 bit-array-2 :optional result-bit-array) cl:bit-xor)
