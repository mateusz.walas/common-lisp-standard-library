(cl:defpackage :std.sequence.vector.simple
  (:use)
  (:local-nicknames (:% :std.internal))
  (:documentation "Generic vector of fixed size."))
(cl:in-package :std.sequence.vector.simple)

(%:type vector.simple cl:simple-vector)
(%:fn/macro ? (object) cl:simple-vector-p)
(%:fn/macro at (simple-vector index) cl:svref)
