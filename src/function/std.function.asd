(asdf:defsystem :std.function
  :depends-on (:std.internal
               :alexandria
               :sb-introspect)
  :components
  ((:file "api")))
