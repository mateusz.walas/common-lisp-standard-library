(cl:defpackage :std.function
  (:use)
  (:local-nicknames (:% :std.internal))
  (:documentation "
Suggested local-nicknames: __function__ or __fun__ or __fn__
"))
(cl:in-package :std.function)

(%:const +keywords-supported+ cl:lambda-list-keywords)
(%:const +arg-limit-def+ cl:lambda-parameters-limit)
(%:const +arg-limit-call+ cl:call-arguments-limit "
```
(<= 50 +arg-limit-def+ (- +arg-limit-call+ 1))
```
")
(%:fn/macro from (thing) cl:function)
(%:fn/macro ? (object) cl:functionp)
(%:fn/macro bound? (name) cl:fboundp)
(%:fn/macro global-definition (name) cl:fdefinition)
(%:fn/macro global-definition-remove! (name) cl:fmakunbound)
;; "Any implementation may legitimately return nil as the lambda-expression of any function."
;; tl;dr - lol why even use it
;; (%:fn/macro function-bound? cl:function-lambda-expression)
(%:fn/macro args! (function) sb-introspect:function-lambda-list "
WARNING: This function always returns the same exact list 'pointer', so if you modify it you might (be screwed and) need to restart SBCL.
")
(%:fn/macro call (function :rest (arguments args)) cl:funcall "" (:symbol-macro? t))
;; I'm torn here with regards to the order
;; I'm leaving the defaults here for now
(%:fn/macro call/symbol (package name :rest args) uiop/package:symbol-call)
(%:fn/macro apply (function arg :rest args) cl:apply)
(%:fn/macro curry (function :rest (arguments args)) alexandria:curry)
(%:fn/macro curry-right (function :rest (arguments args)) alexandria:rcurry)

;; >> is alias for compose, taken from F#
(cl:eval-when (:compile-toplevel :load-toplevel)
  (cl:defun compose (cl:&rest functions)
    (cl:lambda (initial-value)
      (cl:reduce #'cl:funcall functions :initial-value initial-value)))
  (cl:export 'compose))

;; (%:fn/macro compose (function :rest more-functions) alexandria:compose)
(%:fn/macro >> (:rest functions) std.function:compose)

;; (%:fn/macro test-consecutive (predicate :rest more-predicates) alexandria:disjoin)
;; TODO: figure out what to do with these 2 aliases of the same function
;; (%:fn/macro test-all (predicate) alexandria:conjoin)
;; (%:fn/macro pipe-right (predicate) alexandria:conjoin)

(%:fn/macro disjoin (predicate :rest more-predicates) alexandria:disjoin)
(%:fn/macro conjoin (predicate :rest more-predicates) alexandria:conjoin)
