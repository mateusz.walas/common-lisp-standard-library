(cl:defpackage :std.function.generic
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.function.generic)

(%:macro-with-args ensure ((fun-name name) :rest all-keys) cl:ensure-generic-function)
;; make-method is a local macro, cannot rename it here
;; (%:macro fn-method cl:make-method)
(%:macro-with-args method-add (generic-function method) cl:add-method)
(%:macro-with-args method-remove (generic-function method) cl:remove-method)
(%:macro-with-args method-find (generic-function qualifiers specializers :optional ((errorp error?))) cl:find-method)
(%:macro-with-args method-call (:rest args) cl:call-method) ; it's a macro underneath
(%:macro-with-args method-keywords (method) cl:function-keywords)
;; next-method-p is a local function - cannot rename it here
;; (%:macro-with-args fn-method-next? cl:next-method-p)
;; call-next-method is a local function - cannot rename it here
;; (%:macro-with-args fn-method-next-call cl:call-next-method)

;; NOTE: this function is not supposed to be called manually
;; (%:macro-with-args method-next-on-missing cl:no-applicable-method)

(%:macro-with-args method-qualifiers ((m method)) cl:method-qualifiers)
(%:macro-with-args methods-compute (generic-function (arguments args)) cl:compute-applicable-methods)
;; TODO: finish generics / methods / objects / classes etc
