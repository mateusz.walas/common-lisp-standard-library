(cl:defpackage :std.version
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.version)

(%:fn/macro next (version) uiop/version:next-version)
(%:fn/macro string-to-list (version-string :optional on-error) uiop/version:parse-version)
(%:fn/macro list-to-string (version-list) uiop/version:unparse-version)
(%:fn/macro string< (version1 version2) uiop/version:version<)
(%:fn/macro string<= (version1 version2) uiop/version:version<=)
(%:fn/macro deprecation-level (version
                               :key
                               style-warning
                               ;; NOTE:
                               ;; no ideea why it's called style-warning where uiop/version:next-version has argument called 'version'
                               (warning (uiop/version:next-version style-warning))
                               (error (uiop/version:next-version warning))
                               (delete (uiop/version:next-version error)))
            uiop/version:version-deprecation)
