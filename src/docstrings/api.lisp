(cl:defpackage :std.docstrings
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.docstrings)

; maybe move it to the 'symbol' namespace or something

;; TODO: name this file better (there might be another 'documentation'-related functionality soon)

(%:fn/macro documentation (object doc-type) cl:documentation)
(%:fn/macro apropos (string-designator :optional package ((external-only external-only?))) cl:apropos)
;; NOTE:
;; why is the optional argument called 'package-DESIGNATOR 'when (cl:apropos ...) has just 'package' ???
;; why the distinction ?
(%:fn/macro apropos-list (string-designator :optional package-designator ((external-only external-only?))) cl:apropos-list)

;; NOTE:
;; both &OPTIONAL and &KEY arguments present again x)
(%:fn apropos-regex cl-ppcre:regex-apropos)
;; (%:fn/macro apropos-regex (regex  :optional packages :key  ((case-insensitive case-insensitive?) cl:t)) cl-ppcre:regex-apropos)
(%:fn apropos-regex-list cl-ppcre:regex-apropos)
;; (%:fn/macro apropos-regex-list cl-ppcre:regex-apropos-list)

;; NOTE:
;; both &OPTIONAL and &KEY arguments
(%:fn dribble cl:dribble)
;; (%:fn/macro dribble (:optional pathname :key ((if-exists if-exists?) :append)) cl:dribble)

;; TODO:
;; what is 'x' ???
(%:fn/macro ed (:optional x) cl:ed)
