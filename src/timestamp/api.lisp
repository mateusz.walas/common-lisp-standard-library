(cl:defpackage :std.timestamp
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.timestamp)

;; TODO: is nsec NANOseconds?
(%:fn/macro create (:key day sec nsec) local-time:make-timestamp)
(%:fn/macro clone (timestamp) local-time:clone-timestamp)
(%:fn/macro parts (timestamp :key (timezone local-time:*default-timezone*) offset) local-time:decode-timestamp)
(%:macro parts-do local-time:with-decoded-timestamp)
(%:fn/macro microseconds (timestamp) local-time:timestamp-microsecond)
(%:fn/macro microseconds2 (object) local-time:nsec-of) ; no idea what's the difference
(%:fn/macro millisecond (timestamp) local-time:timestamp-millisecond)
(%:fn/macro seconds (object) local-time:sec-of)
(%:fn/macro minute (timestamp :key (timezone local-time:*default-timezone*)) local-time:timestamp-minute)
(%:fn/macro hour (timestamp :key (timezone local-time:*default-timezone*)) local-time:timestamp-hour)
(%:fn/macro day (timestamp :key (timezone local-time:*default-timezone*)) local-time:timestamp-day)
(%:fn/macro day2 (object) local-time:day-of) ; no idea what's the supposed difference
(%:fn/macro day-of-week (timestamp :key (timezone local-time:*default-timezone*) offset) local-time:timestamp-day-of-week)
(%:fn/macro month (timestamp :key (timezone local-time:*default-timezone*)) local-time:timestamp-month)
(%:fn/macro year (timestamp :key (timezone local-time:*default-timezone*)) local-time:timestamp-year)
(%:fn/macro decade (timestamp :key (timezone local-time:*default-timezone*)) local-time:timestamp-decade)
(%:fn/macro century (timestamp :key (timezone local-time:*default-timezone*)) local-time:timestamp-century)
(%:fn/macro millenium (timestamp :key (timezone local-time:*default-timezone*)) local-time:timestamp-millennium)
(%:fn/macro subtimezone (timestamp timezone) local-time:timestamp-subtimezone)

(%:fn/macro + (time amount unit :optional (timezone local-time:*default-timezone*) offset) local-time:timestamp+)
(%:fn/macro - (time amount unit :optional (timezone local-time:*default-timezone*) offset) local-time:timestamp-)
(%:fn/macro < (:rest (times timestamps)) local-time:timestamp<)
(%:fn/macro <= (:rest (times timestamps)) local-time:timestamp<=)
(%:fn/macro > (:rest (times timestamps)) local-time:timestamp>)
(%:fn/macro >= (:rest (times timestamps)) local-time:timestamp>=)
(%:fn/macro = (:rest (times timestamps)) local-time:timestamp=)
(%:fn/macro /= (:rest timestamps) local-time:timestamp/=)

(%:fn/macro min ((time timestamp) :rest (times timestamps)) local-time:timestamp-minimum)
(%:fn/macro max ((time timestamp) :rest (times timestamps)) local-time:timestamp-maximum)
(%:fn/macro now () local-time:now)
(%:fn/macro today () local-time:today)
(%:fn/macro to-universal (timestamp) local-time:timestamp-to-universal)
(%:fn/macro to-unix (timestamp) local-time:timestamp-to-unix)
(%:fn/macro from-universal (universal :key (nsec 0)) local-time:universal-to-timestamp)
(%:fn/macro from-unix (unix :key (nsec 0)) local-time:unix-to-timestamp)
;; there is sb-ext:get-time-of-day -> seconds and microseconds since epoch
