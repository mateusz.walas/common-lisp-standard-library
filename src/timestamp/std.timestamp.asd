(asdf:defsystem :std.timestamp
  :depends-on (:std.internal
               :local-time)
  :components
  ((:file "api")))
