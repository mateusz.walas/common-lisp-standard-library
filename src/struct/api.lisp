(cl:defpackage :std.struct
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.struct)

(%:fn/macro copy (structure) cl:copy-structure)
