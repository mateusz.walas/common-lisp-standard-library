(cl:defpackage :std.values
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.values)

(%:const +count-max+ cl:multiple-values-limit)

(%:macro assign cl:multiple-value-setq)
(%:fn/macro at (form (n index)) cl:nth-value)

(cl:defmacro call (function arg cl:&rest arguments)
  ;; special operator, have to wrap it
  `(cl:multiple-value-call ,function ,arg ,@arguments))

(%:macro to-list cl:multiple-value-list)
(%:fn/macro from-list (list) cl:values-list)

(cl:defmacro prog1 (values-form cl:&rest forms)
  ;; special operator, have to wrap it
  `(cl:multiple-value-prog1 ,values-form ,@forms))

(%:macro with-let cl:multiple-value-bind)
