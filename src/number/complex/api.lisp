(cl:defpackage :std.number.complex
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.number.complex)

(%:fn/macro create ((realpart real) :optional ((imagpart imaginary) 0)) cl:complex)
(%:fn/macro ? (object) cl:complexp)
(%:fn/macro real (number) cl:realpart)
(%:fn/macro imaginary (number) cl:imagpart)
; meh
; TODO: figure out a better name (?)
(%:fn/macro type-best (spec :optional environment) cl:upgraded-complex-part-type)
