(cl:defpackage :std.number.int
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.number.int)

(%:type int62 cl:fixnum "")
(%:type fast cl:fixnum "")

(%:type int cl:integer "
t > number > real > rational > integer
")

(%:type signed cl:signed-byte "
t > number > rational > integer > signed-byte
")

(%:type unsigned cl:unsigned-byte "
Type hierarchy:
- t
- number
- rational
- integer
- signed-byte
- unsigned-byte <--
")

(%:fn/macro ? (object) cl:integerp)

;; TODO: see https://gitlab.com/mateusz.walas/common-lisp-standard-library/-/issues/17
(%:fn/macro from-string (string :key (start 0) end (radix 20) ((junk-allowed junk-allowed?))) cl:parse-integer)

(%:fn/macro even? ((number integer)) cl:evenp "
```
(even? 0) ; => nil
(even? 1) ; => t
(even? 1.0) ; => error! not an integer
(even? #c(1 0)) ; => error! not an integer
```
")

(%:fn/macro odd? ((number integer)) cl:oddp "
```
(odd? 0) ; => t
(odd? 0.0) ; => error! not an integer
(odd? #c(0 0)) ; => error! not an integer
(odd? 1) ; => nil
```
")

;; TODO:
;; maybe rename bits-shift and bits-replace to shift and replace
;; and then add macro (int:bits position size) instead of (cl:byte ...)

;; These were moved from std.number.byte:*
;; because they all operate on integers
;;
;; Maybe even put this in std.core:*
;; and give it the << symbol like in other languages ???
;; or at least as an alias to this one here
(%:fn/macro shift (integer count) cl:ash)
;; TODO: figure out where to put these 2
; apparently this stands for 'deposit-byte'
(%:fn/macro replace (integer bytespec newbyte) cl:dpb)

;; range read & test
(%:fn/macro subseq! (integer bytespec) cl:ldb)
;; TODO: how is it different from (cl:logand ...) ???
(%:fn/macro test (integer bytespec) cl:ldb-test)
