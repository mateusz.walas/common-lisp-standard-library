(cl:defpackage :std.number.byte
  (:use)
  (:local-nicknames (:% :std.internal))
  (:documentation "
Suggested local-nickname: __byte__
"))
(cl:in-package :std.number.byte)

(cl:export
 (cl:deftype byte ()
   '(cl:unsigned-byte 8)))

;; TODO: decide how to name it
;; TODO: check if after changing it this issue appears:
;; https://www.reddit.com/r/Common_Lisp/comments/lzb5ob/fastest_way_to_set_specific_bits_in_a_byteoctet/

;; maybe call this one 'range' or 'spec'
;; because it's supposed to be used as 'byteSPEC' in all the other functions
;; 'bytespec' stands for 'byte specifier' by the way

;; (%:fn/macro bits (size position) cl:byte "kekw")
;; (%:fn/macro bits-size (bytespec) cl:byte-size)
;; (%:fn/macro bits-position (bytespec) cl:byte-position)

;; The name 'byte' is terrible so I'm definitely not moving it to std.core:*
;; I can't decide however between 'range', 'spec' or moving it to std.number.int:* and calling it 'bits' or 'subseq'
;; 'byte' just doesn't explain what this function/macro does
;; because it operates on BITS (plural!!!) and not a single BYTE or even entire BYTES
(%:fn/macro range (position size) cl:byte)
(%:fn/macro range-size (bytespec) cl:byte-size)
(%:fn/macro range-position (bytespec) cl:byte-position)
