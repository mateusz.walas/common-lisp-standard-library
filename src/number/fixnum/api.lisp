(cl:defpackage :std.number.fixnum
  (:use)
  ;; TODO: maybe drop the long std.number.fixnum one?
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.number.fixnum)

(%:var +min+ cl:most-negative-fixnum)
(%:var +max+ cl:most-positive-fixnum)

;; (%:type fixnum cl:fixnum)
; unsafe, but I'm keeping it for now
(%:fn/macro ? (object) sb-int:fixnump)
