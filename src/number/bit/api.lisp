(cl:defpackage :std.number.bit
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.number.bit)

;; These functions operate on integers and bit vectors
;; TODO: add aliases to bit-vector:*
;;   the bit-vector namespace might be called bitv or bitvec or something
;;   haven't decided yet

;; (%:type bit cl:bit "
;; Hierarchy (standard):
;;     t > number > real > rational > integer > signed-byte > unsigned-byte > bit

;; Hierarchy:
;;     t > number > number.real > number.rational > int > int+ > bit
;; ")

(cl:declaim (cl:ftype (cl:function (cl:t) cl:boolean) ?))
(%:fnbody-export ? (value)
  (cl:typep value 'cl:bit))

;; these should probably land in std.number.int instead
(%:fn/macro at? (integer index) cl:logbitp "
Checks if integer has bit set to 1 at a given 'index'.

Signature:
  (bit? index integer)
Same as:
  TODO: use stdlib naming here
  (std.number.byte:test (byte index) integer)

Examples:
(bit? 1 0)            ; => t
(bit? #b00000001 0)   ; => t (this is a binary representation of 1)
(bit? #b00000010 1)   ; => t
(bit? #b00000100 2)   ; => t
(bit? #b00001000 3)   ; ... you get the drill

(bit? -1 any-integer) ; => t (always true)
")

(%:fn/macro set! (integer bytespec newbyte) cl:deposit-field "
Warnings:
- Does not return a 'subseq' of an integer. It only mutates
  - only SETTER (no GETTER counterpart)
- There is no 1-to-1 match between the std.seq:subseq.

TODO: examples
")

(%:fn/macro len (integer) cl:integer-length "
Returns the minimum number of bits to represent a given integer.

(len #b00000000) ; => 0
(len #b00000001) ; => 1
(len #b00000011) ; => 2
(len #b00000101) ; => 3
(len #b00001000) ; => 4
")

(%:fn/macro count-1s (integer) cl:logcount "
(count-1s #b00000001) ; => 1
(count-1s #b00000011) ; => 2
(count-1s #b00000111) ; => 3
(count-1s -1)         ; => 0

The 3 forms are identical:
(count-1s a)
(count-1s (- (+ a 1)))
(count-1s (not a))
")

(%:fn/macro match? (integer1 integer2) cl:logtest "
Does at least 1 bit match in both integers?

(match? #b000 #b001) ; => nil
(match? #b001 #b011) ; => t
(match? #b111 #b101) ; => t
" (:symbol-macro? t))

(%:fn/macro and (:rest integers) cl:logand "
(and -1) ; => -1
(and 0 1) ; => 0
(and 1 1) ; => 0
(and #b101 #b111) ; => #b101 (5 & 7 = 5)
")

(%:fn/macro and-c1 (integer1 integer2) cl:logandc1 "" (:symbol-macro? t))
(%:fn/macro and-c2 (integer1 integer2) cl:logandc2)
(%:fn/macro eqv (:rest integers) cl:logeqv "" (:symbol-macro? t))
(%:fn/macro ior (:rest integers) cl:logior "" (:symbol-macro? t))
(%:fn/macro xor (:rest integers) cl:logxor "" (:symbol-macro? t))
(%:fn/macro nand (integer1 integer2) cl:lognand "" (:symbol-macro? t))
(%:fn/macro nor (integer1 integer2) cl:lognor "" (:symbol-macro? t))

(%:fn/macro not ((number integer)) cl:lognot "
(not #b111) ; => #b000
(not #b000) ; => #b111
(not #b101) ; => #b010
")

(%:fn/macro or-c1 (integer1 integer2) cl:logorc1 "" (:symbol-macro? t))
(%:fn/macro or-c2 (integer1 integer2) cl:logorc1 "" (:symbol-macro? t))
;; NOTE: maybe move/copy it to std.number.byte:mask ???
(%:fn/macro mask (integer bytespec) cl:mask-field)

;; NOTE:
;; moved here from std.number.byte:*
(%:fn/macro range (position size) cl:byte)
(%:fn/macro range-size (bytespec) cl:byte-size)
(%:fn/macro range-position (bytespec) cl:byte-position)
