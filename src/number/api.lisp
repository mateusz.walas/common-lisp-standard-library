(cl:defpackage :std.number
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.number)

;; TODO: decide if (and how) to (re)name these types
(%:type number cl:number "t > number")
(%:type real cl:real "t > number > real")
(%:type rational cl:rational "t > number > real > rational")

(%:fn/macro ? (object) cl:numberp)
(%:fn/macro rational? (object) cl:rationalp)

;; TODO: not sure where to put these
(%:fn/macro real? (object) cl:realp)
(%:fn/macro real-rational ((x number)) cl:rational)
(%:fn/macro real-rationalize ((x number)) cl:rationalize)

(%:fn/macro positive? (number) cl:plusp "
```
(positive? 0) ; => nil
(positive? -1) ; => nil
(positive? 1) ; => nil
(positive? #c(1 0)) ; => error! #c(1 0) is not of type REAL
```
")

(%:fn/macro negative? (number) cl:minusp "
```
(negative? 0) ; => nil
(negative? -0) ; => nil
(negative? -1) ; => nil
(negative? 1) ; => nil
(negative? #c(1 0)) ; => error! not a number.real
```
")

(%:fn/macro zero? (number) cl:zerop "
```
(zero? 0) ; => t
(zero? 0.0) ; => t
(zero? 1) ; => nil
(zero? #c(0 0)) ; => t
```
")
