(cl:defpackage :std.number.float
  ;; TODO: maybe drop the std.number.float one and use just std.float ?
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.number.float)

(%:type short cl:short-float)
(%:type single cl:single-float)
(%:type double cl:double-float)
(%:type long cl:long-float)

(%:type float32 cl:single-float)
(%:type float64 cl:double-float)

(%:const +short-min+ cl:most-negative-short-float)
(%:const +short-max+ cl:most-positive-short-float)
(%:const +short-epsilon+ cl:short-float-epsilon)
(%:const +short-epsilon-negative+ cl:short-float-negative-epsilon)

(%:const +long-min+ cl:most-negative-long-float)
(%:const +long-max+ cl:most-positive-long-float)
(%:const +long-epsilon+ cl:long-float-epsilon)
(%:const +long-epsilon-negative+ cl:long-float-negative-epsilon)

(%:const +single-min+ cl:most-negative-single-float)
(%:const +single-max+ cl:most-positive-single-float)
(%:const +single-epsilon+ cl:single-float-epsilon)
(%:const +single-epsilon-negative+ cl:single-float-negative-epsilon)

(%:const +double-min+ cl:most-positive-double-float)
(%:const +double-max+ cl:most-negative-double-float)
(%:const +double-epsilon+ cl:double-float-epsilon)
(%:const +double-epsilon-negative+ cl:double-float-negative-epsilon)

(%:fn/macro ? (object) cl:floatp)
(%:fn/macro digits ((f float)) cl:float-digits)
(%:fn/macro precision ((f float)) cl:float-precision)
(%:fn/macro radix ((x float)) cl:float-radix)
;; TODO: fix fn/macro for this specific case
;; here is the signature in SBCL:
;; (float-sign float1  &optional sb-kernel::float2 (float 1 sb-kernel::float1))
;; (%:fn/macro sign (float1 float2) cl:float-sign)
;; the 2nd argument is treated as 'format' for the 1st argument
;; (cl:float 1.0 0.0)   ; => 1.0
;; (cl:float 1.0 0.0d0) ; => 1.0d0
(%:fn/macro decode ((f float)) cl:decode-float)
(%:fn/macro decode-integer ((x float)) cl:integer-decode-float)
(%:fn/macro scale ((f float) (ex exponent)) cl:scale-float)
(%:fn/macro coerce ((number real) :optional ((other float-prototype))) cl:float)

(%:fnbody-export short-from (value)
  (cl:declare (cl:type cl:number value))
  (cl:coerce value 'short))

(%:fnbody-export single-from (value)
  (cl:declare (cl:type cl:number value))
  (cl:coerce value 'single))

(%:fnbody-export double-from (value)
  (cl:declare (cl:type cl:number value))
  (cl:coerce value 'double))

(%:fnbody-export long-from (value)
  (cl:declare (cl:type cl:number value))
  (cl:coerce value 'long))
