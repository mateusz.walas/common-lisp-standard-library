(cl:defpackage :std.string.unicode
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.string.unicode)

;; I recently learned that the cl:string< cl:string<= etc
;; all return MISMATCH INDEX (number), not BOOLEAN.
;; Using the functions below for those operations might be a bad idea
;; since it would introduce partial unicode support lacking in other parts (std.char).
;; Unicode support is added via the extended parts of std.char and std.string.
;; TODO: decide if something needs to be done about it
;; and what to do here.

(%:fn/macro normalize (string :optional (format :nfd) filter) sb-unicode:normalize-string)
(%:fn/macro normalized? (string :optional (format :nfd)) sb-unicode:normalized-p)
(%:fn/macro uppercase (string :optional locale) sb-unicode:uppercase)
(%:fn/macro lowercase (string :optional locale) sb-unicode:lowercase)
(%:fn/macro titlecase (string :optional locale) sb-unicode:titlecase)
(%:fn/macro foldcase (string) sb-unicode:casefold)
(%:fn/macro < (string1 string2 (start1 0) end1 (start2 0) end2) sb-unicode:unicode<)
(%:fn/macro = (string1 string2 (start1 0) end1 (start2 0) end2 ((strict strict?) cl:t)) sb-unicode:unicode=)
(%:fn/macro i= (string1 string2 (start1 0) end1 (start2 0) end2 ((strict strict?) cl:t)) sb-unicode:unicode-equal)
(%:fn/macro <= (string1 string2 (start1 0) end1 (start2 0) end2) sb-unicode:unicode<=)
(%:fn/macro > (string1 string2 (start1 0) end1 (start2 0) end2) sb-unicode:unicode>)
(%:fn/macro >= (string1 string2 (start1 0) end1 (start2 0) end2) sb-unicode:unicode>=)
(%:fn/macro confusable? (string1 string2 (start1 0) end1 (start2 0) end2) sb-unicode:confusable-p)
(%:fn/macro graphemes (string) sb-unicode:graphemes)
(%:fn/macro words (string) sb-unicode:words)
(%:fn/macro sentences (string) sb-unicode:sentences)
(%:fn/macro lines (string :key (margin cl:*print-right-margin*)) sb-unicode:lines)
