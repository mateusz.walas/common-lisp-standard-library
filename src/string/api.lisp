(cl:defpackage :std.string
  (:use)
  (:local-nicknames (:% :std.internal))
  (:documentation "
Suggested local-nicknames: __string__
"))
(cl:in-package :std.string)

;; TODO: some of the functions return (values x y) instead of just x
;; so go through all examples and make sure the return values are correct
;; The original docstrings do not mention these values.

(%:fnbody-export from-char (char)
  (cl:declare (cl:type cl:character char))
  (cl:string char))

(%:fnbody-export from-symbol (symbol)
  (cl:declare (cl:type cl:symbol symbol))
  (cl:string symbol))

(%:fn/macro from ((x char-or-symbol)) cl:string)

;; NOTE: the (cl:string object) function resides in std.char:*
;;       since it's the only place where it makes sense
;; TODO: figure out what to do with it
(%:fn/macro create (count :key ((element-type type) 'cl:character) ((initial-element init))) cl:make-string)
(%:fn/macro ? (object) cl:stringp)
(%:fn/macro qwerty? (string) uiop:base-string-p)

(%:type simple cl:simple-string "
```
Type order:
  - t
  - sequence
  - array
  - simple-array
  - vector
  - string
  - simple-string <---
```
")

(%:fn/macro simple? (object) cl:simple-string-p " Does this string satisfy type `string:simple` ?")
(%:fn/macro at (string index) cl:schar)
(%:fn/macro at/ignore-fill-pointer (string index) cl:char)
(%:fn/macro char-first ((s string)) uiop:first-char)
(%:fn/macro char-last ((s string)) uiop:last-char)
(%:fn/macro capitalize (string :key (start 0) end) cl:string-capitalize)
(%:fn/macro lowercase (string :key (start 0) end) cl:string-downcase)
(%:fn/macro lowercase! (string :key (start 0) end) cl:nstring-downcase)
(%:fn/macro lowercase? ((s string)) str:downcase?)
(%:fn/macro uppercase (string :key (start 0) end) cl:string-upcase)
(%:fn/macro uppercase! (string :key (start 0) end) cl:nstring-upcase)
(%:fn/macro uppercase? ((s string)) str:upcase?)
(%:fn/macro collapse-safe ((s string)) str:collapse-whitespaces)
(%:fn/macro trim (string (char-bag chars)) cl:string-trim " INFO: Respects `fill-pointer`. ")
(%:fn/macro trim-safe ((s string)) str:trim)
(%:fn/macro trim-left (string (char-bag chars)) cl:string-left-trim " Like `(trim ...)` except trims only left side. ")
(%:fn/macro trim-right (string (char-bag chars)) cl:string-right-trim)

(%:fn/macro = (string1 string2 :key (start1 0) end1 (start2 0) end2) cl:string= "" (:symbol-macro? cl:t))
(%:fn/macro i= (string1 string2 :key (start1 0) end1 (start2 0) end2) cl:string-equal "" (:symbol-macro? cl:t))

(cl:macrolet ((comparison (name original docstring)
                `(%:fnbody-export ,name (string1 string2 cl:&key (start1 0) end1 (start2 0) end2)
                   ,docstring
                   (cl:let ((result (,original string1 string2 :start1 start1 :end1 end1 :start2 start2 :end2 end2)))
                     (cl:values (cl:not (cl:not result)) result)))))
  (cl:macrolet ((case-sensitive (name original)
                  `(comparison ,name ,original "Returns 2 values: boolean and (nil or index where mismatch starts). Case SENSITIVE.")))
    (case-sensitive != cl:string/=)
    (case-sensitive < cl:string<)
    (case-sensitive <= cl:string<=)
    (case-sensitive > cl:string>)
    (case-sensitive >= cl:string>=))

  (cl:macrolet ((case-insensitive (name original)
                  `(comparison ,name ,original "Returns 2 values: boolean and (nil or index where mismatch starts). Case INSENSITIVE.")))
    (case-insensitive i!= cl:string-not-equal)
    (case-insensitive i< cl:string-lessp)
    (case-insensitive i<= cl:string-not-greaterp)
    (case-insensitive i> cl:string-greaterp)
    (case-insensitive i>= cl:string-not-lessp)))

(%:fn/macro blank? ((s string)) str:blank?)
(%:fn/macro empty? ((s string)) str:empty?)
(%:fn/macro join (strings separator) str:join)
;; was start=
(%:fn/macro starts-with? (string prefix) uiop:string-prefix-p)
(%:fn/macro start-common ((items strings)) str:prefix)
;; was end=
(%:fn/macro ends-with? (string suffix) uiop:string-suffix-p)
(%:fn/macro end-common ((items strings)) str:suffix)
(%:fn/macro around? (string (prefix before) (suffix after)) uiop:string-enclosed-p)

;; TODO: source/target ?
(%:fn/macro contains? ((s string) substring :key ((ignore-case ignore-case?) str:*ignore-case*)) str:contains?)
(%:fn/macro concat (:rest strings) str:concat)
(%:fn/macro concat-list (strings :key start end key) uiop/utility:reduce/strcat "The :end argument requires :start to be present.")
(%:fn/macro insert ((s string) index string/char) str:insert)
(%:fn/macro split (string :key max (separator '(#\Space #\Tab))) uiop:split-string)
(%:fn/macro repeat ((s string) count) str:repeat)
(%:fn/macro punctuation-replace ((s string) :key ((replacement with) " ")) str:remove-punctuation)
(%:fn/macro slice! ((sequence string) start :optional end) cl:subseq)
(%:fn/macro len ((sequence string)) cl:length)
(%:macro read  cl:with-input-from-string "Refer to cl:with-input-from-string documentation.")
(%:macro write cl:with-output-to-string " Refer to cl:with-output-to-string  documentation.")
(%:macro build cl:with-output-to-string " Refer to cl:with-output-to-string  documentation.")
(%:fn/macro to-octets (string :key (external-format :default) (start 0) end ((null-terminate null-terminated?))) sb-ext:string-to-octets "")
;; (%:fn/macro to-octets (string) babel:string-to-octets)
(%:fn/macro to-keyword (name) alexandria:make-keyword)
(%:fn/macro to-symbol-format (package (control format-string) :rest arguments) alexandria:format-symbol)

(cl:export
 (cl:defmacro format (string cl:&rest args)
   "TODO."
   `(cl:format cl:nil ,string ,@args)))

;; TODO: remove this
;; it belongs to `std.io.print:format!`
(cl:export
 (cl:defmacro format! (stream string cl:&rest args)
   "TODO."
   `(cl:format ,stream ,string ,@args)))

(%:fn/macro to-integer (string :key (start 0) end (radix 10) ((junk-allowed junk-allowed?))) cl:parse-integer "" (:symbol-macro? cl:t))
(%:fn/macro fill-pointer ((vector string)) cl:fill-pointer "" (:symbol-macro? cl:t))
