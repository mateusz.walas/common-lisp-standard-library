(asdf:defsystem :std.string.examples
  :depends-on (:std.core
               :std.string
               :std.sequence.list
               :std.sequence)
  :components
  ((:file "api.examples")))
