(cl:defpackage :std.string.examples
  (:use :std.core)
  (:local-nicknames (:list :std.sequence.list)
                    (:string :std.string)
                    (:sequence :std.sequence)))
(cl:in-package :std.string.examples)

(string:from-char #\a)                          ;; => "a"
(string:from-symbol '|aAa|)                     ;; => "aAa"

(string:create 3 :type 'char :init #\a)         ;; => "aaa"
(string:create 5 :init #\b)                     ;; => "bbbbb"
(string:? "a string")                           ;; => t
(string:? 'not-a-string)                        ;; => nil
(string:qwerty? "asd")                            ;; => nil
(string:at "abc" 1)                             ;; => #\b
(string:at/ignore-fill-pointer "abc" 1)         ;; => #\b
(string:char-first "abc")                       ;; => #\a
(string:char-last "abc")                        ;; => #\c
(string:capitalize "hello world")               ;; => "Hello World"

(string:lowercase "HELLO")                      ;; => "hello"
(string:lowercase! (string:create 3 :init #\A)) ;; => "aaa"
(string:lowercase? "hello")                     ;; => t

(string:uppercase "hello")                      ;; => "HELLO"
(string:uppercase! (string:create 3 :init #\a)) ;; => "AAA"
(string:uppercase? "HELLO")                     ;; => t

(string:collapse-safe nil)                      ;; => nil, nil
(string:collapse-safe "a  b  c")                ;; => "a b c", t

(string:trim "1abc1" "1")                       ;; => "abc"
(string:trim "123abc123" '(#\1 #\2 #\3))        ;; => "abc"

(string:trim-safe nil)                          ;; => nil
(string:trim-safe "   abc   ")                  ;; => "abc"

(string:trim-left "123abc123" "123")            ;; => "abc123"
(string:trim-left "123abc123" '(#\1 #\2 #\3))   ;; => "abc123"

(string:trim-right "123abc123" "123")           ;; => "123abc"
(string:trim-right "123abc123" '(#\1 #\2 #\3))  ;; => "123abc"

(string:= "abc" "abc") ;; => t
(string:= "abc" "ABC") ;; => nil

(string:!= "abc" "ABC") ;; => t, 0 (first index where strings don't match)
(string:!= "abc" "abc") ;; => nil, nil

(string:< "aaa" "aaa") ;; => nil, nil
(string:< "aaa" "AAA") ;; => nil, nil
(string:< "AAA" "aaa") ;; => t, 0
(string:< "aaa" "baa") ;; => t, 0
(string:< "aaa" "aba") ;; => t, 1
(string:< "aaa" "aab") ;; => t, 2

(string:<= "aaa" "AAA") ;; => nil, nil (uppercase letters have lower codes)
(string:<= "AAA" "aaa") ;; => t, 0
(string:<= "aaa" "baa") ;; => t, 0
(string:<= "aaa" "aba") ;; => t, 1
(string:<= "aaa" "aab") ;; => t, 2
(string:<= "aaa" "aaa") ;; => t, 3 (equal, so they 'differ' at 3rd index...)

(string:> "aaa" "aaa") ;; => nil, nil
(string:> "baa" "aaa") ;; => t, 0
(string:> "aba" "aaa") ;; => t, 1
(string:> "aab" "aaa") ;; => t, 2

(string:i= "aaa" "aaa") ; => t
(string:i= "aaa" "AAA") ; => t
(string:i= "aaa" "bbb") ; => nil

(string:i!= "aaa" "aaa") ; => nil, nil
(string:i!= "aaa" "AAA") ; => nil, nil
(string:i!= "aaa" "bbb") ; => t, 0

(string:i< "aaa" "aa")  ; => nil, nil
(string:i< "aaa" "$$$") ; => nil, nil
(string:i< "aaa" "aaa") ; => nil, nil
(string:i< "aaa" "AAA") ; => nil, nil
(string:i< "AAA" "aaa") ; => nil, nil
(string:i< "aaa" "baa") ; => t, 0
(string:i< "Aaa" "aBA") ; => t, 1
(string:i< "aAa" "ABb") ; => t, 2

(string:i<= "aaa" "$$$") ; => nil, nil
(string:i<= "aaa" "aaa") ; => t, 3
(string:i<= "aaa" "AAA") ; => t, 3
(string:i<= "AAA" "aaa") ; => t, 3
(string:i<= "AAa" "baA") ; => t, 0
(string:i<= "aAa" "BAa") ; => t, 0

(string:i> "aaa" "aaa") ; => nil, nil
(string:i> "aaa" "bbb") ; => nil, nil
(string:i> "bAA" "aaA") ; => t ,0
(string:i> "aBa" "Aaa") ; => t ,1
(string:i> "AaB" "aAa") ; => t ,2

(string:i>= "$$$" "aaa") ; => nil, nil
(string:i>= "aaa" "$$$") ; => t, 0
(string:i>= "aaa" "AAA") ; => t, 3

(string:blank? nil) ; => t
(string:blank? "")  ; => t
(string:blank? " ") ; => t
(string:blank? (string:create 0))                  ; => t
(string:blank? (string:create 3 :init #\Space))    ; => t
(string:blank? (string:create 3 :init #\Tab))      ; => t
(string:blank? (string:create 3 :init #\Newline))  ; => t
(string:blank? (string:create 3 :init #\Linefeed)) ; => t
(string:blank? "aaa")   ; => nil
(string:blank? "  a  ") ; => nil

; problematic, because nil is not an actual string
(string:empty? nil)   ; => t
(string:empty? "")    ; => t
(string:empty? "   ") ; => nil

(string:join (list "1" "2" "3") #\,)   ; => "1,2,3"
(string:join (list "1" "2" "3") ", ")  ; => "1, 2, 3"

(string:starts-with? "hello world" "hello")     ; => t
(string:ends-with? "hello world" "world")       ; => t
(string:start-common (list "aaa" "aab" "aac"))  ; => "aa"
(string:end-common (list "aaa" "baa" "caa"))    ; => "aa"
(string:around? "a b c" "a" "c") ; => t

(string:contains? "abcdef" "bcde")                ; => t
(string:contains? "abcdef" "BCDE")                ; => nil
(string:contains? "abcdef" "BCDE" :ignore-case? t) ; => t

(string:concat "a" "b" "c") ; => "abc"

(string:concat-list '("a" "b" "c"))                      ; => "abc"
(string:concat-list '("a" "b" "c") :start 1)             ; => "bc"
;; (string:concat-list '("a" "b" "c") :end 3) ;; => error! must specify :START too
(string:concat-list '(("a") ("b") ("c")) :key list:head) ; => "abc"

(string:insert "abc" 0 #\d)   ; => "dabc"
(string:insert "abc" 0 "d")   ; => "dabc"
(string:insert "ac" 1 "b") ; => "abc"

(string:split "one two three") ; => ("one" "two" "three")

(string:repeat "a" 3) ; => "aaa"

(string:punctuation-replace "a, b: 'c!'")                  ; => "a b c"
(string:punctuation-replace "a   b")                       ; => "a b", t
;; (string:punctuation-replace "a, b: 'c!'" :with #\_) ;; => error! replacement must be a string
(string:punctuation-replace "a, b: 'c!'" :with "_") ; => "a_b_c", t

;; => #\a
(string:read (string "asd")
  (cl:read-char string))

;; => "abc"
(string:write (string)
  (cl:format string "a")
  (cl:format string "b")
  (cl:format string "c"))
