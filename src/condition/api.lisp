(cl:defpackage :std.condition
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.condition)

;; TODO: finish it

;; (fn condition cl:make-condition)
;; (defmacro defcondition (name (&rest parent-types) (&rest slot-specs) &body body)
;;   `(cl:define-condition ,name ,parent-types ,slot-specs ,@body))

(%:macro try-catch cl:handler-case)
(%:macro catch-try cl:handler-bind)
;; this function is already exported by std.restart
;; (%:fn signal cl:signal)
(%:macro yolo cl:ignore-errors)
