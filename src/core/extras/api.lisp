(cl:defpackage :std.core.extras
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.core.extras)

(cl:export
 (cl:defmacro defenum (name &body fields)
   "
Since we can't create packages on the fly programmatically due to in what phase of the compiling
packages are actually created.

General usage:
1. create the package
2. use (defenum ...)
   to populate the package with symbols mapped to values starting at 0
   (like a typical enum)

Example usage:

(cl:defpackage :my-package
  (:use)) ; you dont need any other symbols in this enum-dedicated package
; no need for (cl:in-package :my-package)

(defenum numbers
  zero
  one
  two
  ... etc)
"
   (cl:let ((package (cl:symbol-name name))
         (value -1))
     `(cl:block nil
        ,@(cl:mapcan
           (cl:lambda (field)
             (cl:let ((symbol (cl:intern (cl:symbol-name field) package)))
               `((cl:intern ,(cl:symbol-name field) ,package)
                 (cl:export (cl:defconstant ,symbol ,(incf value)) ,package))))
           fields)))))
