(cl:macrolet ((create (name nicknames #| imports/extras |# imports/cl cl:&body imports)
                `(cl:defpackage ,name
                   (:use)
                   ,nicknames
                   ,imports/cl
                   ;; ,imports/extras
                   ,@imports
                   (:export
                     ,@(cl:rest (cl:rest imports/cl))
                     ;; ,@(cl:rest (cl:rest imports/extras))
                     ,@(cl:reduce #'cl:append
                                  (cl:rest imports)
                                  :key (cl:lambda (list)
                                         (cl:rest (cl:rest list)))
                                  :initial-value (cl:car imports))))))
  (create
   :std.core
   (:local-nicknames (:% :std.internal))
   ;; (:import-from :std.core.extras :defpipe*)
   (:import-from
    :cl

    :let
    :let*
    ;; Not sure about this, do people actually use it that often?
    ;; :rotatef

    :lambda
    :&allow-other-keys
    :&aux
    :&body
    :&environment
    :&key
    :&optional
    :&rest
    :&whole

    :return
    :return-from

    :if
    :when
    :unless

    :loop
    :loop-finish
    :dotimes
    :dolist

    :block

    :defparameter
    :defvar
    :defconstant
    :defmacro
    :defstruct
    :defclass
    :deftype
    :defun
    ;; :defsetf
    :defpackage
    :defgeneric
    :defmethod

    ;; TODO: maybe simply don't import these
    ;; and instead prefix every
    ;;
    ;; (declare (type ..)) etc
    ;;
    ;; with the SBCL-extension for package-prefixes like:
    ;;
    ;; cl::(declare (type ...))
    ;;
    ;; Type declarations / compiler optimizations
    ;; TODO: I don't like these symbols here
    ;; maybe I should split the std.core package into std.core and std.core.minimal
    ;; to avoid conflicts
    ;;
    :locally
    :proclaim
    :declaim
    :ftype
    :declare
    ;; :ignore
    ;; :ignorable
    :inline
    ;; :type
    ;; :optimize
    :debug
    ;; :safety
    ;; :special
    ;; :speed
    :quote

    ;; switch-case 'default'
    :otherwise

    ;; revise it vs the std.symbol and std.namespace
    ;; exporting symbols manually like this triggers
    ;; warnings when reloading package definitions
    ;; ASDF complains that 'this package ALSO exports x, y, z... etc'
    :import
    :export

    ;; see std.value for
    :values

    ;; booleans
    :t
    :nil

    :not
    :and
    :or

    ;; basically fn(x) => x
    ;; :identity

    ;; maybe move it to debug or something
    :eval
    :eval-when

    ;; types - simple
    ;; :atom
    ;; :boolean
    :class
    :function
    :list
    ;; :list*

    ;; string is a class
    ;; so it's nontrivial to re-export JUST the type part
    :string
    :array
    :vector
    :sequence
    :symbol
    :keyword
    ;; :integer
    ;; :bit
    ;; :number
    ;; :fixnum

    ;; why is this here?
    ;; i think this is both a function and a type name
    :null
    :assert

    ;; :eq
    ;; :eql
    ;; :equal
    ;; :equalp

    ;;  inconsistent naming with regards to the rest of this project
    :psetf                      ; should be called 'setf'
    :setf                       ; should be called 'setf*'
    ;; getf apparently stands for the 'get function'
    ;; :getf
    :shiftf

    :tagbody
    :go

    ;; TODO. decide what to do with these
    :destructuring-bind
    :handler-case
    :error

    :pathname

    ;; type
    ;; :package
    )))
(cl:in-package :std.core)

;; (export
;;  (defmacro defpackage (name &body options)
;;    (let ((use (cl:assoc :use options)))
;;      (unless use
;;        (cl:push '(:use :std.core) options))
;;      `(cl:progn
;;         (eval-when (:compile-toplevel :load-toplevel :execute)
;;           (cl:defpackage ,name ,@options))
;;         (cl:in-package ,name)))))

;; (cl:typep (cl:find-package :cl) 'cl:package)

;; (defmacro ftype (name args result)
;;   `(cl:declaim (cl:ftype (cl:function ,args ,result) ,name)))
;; (export 'ftype)

(%:type atom cl:atom)
(%:fn/macro atom? (object) cl:atom "
Type: t > atom

Everything except a cons cell is an atom.

(atom? nil)          ; => t
(atom? ())           ; => t
(atom? (cons a b))   ; => nil
(atom? (list 1))     ; => nil
(atom? (list 1 2 3)) ; => nil
(atom? \"hello\")    ; => t

Keep in mind that a list is a chain of conses, so a non-empty list is also NOT an atom.
")

(%:type bool cl:boolean "
Type order:
  - t
  - symbol
  - boolean

Either t or nil.
")

;; (%:type string cl:string "
;; ```
;; Type order:
;;   - t
;;   - sequence
;;   - array
;;   - vector
;;   - string
;; ```
;; Element type must be char or a subtype thereof.
;; Conceptually same as:
;; ```
;; (vector char)
;; ```
;; ")


(%:type char cl:character)

;; BASE COMMONLY-KNOWN TYPES

;; NOTE
;; defined <name>? functions
;; because in some of the cases it doesn't make sense
;; to create a whole package just to add one ? function
;; and THEN have to actually import it
(%:fn/macro bytespec (position size) cl:byte)
(%:fn/macro bytespec-size (bytespec) cl:byte-size)
(%:fn/macro bytespec-position (bytespec) cl:byte-position)

(%:type number cl:number)
(%:fn/macro number? ((object value)) cl:numberp)

(%:type bit cl:bit)
(declaim (ftype (function (t) bool) bit?))
(%:fnbody-export bit? (value) (cl:typep value 'bit))

(cl:deftype byte () '(cl:signed-byte 8))
(cl:export 'byte)
(cl:export (cl:defconstant byte.min (cl:- (cl:expt 2 7))))
(cl:export (cl:defconstant byte.max (cl:- (cl:expt 2 7) 1)))
(declaim (ftype (function (t) bool) byte?))
(%:fnbody-export byte? (value) (cl:typep value 'byte))

(cl:deftype ubyte () '(cl:unsigned-byte 8))
(cl:export 'ubyte)
(cl:export (cl:defconstant ubyte.min 0))
(cl:export (cl:defconstant ubyte.max (cl:- (cl:expt 2 8) 1)))
(declaim (ftype (function (t) bool) ubyte?))
(%:fnbody-export ubyte? (value) (cl:typep value 'ubyte))

(%:type int cl:fixnum)
(%:var int.min cl:most-negative-fixnum)
(%:var int.max cl:most-positive-fixnum)
(declaim (ftype (function (t) bool) int?))
(%:fnbody-export int? (value) (cl:typep value 'int))

(cl:deftype uint () '(cl:unsigned-byte 62))
(cl:export 'uint)
(cl:export (cl:defconstant uint.min 0))
(cl:export (cl:defconstant uint.max (cl:- (cl:expt 2 62) 1)))
(declaim (ftype (function (t) bool) uint?))
(%:fnbody-export uint? (value) (cl:typep value 'uint))

;; leaving these out for custom usage
(%:type integer cl:integer)
(%:type signed cl:signed-byte)
(%:type unsigned cl:unsigned-byte)

;; OTHER STUFF

;; (%:class string cl:string "Alias for cl:class.")

(%:fn/macro eq ((obj1 a) (obj2 b)) cl:eq nil (:symbol-macro? t))
(%:fn/macro eql ((x a) (y b)) cl:eql nil (:symbol-macro? t))
(%:fn/macro equal ((x a) (y b)) cl:equal nil (:symbol-macro? t))
(%:fn/macro equalp ((x a) (y b)) cl:equalp nil (:symbol-macro? t))
;; (%:fn/macro identity (thing) cl:identity)
;; (%:fn/macro list (:rest args) cl:list)
(%:fn/macro list* (arg :rest others) cl:list* nil (:symbol-macro? t))

;; (%:fn/macro defclass* (name cl:&body slots) structy-defclass:deftclass)
;; (%:macro defclass* structy-defclass:deftclass)

(cl:eval-when (:load-toplevel :compile-toplevel :execute)
  ;; (%:fn/macro import (symbols :optional ((package namespace) (sb-int:sane-package))) cl:import)
  ;; (%:fn/macro export (symbols :optional ((package namespace) (sb-int:sane-package))) cl:export)

  (export
   (cl:defmacro --> (cl:&body body)
     (cl:reduce (cl:lambda (acc form)
                  (if (cl:symbolp form)
                      (list form acc)
                      (cl:subst acc :? form)))
                (cl:rest body)
                :initial-value (cl:first body))))
  (export
   (cl:defmacro ->> (cl:&body body)
     (cl:reduce (cl:lambda (acc form)
                  (if (cl:symbolp form)
                      (list form acc)
                      (cl:subst acc :?? form)))
                (cl:rest body)
                :initial-value (cl:first body))))

  (export
   (cl:defmacro >> (cl:&body body)
     `(lambda (arg)
        (->> arg ,@body))))

  (export
   (cl:defmacro ?> (input callback)
     `(let ((stuff ,input))
        (when stuff
          (,callback stuff)))))

  (export
   (cl:defmacro defpipe (name cl:&body body)
     `(block nil
        (defun ,name (arg)
          (--> arg ,@body))
        (cl:define-symbol-macro ,name #',name)
        ',name)))

  (export
   (cl:defmacro defpipe* (name input output cl:&body body)
     (let (input-variable input-type)
       (if (cl:consp input)
           (cl:destructuring-bind (var type)
               input
             (setf input-variable var)
             (setf input-type type))
           ;; we assume symbol here
           (block nil
             (setf input-variable input)
             (setf input-type input)))
       `(define-fun ,name ((,input-variable ,input-type))
          (:returns ,output)
          (--> ,input-variable ,@body))))))

(sb-ext:without-package-locks
  ;; (cl:define-symbol-macro eq #'cl:eq)
  ;; (cl:define-symbol-macro eql #'cl:eql)
  ;; (cl:define-symbol-macro equal #'cl:equal)
  ;; (cl:define-symbol-macro equalp #'cl:equalp)
  ;; (cl:define-symbol-macro list #'cl:list)
  (cl:define-symbol-macro list* #'cl:list*)
  (cl:define-symbol-macro not #'cl:not)
  (cl:define-symbol-macro and #'cl:and)
  (cl:define-symbol-macro or #'cl:or)
  (cl:define-symbol-macro identity #'cl:identity))

(defmacro export* (cl:&body expression)
  "TODO: cover name-p for destruct too
right now it's being neither exported nor renamed"
  (setf expression (cl:car expression))
  ;; TODO: what if it's not a cons but also not a symbol?
  (if (not (cl:consp expression))
      `(export ,expression)
      ;; defun
      (cl:cond
        ;; TODO: handle (setf whatever)
        ((let ((symbol-name (cl:symbol-name (cl:car expression))))
           (cl:string= symbol-name "DEFUN"))
         (let ((name (cl:second expression)))
           (when (cl:symbolp name)
             `(cl:prog1
                  (export ,expression)
                (cl:define-symbol-macro ,name #',name)))))
        ;; defunion
        ((-->
           expression
           cl:car
           cl:symbol-name
           (cl:string= :? "DEFUNION"))
         (let ((type (cl:second expression))
               (fields (cl:mapcar #'cl:first (cl:nthcdr 3 expression))))
           `(cl:eval-when (:load-toplevel :compile-toplevel :execute)
              (cl:prog1
                  ,expression
                (export '(,type ,@fields))
                ,@(cl:mapcar (lambda (field)
                               `(cl:define-symbol-macro ,field #',field))
                             fields)))))
        ;; defstruct
        ((cl:eq (cl:car expression) 'defstruct)
         (cl:destructuring-bind (_ head cl:&body body)
             expression
           (cl:declare (cl:ignore _))
           (cl:flet ((find (list key)
                         (let ((item (cl:assoc key list)))
                           (cl:cond
                             ((not item)
                              (values nil t))
                             ((and (cl:cdr item)
                                   (cl:nth 1 item))
                              (values (cl:nth 1 item) t))
                             (t
                              (values nil nil)))))
                     (assume (symbol prefix)
                       (cl:intern (cl:concatenate
                                   'string
                                   prefix
                                   (cl:symbol-name symbol)))))
             (let (name
                   constructor
                   conc
                   copier
                   predicate)
               (if (cl:consp head)
                   (block nil
                     (setf name (cl:car head))
                     (setf head (cl:cdr head))
                     (setf constructor (cl:multiple-value-bind (value should-exist?)
                                           (find head :constructor)
                                         (when should-exist?
                                           (or value (assume name "MAKE-")))))
                     (setf copier (cl:multiple-value-bind (value should-exist?)
                                      (find head :copier)
                                    (when should-exist?
                                      (or value
                                          (assume name "COPY-")))))
                     (unless (find head :type)
                       (setf predicate (cl:multiple-value-bind (value should-exist?)
                                           (find head :predicate)
                                         (when should-exist?
                                           (or value
                                               (-->
                                                 name
                                                 cl:symbol-name
                                                 (cl:concatenate 'string :? "-P")
                                                 cl:intern))))))
                     (setf conc (cl:multiple-value-bind (value should-exist?)
                                    (find head :conc-name)
                                  (if should-exist?
                                      (if value
                                          (cl:symbol-name value)
                                          (cl:concatenate 'string (cl:symbol-name conc) "-"))
                                      ""))))
                   (block nil
                     (setf name head)
                     (setf constructor (assume name "MAKE-"))
                     (setf copier (assume name "COPY-"))
                     (setf predicate (cl:concatenate 'string (cl:symbol-name name) "-P"))
                     (setf conc (cl:concatenate 'string (cl:symbol-name name) "-"))))
               (let ((fields (cl:mapcar (lambda (item)
                                          (let ((field (if (cl:consp item)
                                                           (cl:car item)
                                                           item)))
                                            (if (cl:string= conc "")
                                                field
                                                (cl:intern (cl:concatenate 'string conc (cl:symbol-name field))))))
                                        body)))
                 `(cl:eval-when (:load-toplevel :compile-toplevel :execute)
                    ,expression
                    ,(when constructor
                       `(cl:define-symbol-macro ,constructor #',constructor))
                    ,(when copier
                       `(cl:define-symbol-macro ,copier #',copier))
                    ,(when predicate
                       `(cl:define-symbol-macro ,predicate #',predicate))
                    ,@(loop :for field :in fields
                            :collect `(cl:define-symbol-macro ,field #',field))
                    (export '(,@(cl:remove nil (cl:list*
                                                name
                                                constructor
                                                copier
                                                predicate
                                                fields))))))))))
        (cl:t
         `(cl:eval-when (:load-toplevel :compile-toplevel :execute)
            (export ,expression))))))
(cl:export 'export*)

(export*
  (defmacro define-fun (name args &body body)
    "(defun ...) + argument types + automatic (export* ...) + (block nil ...) + docstring handling"
    (let ((result t)
          docstring
          preamble
          (ignores (-->
                     (cl:mapcar (lambda (arg)
                                  (let* ((symbol (if (cl:consp arg)
                                                     (cl:first arg)
                                                     arg))
                                         (name (cl:symbol-name symbol)))
                                    (when (cl:char= (cl:char name 0) #\_)
                                      `(ignore ,symbol))))
                                args)
                     (cl:remove nil :?)))
          (declares (cl:mapcar (lambda (arg)
                                 (if (cl:consp arg)
                                     `(type ,(cl:second arg) ,(cl:first arg))
                                     `(type t ,arg)))
                               args)))
      (let ((cons (cl:first body)))
        (when (and (cl:consp cons)
                   (eql (cl:first cons) :returns))
          (setf result (cl:second cons))
          (setf body (cl:rest body))))
      (when (cl:stringp (cl:first body))
        (setf docstring (cl:first body))
        (setf body (cl:rest body)))
      (when docstring
        (cl:push docstring preamble))
      (when ignores
        (cl:push `(declare ,@ignores) preamble))
      (when declares
        (cl:push `(declare ,@declares) preamble))
      `(cl:progn
         (declaim (ftype (function ,(cl:mapcar (lambda (arg)
                                                 (if (cl:consp arg)
                                                     (cl:second arg)
                                                     t))
                                               args)
                                   ,result)
                         ,name))
         (export*
           (defun ,name ,(cl:mapcar (lambda (arg)
                                      (if (cl:consp arg)
                                          (cl:first arg)
                                          arg))
                          args)
             ,@preamble
             (cl:the ,result (block nil ,@body))))
         ,(unless (cl:consp name)
            `(cl:define-symbol-macro ,name #',name))))))

(export*
  (defmacro define-lambda (args &body body)
    "(defun ...) + argument types + automatic (export* ...) + (block nil ...)"
    (let ((result t)
          preamble
          (ignores (-->
                     (cl:mapcar (lambda (arg)
                                  (let* ((symbol (if (cl:consp arg)
                                                     (cl:first arg)
                                                     arg))
                                         (name (cl:symbol-name symbol)))
                                    (when (cl:char= (cl:char name 0) #\_)
                                      `(ignore ,symbol))))
                                args)
                     (cl:remove nil :?)))
          (declares (cl:mapcar (lambda (arg)
                                 (if (cl:consp arg)
                                     `(type ,(cl:second arg) ,(cl:first arg))
                                     `(type t ,arg)))
                               args)))
      (let ((cons (cl:first body)))
        (when (and (cl:consp cons)
                   (eql (cl:first cons) :returns))
          (setf result (cl:second cons))
          (setf body (cl:rest body))))
      (when ignores
        (cl:push `(declare ,@ignores) preamble))
      (when declares
        (cl:push `(declare ,@declares) preamble))
      `(cl:progn
         (lambda ,(cl:mapcar (lambda (arg)
                               (if (cl:consp arg)
                                   (cl:first arg)
                                   arg))
                             args)
           ,@preamble
           (cl:the ,result (block nil ,@body)))))))

(export
 (defmacro define-let (name type value &optional doc)
   `(block nil
      (declaim (type ,type ,name))
      (defparameter ,name ,value ,@(list doc)))))

(export
 (defmacro define-var (name type value &optional doc)
   `(block nil
      (declaim (type ,type ,name))
      (defvar ,name ,value ,@(list doc)))))

(export
 (defmacro define-const (name type value &optional doc)
   `(block nil
      (declaim (type ,type ,name))
      (defconstant ,name ,value ,@(list doc)))))

(%:fn/macro to-string (object
                       ;; :key
                       ;; (array (cl:macroexpand-1 *array*))
                       ;; (base (cl:macroexpand-1 *base*))
                       ;; (case (cl:macroexpand-1 *case*))
                       ;; (circle (cl:macroexpand-1 *circle*))
                       ;; (escape (cl:macroexpand-1 *escape*))
                       ;; (gensym (cl:macroexpand-1 *gensym*))
                       ;; (length (cl:macroexpand-1 *length*))
                       ;; (level (cl:macroexpand-1 *level*))
                       ;; (lines (cl:macroexpand-1 *lines*))
                       ;; (miser-width (cl:macroexpand-1 *miser-width*))
                       ;; (pprint-dispatch (cl:macroexpand-1 *pprint-dispatch*))
                       ;; (pretty (cl:macroexpand-1 *pretty*))
                       ;; (radix (cl:macroexpand-1 *radix*))
                       ;; (readably (cl:macroexpand-1 *readably*))
                       ;; (right-margin (cl:macroexpand-1 *right-margin*))
                       ;; (suppress-print-errors (cl:macroexpand-1 *suppress-print-errors*))
                       )
            cl:write-to-string)

(export*
  (cl:defun void (anything)
    (cl:declare (cl:ignore anything))))

(export*
  (cl:deftype void ()
    'cl:null))

;; (export (defpipe* !! not not))
(export*
  (defmacro !! (stuff)
    `(not (not ,stuff))))

(export*
  (defmacro true? (stuff)
    `(not (not ,stuff))))

(export*
  ;; defun-alias?
  (defmacro defalias (from to)
    `(std.internal:fn ,from ,to)))
(%:type hashtable cl:hash-table)

;; (export
;;  (deftype int (&optional bits)
;;    `(cl:unsigned-byte ,@(list bits))))

(export
 (cl:defmacro defunion (name config &body choices)
   (cl:declare (cl:ignore config))
   `(cl:progn
      ,@(cl:loop :for (choice . fields) :in choices
           :collect `(cl:defstruct (,choice (:constructor ,choice (,@fields))
                                            (:conc-name nil)
                                            (:copier nil)
                                            (:predicate ,(cl:intern (cl:concatenate 'string
                                                                                    (cl:symbol-name choice)
                                                                                    "?"))))
                       ,@fields))
      (cl:deftype ,name ()
        '(or ,@(cl:loop :for (choice) :in choices
                  :collect choice))))))

;; -----------
;;    LIST
;; -----------
;; (%:type list cl:list)
;; (%:class list cl:list)
;; (%:macro-with-args list () cl:list "
;; Creates a list with no tail.
;; Class hierarchy: t > sequence > list
;; ```
;; (list)       ; => nil
;; (list)       ; => ()
;; (list a)     ; => (a)
;; (list a b)   ; => (a b)
;; (list a b c) ; => (a b c)
;; ```
;; ")


;; (cl:do-external-symbols (symbol :std.core.convenience)
;;   (export symbol :std.core))


;; All the things below either truly belong here
;; or there simply isn't enough of them to warrant a separate sub-library.

;; (%:macro define-condition cl:define-condition)
;; ;; (%:macro defmethod/combination cl:define-method-combination)
;; (%:macro define-macro/compiler cl:define-compiler-macro)
;; (%:macro defmacro/symbol cl:define-symbol-macro)
;; (%:macro defmacro/modify cl:define-modify-macro)
;; (%:macro defsetf/expander cl:define-setf-expander)

;; (%::macro-with-args atom? (object) cl:atom "")

;; (%:defmacro setf*-zip-do (symbols values &body forms)
;;   `(cl:progv ,symbols ,values ,@forms))

;; --------------
;;    NUMBERS
;; --------------

(%:fn/macro + (:rest numbers) cl:+ "
WARNING: Type coersion

(+) ; => 0
(+ 1) ; => 1
(+ 1 1.0) ; => 2.0
(+ 1 2 3) ; => 6
" (:symbol-macro? t))

(%:fn/macro - (number :rest numbers) cl:- "
WARNING: Type coersion

(-)       ; => error! need at least 1 argument
(- 1)     ; => -1
(- 1 1)   ; => 0
(- 1 1.0) ; => 0.0
(- 1 2 3) ; => -4
" (:symbol-macro? t))

(%:fn/macro * (:rest numbers) cl:* "
TODO.
" (:symbol-macro? t))

(%:fn/macro / (number :rest numbers) cl:/ "
TODO.
NOTE: Not sure about making it a symbol macro. Leaving it as symbol macro for now.
" (:symbol-macro? t))

(%:fn/macro = (number :rest more-numbers) cl:= "
WARNING: Type coersion

(= 0 -0.0)    ; => t
(= 1 1)       ; => t
(= 1 1.0)     ; => t
(= 1 \"abc\") ; => error! \"abc\" is not a number
(= 1 1 1)     ; => t (can compare more than just 2 arguments)
" (:symbol-macro? t))

(%:fn/macro != (number :rest more-numbers) cl:/= "
WARNING: Type coersion

```
(/= 0)     ; => t
(/= 0 1)   ; => t
(/= 0 0.0) ; => nil
```
" (:symbol-macro? t))

(%:fn/macro < (number :rest more-numbers) cl:< "
WARNING: Type coersion
```
(< 0)           ; => t
(< 0 0)         ; => nil
(< 0 1)         ; => t
(< 0 1 2 3 4)   ; => t
(< 0 1 2 3 4 4) ; => nil
```
" (:symbol-macro? t))

(%:fn/macro <= (number :rest more-numbers) cl:<= "
WARNING: Type coersion

```
(<= 0)           ; => t
(<= 0 0)         ; => t
(<= 0 1)         ; => t
(<= 0 1 2 3 4)   ; => t
(<= 0 1 2 3 4 4) ; => t
```
" (:symbol-macro? t))

(%:fn/macro > (number :rest more-numbers) cl:> "
WARNING: Type coersion

```
(> 4)           ; => t
(> -4)          ; => t
(> 4 4)         ; => nil
(> 4 3)         ; => t
(> 4 3 2 1 0)   ; => t
(> 4 3 2 1 0 0) ; => nil
```
" (:symbol-macro? t))

(%:fn/macro >= (number :rest more-numbers) cl:>= "
WARNING: Type coersion

```
(>= 4)           ; => t
(>= -4)          ; => t
(>= 4 4)         ; => t
(>= 4 3)         ; => t
(>= 4 3 2 1 0)   ; => t
(>= 4 3 2 1 0 0) ; => t
```
" (:symbol-macro? t))

(%:fn/macro 1+ (number) cl:1+ "
INFO: Same as `(+ number 1)`. To mutate in-place, see 'incf'.
" (:symbol-macro? t))

(%:fn/macro 1- (number) cl:1- "
INFO: Same as `(- number 1)`. To mutate in-place, see `decf`.
" (:symbol-macro? t))

(%:fn/macro incf (place :optional (delta 1)) cl:incf "
WARNING: Type coersion
INFO: Place is evaluated only once
")

(%:fn/macro decf (place :optional (delta 1)) cl:decf "
WARNING: Type coersion
INFO: Place is evaluated only once
")

(cl:export
 (deftype maybe (type)
   `(or null ,type)))
