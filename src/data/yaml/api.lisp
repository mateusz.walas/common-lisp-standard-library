(cl:defpackage :std.data.yaml
  (:use)
  ;; :yaml is one of the nicknames of cl-yaml
  ;; so we're going with :yml instead I guess
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.data.yaml)

(%:fn to-stream cl-yaml:emit) ;
(%:fn to-object cl-yaml:emit-object)
(%:fn to-string cl-yaml:emit-to-string)
(%:fn from-string cl-yaml:parse)
;; TODO: finish it
