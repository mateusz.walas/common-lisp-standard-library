(asdf:defsystem :std.data.csv
  :depends-on (:std.internal
               :cl-csv)
  :components
  ((:file "api")))
