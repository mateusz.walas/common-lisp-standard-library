(cl:defpackage :std.data.csv
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.data.csv)

(%:var *newline* cl-csv:*newline*)
(%:var *quote* cl-csv:*quote*)
(%:var *separator* cl-csv:*separator*)
(%:var *quote-escape* cl-csv:*quote-escape*)
(%:var *signals?* cl-csv:*enable-signals*)
(%:var *string-empty-is-nil* cl-csv:*empty-string-is-nil*)
(%:var *default-external-format* cl-csv:*default-external-format*)

(%:fn read cl-csv:read-csv) ; works with strings | streams | pathnames
(%:fn read-row cl-csv:read-csv-row)
(%:fn read-sample cl-csv:read-csv-sample) ; no idea what a 'sample' is

;; (cl-csv:data)
;; (cl-csv:import-from-csv )
;; (cl-csv:rows-foreach ())

;; TODO: include the rest of the library
