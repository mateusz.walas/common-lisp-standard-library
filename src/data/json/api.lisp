(cl:defpackage :std.data.json
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.data.json)

(%:var +true+ jsown:*parsed-true-value* "Equivalent of javascript's true")
(%:var +false+ jsown:*parsed-false-value* "Equivalent of javascript's false")
(%:var +null+ jsown:*parsed-null-value* "Equivalent of javascript's null")
(%:var +list-empty+ jsown:*parsed-empty-list-value* "Equivalent of javascript's [] empty array")

(%:macro keys-do jsown:do-json-keys)
;; TODO:
;; there is some problem here
;; i get a 'function is nil' error during compilation
;; (%:macro create () jsown:new-js)
(%:macro extend jsown:extend-js)
(%:macro filter jsown:filter)

(%:fn/macro create-empty () jsown:empty-object)
(%:fn/macro keys (object) jsown:keywords)
(%:fn/macro key? (object key) jsown:keyp)
(%:fn/macro key-container-build () jsown:build-key-container)
(%:fn/macro key-remove! (object key) jsown:remkey)
(%:fn read jsown:parse)
(%:fn/macro read-contained () jsown:parse-with-container) ; no idea what it does
(%:fn/macro value (object key) jsown:val-safe)
(%:fn/macro value-unsafe (object key) jsown:val)
(%:fn/macro write (object) jsown:to-json)
(%:fn/macro write* (object) jsown:to-json*) ; no idea what's the difference

;; TODO: finish it (?)
