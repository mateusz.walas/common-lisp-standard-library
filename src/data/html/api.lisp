(cl:defpackage :std.data.html
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.data.html)

;; (%:fn xhtml plump:)
;; TODO: finish it

(%:fn/macro parse (input :key root) plump:parse)
(%:fn/macro serialize (node :optional (stream cl:t)) plump:serialize)
(%:fn/macro remove (child) plump:remove-child)
(%:fn/macro child-append (parent child) plump:append-child)
(%:fn/macro child-first (element) plump:first-child)
(%:fn/macro child-last (element) plump:last-child)
(%:fn/macro child-last-element (element) plump:last-element)
(%:fn/macro children-clone (node :optional ((deep deep?)) new-parent) plump:clone-children)
(%:fn/macro attribute-remove (element attribute) plump:remove-attribute)
(%:fn/macro attribute-has (element attribute) plump:has-attribute)
(%:fn/macro attribute-set (element attribute value) plump:set-attribute)
(%:fn/macro attribute-get (element attribute) plump:get-attribute)
(%:fn/macro attributes-clone (node) plump:clone-attributes)
(%:fn/macro attributes-clone (node) plump:clone-attributes)
