(cl:defpackage :std.type
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.type)

(%:fn/macro of (object) cl:type-of)
;; TODO: does anyone even use the 'ENVIRONMENT' argument?
(%:fn/macro ? (object type :optional environment) cl:typep)
(%:fn/macro = (type1 type2) alexandria:type=)
(%:fn/macro checker (type) alexandria:of-type)
(%:macro assert! cl:check-type)
