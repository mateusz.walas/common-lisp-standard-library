(cl:defpackage :std.cffi.sbcl
  (:use)
  ;; TODO: see std.cffi file's TODO
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.cffi.sbcl)

(%:macro deftype sb-alien:define-alien-type)
(%:macro defvar sb-alien:define-alien-variable)
(%:macro defun sb-alien:define-alien-routine)
(%:macro cast sb-alien:cast)
(%:macro pointer-at sb-alien:addr) ; maybe pointer-to ?

(%:macro create sb-alien:make-alien)
(%:macro-with-args alien/string (string) sb-alien:make-alien-string)
(%:macro-with-args alien-call (alien) sb-alien:alien-funcall)
(%:macro-with-args alien-deref (alien) sb-alien:deref)
(%:macro-with-args alien-free (alien) sb-alien:free-alien)
(%:macro alien-size sb-alien:alien-size)
(%:macro-with-args alien-slot-value (alien slot) sb-alien:slot)
(%:macro alien-do sb-alien:with-alien)
(%:macro alien-extern sb-alien:extern-alien)
(%:macro-with-args alien-error-code () sb-alien:get-errno)

(%:macro-with-args shared-object-load (pathname) sb-alien:load-shared-object)
(%:macro-with-args shared-object-unload (pathname) sb-alien:unload-shared-object)

;; (%:macro-with-args cffi-memory-disown! sb-ext:purify) ; SBCL 2.0.9 does not implement it ...?

;; TODO: idk, the with* has emacs formatting/indentation
(%:macro gc-exclude-do sb-sys:with-pinned-objects)
(%:macro gc-disable-do sb-sys:without-gcing)

(cl:export
 (cl:defmacro alien-deref* (object cl:&rest slots)
   (cl:reduce (cl:lambda (object slot)
                (cl:list 'alien-deref object slot))
              (cl:rest slots)
              :initial-value (cl:list 'alien-deref object))))

;; TODO: move sap* to a separate file?
;; SAP = System Area Pointer
(%:macro-with-args sap= (x y) sb-sys:sap=)
(%:macro-with-args sap-ref-8 (sap offset) sb-sys:sap-ref-8)
(%:macro-with-args sap-ref-16 (sap offset) sb-sys:sap-ref-16)
(%:macro-with-args sap-ref-32 (sap offset) sb-sys:sap-ref-32)
(%:macro-with-args sap-ref-64 (sap offset) sb-sys:sap-ref-64)
(%:macro-with-args sap-ref-word (sap offset) sb-sys:sap-ref-word)
(%:macro-with-args sap-ref-single (sap offset) sb-sys:sap-ref-single)
(%:macro-with-args sap-ref-sap (sap offset) sb-sys:sap-ref-sap)
;; (%:macro-with-args sap-ref-long sb-sys:sap-ref-long) ; SBCL 2.0.9 does not implement it ...?
(%:macro-with-args sap-ref-double (sap offset) sb-sys:sap-ref-double)
(%:macro-with-args sap-ref-lisp-object (sap offset) sb-sys:sap-ref-lispobj)

(%:macro-with-args sap-from-int (x) sb-sys:int-sap)
(%:macro-with-args sap-from-alien (alien) sb-alien:alien-sap)
;; TODO: make sure all functions are included here
