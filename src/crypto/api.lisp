(cl:defpackage :std.crypto
  (:use)
  ;; TODO: it seems ironclad already reserves the namespace :crypto... <_<
  ;; UPDATE to TODO: it doesn't matter, we namespace everything with std.*
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.crypto)

(%:fn/macro kdf-list () ironclad:list-all-kdfs)
(%:fn/macro prng-list () ironclad:list-all-prngs)
(%:fn/macro mac-list () ironclad:list-all-macs)
(%:fn/macro mode-list () ironclad:list-all-modes)
(%:fn/macro key-pair-kind-list () ironclad:list-all-key-pair-kinds)
(%:fn/macro authenticated-encryption-mode-list () ironclad:list-all-authenticated-encryption-modes)
(%:fn/macro cipher-list () ironclad:list-all-ciphers)
(%:fn/macro cipher-supported? (name) ironclad:cipher-supported-p)
(%:fn/macro digest-list () ironclad:list-all-digests)
(%:fn/macro digest-supported? (name) ironclad:digest-supported-p)
(%:fn/macro digest-len (digest) ironclad:digest-length)
(%:fn/macro digest-file (digest-spec pathname) ironclad:digest-file)
(%:fn/macro digest-stream (digest-spec stream) ironclad:digest-stream)
(%:fn/macro digest-sequence (digest-spec sequence) ironclad:digest-sequence)
;; salt and digest
(%:fn/macro password-hash-pbkdf2-to-vectors (password
                                                    :key
                                                    (salt (ironclad:make-random-salt))
                                                    (digest 'ironclad:sha256)
                                                    (iterations 1000))
                   ironclad:pbkdf2-hash-password)
(%:fn/macro password-hash-pbkdf2-to-string (password
                                                   :key
                                                   (salt (ironclad:make-random-salt))
                                                   (digest 'ironclad:sha256)
                                                   (iterations 1000)
                                                   )
                   ironclad:pbkdf2-hash-password-to-combined-string)
(%:fn/macro password-hash-pbkdf2-check (password combined-salt-and-digest) ironclad:pbkdf2-check-password)

;; TODO: finish it, ironclad is pretty big
