(asdf:defsystem :std.crypto.ulid
  :depends-on (:std.core
               :std.string
               :std.math
               :std.number
               :std.number.int
               :std.sequence)
  :components
  ((:file "api")))
