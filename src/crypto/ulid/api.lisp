(cl:defpackage :std.crypto.ulid
  (:use :std.core)
  ;; TODO: it seems ironclad already reserves the namespace :crypto... <_<
  ;; UPDATE to TODO: it doesn't matter, we namespace everything with std.*
  (:local-nicknames (:char :std.char)
                    (:math :std.math)
                    (:number :std.number)
                    (:int :std.number.int)
                    (:random :std.crypto.random)
                    (:sequence :std.sequence)
                    (:string :std.string)))
(cl:in-package :std.crypto.ulid)

(defvar +encoding+ "0123456789ABCDEFGHJKMNPQRSTVWXYZ")
(defvar +encoding-len+ (string:len +encoding+))
(defvar +time-max+ (- (math:power 2 48) 1))
(defvar time-len 10)
(defvar random-len 16)

(export*
  (defun char-replace-at (string index character)
    (cl:declare (cl:type string string)
                (cl:type number index)
                (cl:type cl:character character))
    (if (> index (- (string:len string) 1))
        string
        (string:concat (sequence:subseq! string 0 index)
                       (string character)
                       (char:to-string character)
                       (sequence:subseq! string (+ index 1))))))

(export*
  (defun base32-increment (string)
    (let ((char-done nil)
          (index (string:len string))
          (char nil)
          (char-index nil)
          (char-index-max (- +encoding-len+ 1)))
      (loop :while (and (not char-done)
                        ;; basically index-- post-decrement
                        (let ((index-current index))
                          (decf index)
                          (<= 0 index-current)))
            :do (block nil
                  (setf char (string:at string index))
                  (setf char-index (sequence:position +encoding+ char))
                  (unless char-index
                    (return-from base32-increment (values nil '(:error "Incorrectly encoded string."))))
                  (if (= char-index char-index-max)
                      (setf string (char-replace-at string index (string:at +encoding+ 0)))
                      (setf char-done (char-replace-at string index (string:at +encoding+ (+ char-index 1)))))))
      (when (<= 0 char-done)
        (return-from base32-increment (values char-done nil)))
      (values nil '(:error "Cannot increment this string.")))))

(export*
  (defun char-random ()
    (-->
      (random:create t)
      (math:random +encoding-len+ :?)
      (string:at +encoding+ :?))))

(export*
  (defun time-encode (now len)
    (cl:cond
      ((not (number:? now))
       (values nil (string:format "~a must be a number" now)))
      ((< +time-max+ now)
       (values nil (string:format "cannot encode ime greater than ~a" +time-max+)))
      ((< now 0)
       (values nil "time must be positive"))
      ((not (int:? now))
       (values nil "time must be an integer"))
      (t
       (let ((mod nil)
             (string (string:create len)))
         (loop :for i :from (- len 1) :downto 0
               :do (block nil
                     (setf mod (math:modulo now +encoding-len+))
                     (setf (string:at string i) (string:at +encoding+ mod))
                     (setf now (/ (- now mod) +encoding-len+))))
         string)))))

(time-encode (cl:get-internal-run-time) 6)
