(cl:defpackage :std.crypto.base64
  (:use)
  ;; TODO: it seems ironclad already reserves the namespace :crypto... <_<
  ;; UPDATE to TODO: it doesn't matter, we namespace everything with std.*
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.crypto.base64)

(%:fn/macro from-integer (input
                          :key
                          uri
                          (columns 0))
            cl-base64:integer-to-base64-string)
(%:fn/macro from-string (input
                         :key
                         uri
                         (columns 0))
            cl-base64:string-to-base64-string)
(%:fn/macro to-integer (input
                        :key
                        (table cl-base64:+decode-table+)
                        uri
                        (whitespace :ignore))
            cl-base64:base64-string-to-integer)
(%:fn/macro to-string (input
                       :key
                       (table cl-base64:+decode-table+)
                       uri
                       (whitespace :ignore))
            cl-base64:base64-string-to-string)

;; (%:fn/macro from-stream () cl-base64:stream-to-base64-string)
