(cl:defpackage :std.crypto.random
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.crypto.random)

(%:type type cl:random-state "" t)

;; TODO: check if this is cryptograhically safe
;; if not then move it to std.random or something
(%:fn/macro create (:optional state) cl:make-random-state)
(%:fn/macro ? (object) cl:random-state-p)
(%:fn/macro seed (:optional state) sb-ext:seed-random-state)

;; TODO: add UUID?
;; TODO: add cl:random here? - that's probably not cryptographically safe tho
