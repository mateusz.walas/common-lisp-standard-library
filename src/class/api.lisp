(cl:defpackage :std.class
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.class)

(%:fn/macro find! (symbol :optional ((errorp error?) cl:t) environment) cl:find-class "
Warnings:
- This is an accessor
  - Affects only the connection between symbol and class object.
  - Does NOT mutate the class itself.
- May return nil

Signature:
(find! class-name-symbol &optional signal-error-if-not-found? environment)

(find! 'cl:standard-class) ; => #<STANDARD-CLASS COMMON-LISP:STANDARD-CLASS>
(find! 'what?) ; => error! There is no class named WHAT?.
(find! 'what? nil) ; => nil
; Alias for cl:standard-class
(setf (find! 'what?)
      (find! 'cl:standard-class)) ; => #<STANDARD-CLASS COMMON-LISP:STANDARD-CLASS>

; TODO: figure out how to use the environment argument.
")

(%:fn/macro name (class) cl:class-name "
(name (find! 'cl:standard-class)) ; => 'STANDARD_CLASS (symbol, not string)
")

(%:fn/macro instances-deprecate! (class) cl:make-instances-obsolete "
Warnings:
- Called automatically upon backwards-incompatible call of (class:def ...)
- Can be called explicitly by YOU (the user / developer)
- Initiates the process of updating instances
  - only sets a flag, does NOT actually update all the instances all at once
    - TODO: verify this
  - Calls object:update-class-redefined on instances
    - TODO: find out when exactly
")
