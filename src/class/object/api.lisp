(cl:defpackage :std.class.object
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.class.object)

(%:fn/macro create (class :rest initargs :key :key-other) cl:make-instance "
; Class
(class:def person ()
  ((id :initarg :id)
   (name :initarg :name)))

(object 'person :id 1 :name \"Mateusz\") ; => #<PERSON {10024DE5F3}>}
(object 'lol :id 1 :whatever 3) ; => error! There is no class named LOL
")
(%:fn create cl:make-instance "
; Class
(class:def person ()
  ((id :initarg :id)
   (name :initarg :name)))

(object 'person :id 1 :name \"Mateusz\") ; => #<PERSON {10024DE5F3}>}
(object 'lol :id 1 :whatever 3) ; => error! There is no class named LOL
")

(%:macro-with-args allocate (class :rest initargs) cl:allocate-instance)
;; yeah I give up here xd
;; so many arguments...
;; I'm leaving it as just %:fn
(%:fn init! cl:initialize-instance)
;; same story here, too many arguments to even bother
(%:fn init! cl:shared-initialize)
(%:fn/macro init-again! (instance :rest initargs :key argument-precedence-order lambda-list direct-superclasses) cl:reinitialize-instance)
(%:fn/macro class ((x anything)) cl:class-of)
;; (cl:change-class)
(%:macro-with-args class! (instance new-class-name :rest initargs :key-other) cl:change-class)
;; "update-class-*" instead of "class-update-*"
;; because we're updating the object, not the object's class
(%:fn/macro update-for-class-redefined (instance (added-slots slots-added) (discarded-slots slots-discarded) property-list :rest initargs)
            cl:update-instance-for-redefined-class)
(%:fn/macro update-for-class-different (current previous :rest initargs)
            cl:update-instance-for-different-class)
(%:macro-with-args slot (object slot-name) cl:slot-value)
(%:macro-with-args slot-exists? (object slot-name) cl:slot-exists-p)
(%:macro-with-args slot-bound? (object slot-name) cl:slot-boundp)
(%:macro-with-args slot-unbind (object slot-name) cl:slot-makunbound)
;; TODO: see https://gitlab.com/mateusz.walas/common-lisp-standard-library/-/issues/19
(%:fn/macro on-slot-unbound (class instance slot-name) cl:slot-unbound)
(%:fn/macro on-slot-missing (class instance slot-name operation :optional new-value) cl:slot-missing)
;; TODO: maybe just leave it as (object:with-slots ...)
;; looks fine, honestly
;; the only 'problem' is that it would have the object as 2nd argument instead of 1st
(%:macro with-slots cl:with-slots)
(%:macro with-accessors cl:with-accessors)
