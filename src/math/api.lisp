(cl:defpackage :std.math
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.math)

;; TODO: check how quickutil.org does granular loading
;;       check if it's possible to load some functions and then unload the lib
;;       and have just the functions loaded (to save memory)

(%:const +pi+ cl:pi)

(%:fn/macro max (number :rest more-numbers) cl:max "
(max 0) ; => 0
(max 0 1 2) ; => 2
(max 0 1.0 2) ; => 2
(max 0 1.0 #c(2 0)) ; => 2
")

(%:fn/macro min (number :rest more-numbers) cl:min "
(min 0) ; => 0
(min 0 1 2) ; => 0
(min 0 1 2) ; => 0
")

(%:fn/macro floor (number :optional (divisor 1)) cl:floor "
(cl:floor 5.5) ; => (values 5 0.5)
(cl:floor 5.5 0) ; => error! division by 0
(cl:floor 5.5 1) ; => (values 5 0.5)
(cl:floor 5.5 2) ; => (values 2 1.5)
(cl:floor 5.5 3) ; => (values 1 2.5)
(cl:floor 5.5 4) ; => (values 1 1.5)
(cl:floor 5.5 5) ; => (values 0 5.5)
")

(%:fn/macro floor-convert (number :optional (divisor 1)) cl:ffloor)

(%:fn/macro ceiling (number :optional (divisor 1)) cl:ceiling "
(ceiling 1) ; => (values 1 0)
(ceiling 1.5) ; => (values 2 -0.5)
(ceiling 1.5d0) ; => (values 2 -0.5d0)
")

(%:fn/macro ceiling-convert (number :optional (divisor 1)) cl:fceiling)

(%:fn/macro truncate (number :optional (divisor 1)) cl:truncate "
(truncate 0) ; => (values 0 0)
(truncate 0d0) ; => (values 0 0.0d)
(truncate 0.5) ; => (values 0 0.5)
(truncate 1.5d0) ; => (values 1 0.5d0)
")

(%:fn/macro truncate-convert (number :optional (divisor 1)) cl:ftruncate)

(%:fn/macro round (number :optional (divisor 1)) cl:round "
(round 1) ; => (values 1 0)
(round 1.0) ; => (values 1 0.0)
(round 1.5) ; => (values 2 -0.5)
(round 1.45) ; => (values 2 -0.45000005)
")

(%:fn/macro round-convert (number :optional (divisor 1)) cl:fround)

(%:fn/macro power-e (number) cl:exp "
(power-e 0) ; => 1.0)
(power-e 1) ; => 2.7182817)
")

(%:fn/macro power (base power) cl:expt "
(power 0) ; => error! need 2 arguments

(power 0 0) ; => 1
(power 0 1) ; => 1
(power 0 2) ; => 1

(power 1 2) ; => 2
(power 2 2) ; => 4
(power 3 2) ; => 8
(power 4 2) ; => 16
")

(%:fn/macro abs (number) cl:abs "
(abs 0) ; => 0
(abs 1) ; => 1
(abs -1) ; => 1
(abs #c(1.0 1.0)) ; => 1.4142135
")

;; TODO: move/copy to std.number.int:*
; greatest common denominator
(%:fn/macro gcd (:rest integers) cl:gcd "
(gcd) ; => 0
(gcd x) ; => x
(gcd 5 10 15) ; => 5
(gcd 8 12) ; => 4
")

; least common multiple
(%:fn/macro lcm (:rest integers) cl:lcm "
(lcm) ; => 1
(lcm x) ; => x
(lcm 4 8 12) ; => 24
(lcm 5 10) ; => 10
")

(%:fn/macro log (number :optional base) cl:log)
(%:fn/macro modulo (number divisor) cl:mod)
(%:fn/macro remainder (number divisor) cl:rem)
(%:fn/macro sqrt (number) cl:sqrt)
(%:fn/macro sqrt-integer ((n number)) cl:isqrt)
(%:fn/macro sign (number) cl:signum)
(%:fn/macro cis (theta) cl:cis) ; real = cos(radians) and imaginary = sin(radians)

(%:fn/macro conjugate (number) cl:conjugate)
(%:fn/macro phase (number) cl:phase)

(%:fn/macro numerator (number) cl:numerator)
(%:fn/macro denominator (number) cl:denominator)
(%:fn/macro sin (number) cl:sin)
(%:fn/macro cos (number) cl:cos)
(%:fn/macro tan (number) cl:tan)
(%:fn/macro asin (number) cl:asin)
(%:fn/macro acos (number) cl:acos)
(%:fn/macro atan (y :optional x) cl:atan)
(%:fn/macro sinh (number) cl:sinh)
(%:fn/macro cosh (number) cl:cosh)
(%:fn/macro tanh (number) cl:tanh)
(%:fn/macro asinh (number) cl:asinh)
(%:fn/macro acosh (number) cl:acosh)
(%:fn/macro atanh (number) cl:atanh)
(%:fn/macro mean ((sample numbers)) alexandria:mean)
(%:fn/macro median ((sample numbers)) alexandria:median)
(%:fn/macro variance ((sample numbers) :key ((biased biased?) cl:t)) alexandria:variance)
(%:fn/macro standard-deviation ((sample numbers) :key ((biased biased?) cl:t)) alexandria:standard-deviation)
;; TODO: maybe move it to std.crypto? or maybe not, decide what to do
(%:fn/macro random ((arg max) :optional (state cl:*random-state*)) cl:random)
(%:fn/macro random-gaussian (:optional min max) alexandria:gaussian-random)
(%:fn/macro permutations-count (n :optional (k n)) alexandria:count-permutations)
(%:fn/macro binomial-coefficient (n k) alexandria:binomial-coefficient)
(%:fn/macro lerp (v a b) alexandria:lerp)
(%:fn/macro clamp (number min max) alexandria:clamp)
(%:fn/macro factorial (n) alexandria:factorial)
(%:fn/macro subfactorial (n) alexandria:subfactorial)
