(cl:defpackage :std.let
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.let)

;; TODO: add a macro for binding dynamic vars in particular (?)
(%:defmacro dynamic (definitions cl:&body body) "
Binds DYNAMIC variables specifically.
Don't use it to bind regular (local) variables.

You can obviously use the default let for both local and dynamic variables if you want.
This macro exists because common lisp does not support macroexpanding symbols in the default (let ...) form.
This macro does that for you, so you can use aliases for variables and still have the original variables dynamically bound in a given scope.

(defparameter *a* 0)
(cl:define-symbol-macro *b* *a*)
(let:dynamic ((*b* 5))
  *a*) ; => 5
"
  `(cl:let ((,@(cl:mapcan (cl:lambda (definition)
                            (cl:destructuring-bind (var cl:&optional val)
                                definition
                              `(,(cl:macroexpand var) ,val)))
                          definitions)))
     ,@body))

(%:defmacro fun (definitions cl:&body body)
  `(cl:flet ,definitions ,@body))

;; not exactly like let* because fun* can reference previously define functions
;; but it's the closest / easiest / most analogous name I can think of
(%:defmacro fun* (definitions cl:&body body)
  `(cl:labels ,definitions ,@body))

(%:defmacro macro (definitions cl:&body body)
  `(cl:macrolet ,definitions ,@body))

(%:defmacro symbol (definitions cl:&body body)
  `(cl:symbol-macrolet ,definitions ,@body))

(%:macro if alexandria:if-let)
(%:macro when alexandria:when-let)
(%:macro when alexandria:when-let*)
(%:macro with-if alexandria:if-let)
(%:macro with-when alexandria:when-let)
(%:macro with-when* alexandria:when-let*)
;; (cl:export
;;  (cl:defmacro unless ()))
;;
