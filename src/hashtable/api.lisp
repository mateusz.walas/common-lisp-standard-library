(cl:defpackage :std.hashtable
  (:use)
  (:local-nicknames (:% :std.internal))
  ;; TODO: idk about the :ht one
  (:documentation "
Suggested local-nicknames: __hashtable__ or __ht__
"))
(cl:in-package :std.hashtable)

(%:type type cl:hash-table "" t)

                                        ; maybe hash-code? move it to crypto-* namespace?
;; TODO: what is 'x'? Is can probably be any object, right? check it
(%:fn/macro hash (x) cl:sxhash)

;; NOTE: why is (test 'eql')  default value a SYMBOL instead of a FUNCTION OBJECT?
;; what was the idea behind this?
;; I guess some people recompile those test functions after the hashtable is created
;; for development purposes or somethiing
;; NOTE: I'm really tempted to to rename SIZE to INIT-SIZE otherwise I keep thinking it means TOTAL-SIZE or MAX-SIZE
(%:fn/macro create (:key (test 'cl:eql) (size 7) (rehash-size 1.5) (rehash-threshold 1) hash-function weakness ((synchronized synchronized?))) cl:make-hash-table "
```
(create) ; => #<HASH-TABLE :TEST EQL :COUNT 0 {1006901FE3}>
(create :test 'eq
           :size 150
           :rehash-size 2.0
           :rehash-threshold
           :hash-function
           :weakness t
           :synchronized t)
; => #<HASH-TABLE :TEST EQ :COUNT 0 {1006901FE3}>
```

`:test`

  Equality check for keys
  You can define your own test functions via hashtable:deftest

`:rehash-size`

  How much the hashtable grows when it gets 'full'
    integer -> addition
    float -> multiplication

`:rehash-threshold`
  In case of 1.0 you can just say 1
  0.0 < x <= 1.0

`:hash-function`
  [SBCL EXTENSION]
  Args:
    Must accept exactly 2 arguments to compare against each other

  Returns:
    0+ fixnum - pseudo type for the purpose of this description

  Allowed values:
- if specified `:test` is non-standard or not defined by `deftest`
   - `:hash-function` must be specified according to [Args] and [Returns]
- else if `nil` or unspecified
   - a hashing function based on `:test` is used instead (`cl:eql` by default)
")

(%:defmacro create* (opts cl:&body entries)
  "
(create* ()
  (:a . 1)
  (:b . 2))
; => #<HASH-TABLE :TEST EQL :COUNT 2 {100721D8F3}>
"
  (cl:let ((var (cl:gensym)))
    `(cl:let ((,var (cl:make-hash-table ,@opts)))
       (cl:setf ,@(cl:mapcan (cl:lambda (entry)
                               (cl:destructuring-bind (key . value)
                                   entry
                                 (cl:list (cl:list 'cl:gethash key var) value)))
                             entries))
       ,var)))

(%:fn/macro ? (object) cl:hash-table-p "
```
(? (hashtable)) ; => t
(? _) ; => nil
```
")

(%:fn/macro clear! ((hash-table hashtable)) cl:clrhash "
```
Removes all entries from the hashtable.
```
")

(%:fn/macro copy ((table hashtable) :key key test size rehash-size rehash-threshold) alexandria:copy-hash-table "
```
(copy ht) ; => shallow copy
(copy ht :key <deeply-copying-function>) ; => deep copy
```
")

(%:fn/macro count ((hash-table hashtable)) cl:hash-table-count "
Returns:
  Number of elements
```
(count (create)) ; => 0 (by default there are 0 elements, duh)
```
")

(%:fn/macro keys ((table hashtable)) alexandria:hash-table-keys "
Returns:
  List of keys

```
(let ((ht (hashtable)))
  (setf (hashtable:get :a ht) 1)
  (setf (hashtable:get :b ht) 2)
  (setf (hashtable:get :c ht) 3)
  (hashtable:keys ht)) ; => (list :a :b :c)
; TODO: make sure whether the order of keys is specified or not.
```
")

(%:fn/macro keys-foreach ((table hashtable) function) alexandria:maphash-keys "
Example:
```
(keys-foreach <some-hashtable>
              (lambda (key)
                (print:format t \"~%Key: ~a\" key)))
```
")

(%:fn/macro values ((table hashtable)) alexandria:hash-table-values "
```
(let ((ht (hashtable)))
  (setf (hashtable:get :a ht) 1)
  (setf (hashtable:get :b ht) 2)
  (setf (hashtable:get :c ht) 3)
  (hashtable:keys ht)) ; => (list 1 2 3)
; TODO: make sure whether the order of keys is specified or not.
```
")

(%:fn/macro values-foreach ((table hashtable) function) alexandria:maphash-values "
```
(let ((ht (hashtable)))
  (setf (hashtable:get :a ht) 1)
  (setf (hashtable:get :b ht) 2)
  (setf (hashtable:get :c ht) 3)
  (values-foreach ht (lambda (value)
                       (print:format t \"~%value: ~a\" value)))
```
")
;; (%:macro hashtable-ensure alexandria:ensure-gethash)
;; (%:fn/macro ensure! uiop:ensure-gethash "
;; Same syntax as for hashtable:get
;; Args:
;;   key
;;   hashtable
;;   default-value

;; Returns:
;; ")

(std.core:export*
  (cl:defun values-map (hashtable function)
    "Returns a list of mapped values."
    (cl:loop :for value :being :the :hash-value :in hashtable
       :collect (cl:funcall function value))))

(std.core:export*
  (cl:defun values-map! (hashtable function)
    "Mutates <hashtable> values in place using <function> and returns a list of said values."
    (cl:loop :for key :being :the :hash-key :in hashtable
         :using (:hash-value value)
       :collect (cl:let ((value-mapped (cl:funcall function value)))
                  (set hashtable key value-mapped)
                  value-mapped))))

(%:fn/macro ensure! ((table hashtable) key (default default-value)) uiop:ensure-gethash)
(%:fn/macro foreach ((hash-table hashtable) (function-designator function)) cl:maphash "
Warnings:
- You CAN mutate EXISTING keys...
- ... but DO NOT ADD or REMOVE keys (that's undefined behavior)

```
(let ((ht (hashtable)))
  (setf (ht:get :a ht) 1)
  (setf (ht:get :b ht) 2)
  (setf (ht:get :c ht) 3)
  (hashtable:foreach ht
                     (lambda (key value)
                       (print:format t \"~%key: ~a, value: ~a\" key value))))
```
")

;; NOTE: there is also sb-int:%hash-table-alist
;; but it has % so it's internal API, can't use it D:
(%:fn/macro to-alist ((table hashtable)) alexandria:hash-table-alist)
(%:fn/macro to-plist ((table hash-table)) alexandria:hash-table-plist)
(%:fn/macro from-alist (alist) alexandria:alist-hash-table)
(%:fn/macro from-plist (plist) alexandria:plist-hash-table)
(%:fn/macro get ((hash-table hashtable) key :optional ((default default-value))) cl:gethash)
(cl:export
 (cl:defun set (hashtable key value)
   (cl:setf (cl:gethash key hashtable) value)))
;; (cl:export
;;  (cl:defmacro get (hashtable key cl:&optional default)
;;    `(cl:gethash ,key ,hashtable ,@(cl:list default))))
;;
;; (%:fn/macro get cl:gethash "
;; Args:
;;   key
;;   hashtable
;;   default-value

;; Returns:
;;   (values value exists?)

;; (let ((ht (hashtable)))
;;   (setf (hashtable:get :a hashtable) 1)
;;   (hashtable:get :a hashtable)   ; => (values 1 t)
;;   (hashtable:get :b hashtable)   ; => (values nil nil)
;;   (hashtable:get :b hashtable 3) ; => (values 3 nil)
;; )
;; ")

(%:fn/macro remove! ((hash-table hashtable) key) cl:remhash "
```
(let ((ht (hashtable)))
  (setf (hashtable:get :a 1) ht) ; now ht contains 1 entry
  (ht:remove! :a ht))            ; now ht is empty again
```
")

(%:fn/macro rehash-size-multiplier ((instance hashtable)) cl:hash-table-rehash-size)
(%:fn/macro rehash-threshold ((instance hashtable)) cl:hash-table-rehash-threshold)
(%:fn/macro size ((hash-table hashtable)) cl:hash-table-size)
(%:fn/macro test ((instance hashtable)) cl:hash-table-test)
;; TODO: think of a better name
(%:macro deftest sb-ext:define-hash-table-test)
(%:fn/macro weakness ((ht hashtable)) sb-ext:hash-table-weakness)
(%:fn/macro synchronized? ((ht hashtable)) sb-ext:hash-table-synchronized-p)
(%:macro with-locked sb-ext:with-locked-hash-table)
