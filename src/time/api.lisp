(cl:defpackage :std.time
  (:use)
  ;; TODO: think if there is a better name
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.time)

(%:const internal-units-per-second cl:internal-time-units-per-second)

(%:fn/macro universal () cl:get-universal-time)
(%:fn/macro universal-encode (second minute hour date month year :optional time-zone) cl:encode-universal-time)
(%:fn/macro universal-decode (universal-time :optional time-zone) cl:decode-universal-time)
(%:fn/macro parts () cl:get-decoded-time)
(%:fn/macro internal-real () cl:get-internal-real-time)
(%:fn/macro internal-run () cl:get-internal-run-time)

(%:fn/macro sleep! (seconds) cl:sleep)

;; (cl:macrolet ((deftime (name i)
;;                   `(cl:export
;;                     (cl:defun ,name ()
;;                       (cl:nth-value ,i (cl:get-universal-time))))))
;;   (deftime second 0)
;;   (deftime minute 1)
;;   (deftime hour 2)
;;   (deftime day/month 3)
;;   (deftime month 4)
;;   (deftime year 5)
;;   (deftime day/week 6)
;;   (deftime daylight? 7)
;;   (deftime timezone 8))

;; (cl:defstruct (universal (:constructor universal))
;;   (seconds 0 :type (integer 0))
;;   (minute 0 :type (integer 0))
;;   (hour 0 :type (integer 0))
;;   (date 0 :type (integer 0)))
