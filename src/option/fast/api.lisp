(cl:defpackage :std.option.fast
  (:use :std.core)
  (:local-nicknames (:% :std.internal)
                    (:function :std.function)
                    (:match :std.match))
  (:documentation "
Suggested local-nicknames: __option__
"))
(cl:in-package :std.option.fast)

(setf trivia.level2:*arity-check-by-test-call* nil)

(defunion option ()
  (some value)
  (none))

(defun map (option function)
  (declare (cl:type option option)
           (cl:type function function))
  (match:pattern option
    ((none) option)
    ((some value) (some (function:call function value)))))

(defun none? (option)
  (declare (cl:type option option))
  (cl:etypecase option
    (none t)
    (some nil)))

(defun some? (option)
  (declare (cl:type option option))
  (cl:etypecase option
    (none nil)
    (some t)))

(defun bind (option function)
  (declare (cl:type option option)
           (cl:type (function (t) option) function))
  (match:pattern option
    ((some value) (cl:the option (function:call function value)))
    (_ option)))

(defun do (option function)
  (declare (cl:type option option)
           (cl:type (function (t) option) function))
  (match:pattern option
    ((some value)
     (function:call function value)
     option)
    (_
     option)))

(defun count (option)
  (declare (cl:type option option))
  (cl:etypecase option
    (some 1)
    (none 0)))

(defun filter (option function)
  (declare (cl:type option option)
           (cl:type (function (option) bool) function))
  (match:pattern option
    ((some value)
     (if (function:call function value)
         option
         (none)))
    ((none)
     option)))

;; (define-fun flatten ((option option))
;;   (match:pattern option
;;     ((none) option)
;;     ((some option)
;;      (match:pattern option
;;        (()))
;;      (some (value option)))))

(defun of (value)
  (if value
      (some value)
      (none)))

(defun every (option function &key (default t))
  (declare (cl:type option option)
           (cl:type (function (option) bool) function)
           (cl:type bool default))
  (match:pattern option
    ((some value) (function:call function value))
    ((none) default)))

(defun to-list (option)
  (declare (cl:type option option))
  (match:pattern option
    ((some value) (list value))
    ((none) (list))))

(defun or-call (option function)
  (declare (cl:type option option)
           (cl:type (function () option) function))
  (if (some? option)
      option
      (function:call function)))

;; (defun map2 (list function)
;;   (declare (cl:type cl:cons list)
;;            (cl:type (function (option) bool) function))
;;   (cl:destructuring-bind (option1 option2)
;;       list
;;     (or (match:pattern option1
;;           ((some value1)
;;            (match:pattern option2
;;              ((some value2)
;;               (some (function:call value1 value2))))))
;;         (none))))

(cl:define-symbol-macro none #'none)
(cl:define-symbol-macro some #'some)
(cl:define-symbol-macro none? #'none?)
(cl:define-symbol-macro some? #'some?)
(cl:define-symbol-macro bind #'bind)
(cl:define-symbol-macro map #'map)
(cl:define-symbol-macro do #'do)
(cl:define-symbol-macro count #'count)
(cl:define-symbol-macro filter #'filter)
(cl:define-symbol-macro flatten #'flatten)
(cl:define-symbol-macro of #'of)
(cl:define-symbol-macro every #'every)
(cl:define-symbol-macro satisfies #'every)
(cl:define-symbol-macro to-list #'to-list)
(cl:define-symbol-macro or-call #'or-call)

(export '(option
          none none?
          some some?
          map
          bind
          do
          count
          filter
          flatten
          of
          every
          satisfies
          to-list
          or-call))

;; (-->
;;   (some 3)
;;   (map :? (lambda (x)
;;             (* x 5)))
;;   (bind :? (lambda (x)
;;              (some (* x 5))))
;;   (bind :? (lambda (x)
;;              (if (< x 100)
;;                  (some x)
;;                  (none)))))
