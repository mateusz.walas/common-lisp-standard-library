(cl:defpackage :std.option
  (:use :std.core)
  (:import-from :cl :signed-byte)
  (:local-nicknames (:% :std.internal)
                    (:function :std.function)
                    (:match :std.match)
                    (:type :std.type))
  (:documentation "
Suggested local-nicknames: __option__
"))
(cl:in-package :std.option)

(setf trivia.level2:*arity-check-by-test-call* nil)

;; this should be called an enum actually
(defunion option ()
  (some value)
  (none))

(export '(option some none))

(define-fun map ((option option) (fn function))
  (:returns option)
  (match:pattern option
                 ((none) option)
                 ((some value) (some (function:call fn value)))))

(deftype t-to-option ()
  '(function (t) option))

(deftype nothing-to-option ()
  '(function () option))

(deftype option-to-bool ()
  '(function (option) bool))

(define-fun bind ((option option)
                      (fn t-to-option))
  (:returns option)
  (match:pattern option
    ((none) option)
    ((some value) (values (function:call fn value)))))

(define-fun do ((option option)
                    (fn t-to-option))
  (:returns option)
  (when (some? option)
    (function:call fn (value option)))
  option)

;; (define-fun count ((option option))
;;   (:returns int)
;;   (match:type! option
;;     (some 1)
;;     (none 0)))

(define-fun filter ((option option) (fn option-to-bool))
  (:returns option)
  (match:pattern option
    ((some value)
     (if (function:call fn value)
         option
         (none)))
    ((none)
     option)))

;; (define-fun flatten ((option option))
;;   (match:pattern option
;;     ((none) option)
;;     ((some option)
;;      (match:pattern option
;;        (()))
;;      (some (value option)))))

(define-fun of (value)
  (:returns option)
  (if value
      (some value)
      (none)))

(declaim (ftype (function (option (function (t) bool) &key (:default bool))) every?))
(export*
  (defun every? (option fn &key (default t))
    (match:pattern option
      ((some value) (values (function:call fn value)))
      ((none) default))))

(define-fun to-list ((option option))
  (:returns list)
  (match:pattern option
    ((some value) (list value))
    ((none) (list))))

(define-fun or-call ((option option) (function nothing-to-option))
  (if (some? option)
      option
      (function:call function)))

;; (defun map2 (list function)
;;   (declare (cl:type cl:cons list)
;;            (cl:type (function (option) bool) function))
;;   (cl:destructuring-bind (option1 option2)
;;       list
;;     (or (match:pattern option1
;;           ((some value1)
;;            (match:pattern option2
;;              ((some value2)
;;               (some (function:call value1 value2))))))
;;         (none))))

;; (cl:define-symbol-macro none #'none)
;; (cl:define-symbol-macro some #'some)
;; (cl:define-symbol-macro none? #'none?)
;; (cl:define-symbol-macro some? #'some?)
;; (cl:define-symbol-macro bind #'bind)
;; (cl:define-symbol-macro map #'map)
;; (cl:define-symbol-macro do #'do)
;; (cl:define-symbol-macro count #'count)
;; (cl:define-symbol-macro filter #'filter)
;; (cl:define-symbol-macro flatten #'flatten)
;; (cl:define-symbol-macro of #'of)
;; (cl:define-symbol-macro every #'every)
(cl:define-symbol-macro satisfies #'every)
;; (cl:define-symbol-macro to-list #'to-list)
;; (cl:define-symbol-macro or-call #'or-call)

(export '(option satisfies))

;; (-->
;;   (some 3)
;;   (map :? (>> (* :val 5)))
;;   (bind :? (>> (some (* :arg 5))))
;;   (bind :? (>> (if (< :varg 100)
;;                    (some :arg)
;;                    (none)))))

;; (cl:funcall (>> (* :? 5)) 5)
