(cl:defpackage :std.os.process
  (:use)
  (:local-nicknames (:% :std.internal))
  (:documentation "
Suggested local-nickname: __os.process__ or __process__
"))
(cl:in-package :std.os.process)

(%:fn/macro run (program
                 (args arguments)
                 :key
                 directory
                 input
                 output
                 pty
                 ((wait wait?) cl:t)
                 ((search search?))
                 (external-format :default)
                 preserve-fds
                 status-hook
                 ((if-input-does-not-exists if-no-input))
                 ((if-output-exists if-ouput))
                 (error :output)
                 (if-error-exists :error)
                 env)
            sb-ext:run-program)

(%:fn/macro ? (object) sb-ext:process-p)
(%:fn/macro alive? (process) sb-ext:process-alive-p)
(%:fn/macro input ((instance process)) sb-ext:process-input)
(%:fn/macro output ((instance process)) sb-ext:process-output)
(%:fn/macro pid ((instance process)) sb-ext:process-pid) ; not documented in the SBCL manual
(%:fn/macro pty ((instance process)) sb-ext:process-pty) ; not documented in the SBCL manual
(%:fn/macro error ((instance process)) sb-ext:process-error)
(%:fn/macro exit-code (process) sb-ext:process-exit-code)
(%:fn/macro core-dumped ((instance process)) sb-ext:process-core-dumped)
(%:fn/macro close (process) sb-ext:process-close)
(%:fn/macro kill (process signal :optional (whom :pid)) sb-ext:process-kill)
(%:fn/macro status (process) sb-ext:process-status)
(%:fn/macro status-hook ((instance process)) sb-ext:process-status-hook) ; not documented in the SBCL manual
(%:fn/macro wait (process :optional check-for-stopped) sb-ext:process-wait)
