(cl:defpackage :std.os
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.os)

(%:fn/macro type-as-keyword () uiop/os:detect-os)
(%:fn/macro name-as-keyword () uiop/os:operating-system)
(%:fn/macro name-as-string () cl:software-type)
(%:fn/macro kernel () cl:software-version)
(%:fn/macro macos? () uiop/os:os-macosx-p)
(%:fn/macro unix? () uiop/os:os-unix-p)
(%:fn/macro windows? () uiop/os:os-windows-p)
(%:fn/macro cpu-info () cl:machine-version)
;; TODO: is it supposed to be online? (boolean)?
(%:fn/macro cpu-count (:key (default 2) online) serapeum:count-cpus)
(%:fn/macro cpu-architecture-as-keywords () uiop/os:architecture)
(%:fn/macro cpu-architecture-as-string () cl:machine-type)
;; TODO: think of a better name
(%:fn/macro domain-instance () cl:machine-instance)

;; TODO: ??? these return NIL
;; (%:fn/macro site-name-short cl:short-site-name)
;; (%:fn/macro site-name-long cl:long-site-name)

