(cl:defpackage :std.match
  (:use)
  (:import-from :trivia :guard)
  (:local-nicknames (:% :std.internal)
                    (:trivia :trivia)))
(cl:in-package :std.match)

(cl:export 'guard)

;; TODO: trivia has a bunch of symbols exported
;; some of them have insane names (match2*, match2+, match2*+... what?)
;; will have to look through them and see if they are exported by accident or not
;; and then think of normal names for them
;; also... might need to figure out a way to avoid cluttering our own namespaces
;; with all the symbols like @ ! -> etc that trivia seems to use
(%:macro pattern trivia:match)

;; match-* names for the purpose of organizing code
;; might change
;; (%:macro value cl:case "
;; There is a matching branch so its value is returned.
;; (value 1
;;   (0 \"zero\")
;;   (1 \"one\")
;;   (2 \"two\")) => \"one\"

;; No branch matches so nil is returned instead.
;; (value 1
;;   (0 \"zero\")
;;   (2 \"two\")) => nil
;; ")

(cl:export
 (cl:defmacro value (value cl:&body cases)
   `(cl:case ,value
      ,@cases)))

;; (%:macro value! cl:ecase "
;; Same as #'std.match:value except value MUST match a branch.
;; ")

(cl:export
 (cl:defmacro value! (value cl:&body cases)
   `(cl:ecase ,value
      ,@cases)))

(%:macro tree cl:destructuring-bind "
:: Alias for cl:destructuring-bind

Also works on conses and lists.
There are no separate macros for matching just conses or just lists.

(tree (a) (list 1) ... a = 1 ...)
(tree (a b) (list 1 2) ... a = 1, b = 2 ...)
(tree (a b) (list 1 2 3) ... ) => error! too many elements

(tree (a . b) '(1 . 2) ... a = 1, b = 2 ...)
(tree (a . b) '(1 . nil) ... a = 1, b = nil ...)
(tree (a . b) '(1) ... a = 1, b = nil ...)

(tree (a &rest whatever) '(1 2 3 4 5) ... a = 1, whatever = (2 3 4 5) ...)

(tree (a b &aux (c (+ a b))) '(1 2) ... a = 1, b = 2, c = 3 ...)

(tree (a &key b) '(1 :b 2) ... a = 1, b = 2 ...)
(tree (a &key b) '(1 :c 2) ... ) => error! unknown keyword :c

(tree (a &key (b 2)) '(1) ... a = 1, b = 2 ... )
(tree (a &key (b 2)) '(1 :c 3) ... ) => error! expected :b = whatever, got :c = 3

(tree (a &key (b 2) &allow-other-keys) '(1 :c 2 :d 3 :e 4) ... a = 1, b = 2 ...)

(tree (a &optional b) '(1) ... a = 1, b = nil ...)
(tree (a &optional b) '(1 \"hello\") ... a = 1, b = \"hello\" ...)
(tree (a &optional (b 2)) '(1) ... a = 1, b = 2 ...)

(tree (&whole my-list a b) '(1 2) ... my-list = (1 2), a = 1, b = 2 ...)

(tree (a (b)) '(1 (2)) ... a = 1, b = 2 ...)

(tree (a (b &key (c))) (1 (2 :c (3)) ... a = 1, b = 2, c = 3 ...)

; You can specify type declarations or other optimizations if you put them before
; the rest of the forms, i.e.:
(tree (a b c)
    '(1 2 3)
  (declare (type integer a b c))
  ... other code ...)
")

(%:macro test cl:cond "
Common Lisp treats nil as 'false' and everything else as true.
Condition is just a form that evaluates to something, so
it doesn't have to be an actual form, it can be just t or a string or whatever.
First successful branch is entered, any further branches are ignored.

; no conditions are specified so the entire match-test form simply returns nil
(test)

; branch has no body so the condition result is returned instead
(test
  ((+ 1 2))) => 3

; branch is entered and it's body is just \"hello\" so the string is returned
(test
  (t \"hello\")) => \"hello\"

(test
  (nil \"some stuff\") ; ignored
  ((= 1 1) \"yes\") ; branch is entered and returns \"yes\"

; first successful branch is entered and any consecutive one are ignored
(test
  (nil :nil) ; this :nil will never get returned
  ((+ 1 2) \"got 3 which is treated as 'true', so this branch is ok\")
  ; any further branches are ignored, because (+ 1 2) was 'true'
  (t \"t is treated as 'true', so this branch is ok\") ; this branch
  ... doesn't matter what goes here, it's ignored ...
)")

;; (%:macro type cl:typecase  "
;; (type 1
;;   (string \"string\")
;;   (integer \"integer\")) => \"integer\"
;; ")

(cl:export
 (cl:defmacro type (value cl:&body cases)
   `(cl:typecase ,value
      ,@cases)))

;; (%:macro type! cl:etypecase "
;; Same as #'std.match:type except value MUST match a branch.
;; ")

(cl:export
 (cl:defmacro type! (value cl:&body cases)
   `(cl:etypecase ,value
      ,@cases)))


;; TODO: add (at least) trivia here
;; side note: idk if unifying all of these match-*
;; into one generic 'match' macro is even doable, so..
;; TODO: think about it?
