(cl:defpackage :std.debug
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.debug)

(%:fn/macro break (:optional (datum "break") :rest arguments) cl:break)
(%:fn/macro error (datum :rest arguments) cl:error)
(%:fn/macro cerror (continue-string datum :rest arguments ) cl:cerror)
(%:fn/macro inspect (object) cl:inspect)
(%:fn/macro invoke-debugger (condition) cl:invoke-debugger)
;; args: (form)
(%:macro step cl:step)
(%:fn/macro disassemble (object :key (stream cl:*standard-output*) ((use-labels use-labels?) cl:t)) cl:disassemble)

;; TODO: maybe move these to a separate file std.introspection.lisp ?
(%:macro time cl:time)
(%:macro trace-on! cl:trace)
(%:macro trace-off! cl:untrace)

(%:fn/macro describe (object :optional (stream-designator cl:*standard-output*)) cl:describe
    "Prints information about object (can be anything: struct, symbol, function etc) to standard output.")
(%:fn/macro describe-to-stream (object stream) cl:describe-object
    "Same as help except you can specify a stream to write to (instead of standard output).")
