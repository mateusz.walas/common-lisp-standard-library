(cl:defpackage :std.debug.restart
  (:use)
  (:local-nicknames (:% :std.internal)))
(cl:in-package :std.debug.restart)

;; tbh i havent used conditions / restarts / errors
;; I don't understand how they all work so I'm not able to
;; give the function below meaningful names... HELP!
(%:fn find cl:find-restart)
(%:fn compute-all cl:compute-restarts)
;; not sure whether to call it *-bind or *-bind-do
(%:macro case cl:restart-case)
(%:macro bind cl:restart-bind)
(%:macro handler-bind cl:handler-bind) ; terrible name, think of a better one
(%:macro handler-case cl:handler-case) ; terrible name, think of a better one
(%:macro with-simple cl:with-simple-restart)
;; NOTE:
;; ok so there is a few functions that have the same names as their corresponding RESTART classes
;; these are:
;; - abort
;; - continue
;; - muffle-warning
;; - store-value
;; - use-value

;; TODO:
;; come up with some good distinguishing symbol for classes/restarts etc
;; cause if there is a function that is called 'use-value' and then there is a class/restart called 'use-value'
;; then how the hell can you tell

(%:fn value-use cl:use-value)
(%:fn value-store! cl:store-value)
(%:fn abort! cl:abort)
(%:fn invoke! cl:invoke-restart)
(%:fn invoke-interactively! cl:invoke-restart-interactively)
(%:fn continue! cl:continue)
(%:fn warning-muffle! cl:muffle-warning)
(%:fn warn! cl:warn)
(%:fn signal! cl:signal)

;; TODO: finish this
