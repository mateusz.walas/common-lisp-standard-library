(asdf:defsystem :std.doc.dev
  :entry-point "std.doc.dev:start"
  :depends-on (:std.internal.doc
               :std.core
               :std.fs.dir
               :std.fs.namestring
               :std.io.print
               :std.sequence.array
               :std.sequence.list
               :std.io.stream
               :std.os.process
               :std.string
               :std.symbol)
  :components
  ((:file "std.doc.dev")))
