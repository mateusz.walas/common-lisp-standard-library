(cl:defpackage :std.doc.dev
  (:use :std.core)
  (:local-nicknames (:fs.dir :std.fs.dir)
                    (:namestring :std.fs.namestring)
                    (:print :std.io.print)
                    (:print :std.io.print)
                    (:process :std.os.process)
                    (:stream :std.io.stream)
                    (:string :std.string)
                    (:symbol :std.symbol)))
(cl:in-package :std.doc.dev)

;; I don't really like this macro approach here
;; but these functions would look literally the same
;; (except the actual function names, so...)
(defmacro define (name)
  (let ((name-process (symbol:from-string-list name "-PROCESS"))
        (name-start (symbol:from-string-list name "-START"))
        (name-stop (symbol:from-string-list name "-STOP"))
        (name-create (symbol:from-string-list name "-CREATE")))
    `(block nil
       (defun ,name-stop ()
         (when (and ,name-process (process:alive? ,name-process))
           (print:line
            ,(string:format "~%Stopping ~(~a~) process..." name))
           (if (process:kill ,name-process 9)
               (cl:progn
                 (setf ,name-process nil)
                 (print:string " done."))
               (print:line
                ,(string:format "~%Could not stop ~(~a~) process for some reason" name)))))
       (defun ,name-start ()
         (cl:cond
           ((not ,name-process)
            (setf ,name-process (,name-create))
            (print:line
             ,(string:format "~%~@(~a~) process started." name)))

           ((process:alive? ,name-process)
            (print:line
             ,(string:format "~%~@(~a~) process is already running." name)))

           (t
            (print:line
             ,(string:format "~%~@(~a~) process is dead. Resurrecting..." name))
            (setf ,name-process (,name-create))))))))

(defparameter reader-process nil)
(defun reader-create ()
  (process:run
   (string:concat (namestring:namestring (fs.dir:home))
                  "node_modules/.bin/docsify")
   (list "serve" "doc")
   :wait? nil
   :output stream:*stdout*))
(define "READER")

(defparameter writer-process nil)
(defun writer-create ()
  (process:run
   "/bin/bash"
   (list "-c"
         (string:concat "find . -type f -not -path './doc*' -not -path './.git*'"
                        " | entr -c -n -p -d"
                        " sbcl --quit --eval '(asdf:load-system :std :force t)'"))
   :wait? nil
   :output stream:*stdout*))
(define "WRITER")

(export
 (defun start ()
   (reader-start)
   (writer-start)))

(start)
